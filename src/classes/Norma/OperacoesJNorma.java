package classes.Norma;

import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import telas.Norma.Configurar;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Formatter;
import telas.Norma.CriaMacro;
import telas.Norma.FormIni;

public class OperacoesJNorma {

    private String NomeArq;
    private String EndArq;
    private String Retorno;
    private int RetParam;
    private int TipoRetorno;
    private ArrayList<Integer> ValorRegs;
    private String comando;
    private boolean Erro;
    private boolean RetornoBool;
    private String NomMacrosUsadas = "";
    private boolean voltMap;
    private Configurar objConfi;
    private ArrayList<String> arqExcluir;
    private FormIni objFormIni;

    public OperacoesJNorma(Configurar Conf, FormIni objFormP) {
        objConfi = Conf;
        objFormIni = objFormP;
        ValorRegs = new ArrayList();
        arqExcluir = new ArrayList<>();
    }

    public OperacoesJNorma() {
        ValorRegs = new ArrayList();
    }

    public boolean isRetornoBool() {
        return RetornoBool;
    }

    public void setRetornoBool(boolean RetornoBool) {
        this.RetornoBool = RetornoBool;
    }

    public ArrayList<Integer> getValorRegs() {
        return ValorRegs;
    }

    public ArrayList<String> getArqExcluir() {
        return arqExcluir;
    }

    public void setArqExcluir(ArrayList<String> arqExcluir) {
        this.arqExcluir = arqExcluir;
    }

    public void setNomMacrosUsadas(String NomMacrosUsadas) {
        this.NomMacrosUsadas = NomMacrosUsadas;
    }

    public void setValorRegs(ArrayList<Integer> ValorRegs) {
        this.ValorRegs = ValorRegs;
    }

    public String getNomeArq() {
        return NomeArq;
    }

    public void setNomeArq(String NomeArq) {
        this.NomeArq = NomeArq;
    }

    public String getEndArq() {
        return EndArq;
    }

    public void setEndArq(String EndArq) {
        this.EndArq = EndArq;
    }

    public String getRetorno() {
        return Retorno;
    }

    public void setRetorno(String Retorno) {
        this.Retorno = Retorno;
    }

    public int getRetParam() {
        return RetParam;
    }

    public void setRetParam(int RetParam) {
        this.RetParam = RetParam;
    }

    public int getTipoRetorno() {
        return TipoRetorno;
    }

    public void setTipoRetorno(int TipoRetorno) {
        this.TipoRetorno = TipoRetorno;
    }

    public boolean isVoltMap() {
        return voltMap;
    }

    public void setVoltMap(boolean voltMap) {
        this.voltMap = voltMap;
    }

    public String getComando() {
        return comando;
    }

    public void setComando(String comando) {
        this.comando = comando;
    }

    public boolean isErro() {
        return Erro;
    }

    public void setErro(boolean Erro) {
        this.Erro = Erro;
    }

    public String getNomMacrosUsadas() {
        return NomMacrosUsadas;
    }

    public void leArqRetorno(String nomeArq) {
        try {
            nomeArq = "file:///" + System.getProperty("user.dir") + "\\Java\\" + nomeArq + "-2.xml";

            DocumentBuilderFactory bF = DocumentBuilderFactory.newInstance();

            DocumentBuilder dBuilder = null;

            dBuilder = bF.newDocumentBuilder();

            Document d = null;

            d = dBuilder.parse(nomeArq);

            d.normalize();

            NodeList rootNodes = d.getElementsByTagName("Retorno");
            Node root = rootNodes.item(0);
            Element rootElement = (Element) root;

            NodeList ListMacro = rootElement.getElementsByTagName("TipoRetorno");
            Node NMacro = ListMacro.item(0);
            Element NomeMacro = (Element) NMacro;
            this.setTipoRetorno(Integer.parseInt(NMacro.getTextContent()));

            NodeList ListCodMacro = rootElement.getElementsByTagName("ValRetorno");
            Node CoMacro = ListCodMacro.item(0);
            Element CodiMacro = (Element) CoMacro;
            this.setRetorno(CoMacro.getTextContent());

            NodeList RetRegs = rootElement.getElementsByTagName("Regs");
            for (int i = 0; i < RetRegs.getLength(); i++) {
                Element ValorReg = (Element) RetRegs.item(i);
                this.ValorRegs.add(Integer.parseInt(ValorReg.getTextContent()));
            }
            setErro(false);
        } catch (ParserConfigurationException | SAXException | IOException | DOMException | NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Não foi possivel ler o arquivo de Retorno !!!", "Erro Leitura", JOptionPane.ERROR_MESSAGE);
            setErro(true);

            StackTraceElement erro[] = e.getStackTrace();
            objFormIni.setLogSistema("----OpJNorma - lerArqRetorno ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormIni.setLogSistema(erro1.toString() + '\n');
            }

        }
    }

    //faz a substituição da chamada de macro !nome(); para a sintaxe java nome.nome(paramt);
    public String mapeamentoMacros(String Cod, ArrayList Nomes, ArrayList<String> ParametMacro, CriaMacro objCriaMacro) {
        ArrayList<String> parRetornos = new ArrayList<>();
        ArrayList<String> Parametros = new ArrayList<>();
        Parametros = (ArrayList<String>) ParametMacro.clone();
        String texto = "";
        String sub = "";
        String reg0 = "";

        int controle = 0, aux = 0, pos, valor, cont = 0;
        ExecXML execXML;
        sub = "int Retorno[];\n";
        for (int i = 0; i < Nomes.size(); i++) {

            try {
                execXML = new ExecXML();
                execXML.executarXML(Nomes.get(i).toString().trim() + ".xml");
                texto = Nomes.get(i).toString().trim();

                if (Cod.contains(texto)) {
                    sub += "Retorno = " + Nomes.get(i).toString().trim() + "." + Nomes.get(i).toString().trim() + "(";
                    NomMacrosUsadas += "\n//" + Nomes.get(i).toString() + "(";
                    for (int j = 0; j < Integer.parseInt(execXML.getNumReg()); j++) {
                        if (j > controle) {
                            sub += " , ";
                            NomMacrosUsadas += " , ";
                        }
                        NomMacrosUsadas += '#' + Parametros.get(aux);
                        sub += '#' + Parametros.get(aux);
                        parRetornos.add(Parametros.get(aux));
                        aux++;
                    }
                    NomMacrosUsadas += " ) ";

                    sub += " );";
                    pos = 0;
                    //seta os registradores que vao receber os retornos
                    for (int j = i; pos < execXML.getVarsRetorno().size(); j++) {
                        valor = execXML.getRegsUsados().indexOf(execXML.getVarsRetorno().get(pos));
                        sub += "\n" + '#' + objCriaMacro.getRegsRecebeRet().get(j) + " = Retorno[" + valor + "];";
                        parRetornos.remove(valor);
                        parRetornos.add(valor, objCriaMacro.getRegsRecebeRet().get(j));
                        pos++;
                    }

                    pos = i;
                    for (int j = 0; j < parRetornos.size(); j++) {
                        if (!parRetornos.get(j).equals(objCriaMacro.getRegsRecebeRet().get(pos))) {
                            sub += "\n" + '#' + parRetornos.get(j) + " = Retorno[" + j + "];";
                        } else if (pos < objCriaMacro.getRegsRecebeRet().size() - 1) {
                            pos++;
                        }

                    }

                    Cod = Cod.replaceFirst('!' + texto.trim(), sub);
                    Cod = Cod.replace("();", "");
                    parRetornos.clear();
                    sub = "";

                }
            } catch (ParserConfigurationException | SAXException | IOException ex) {
                JOptionPane.showMessageDialog(null, "Erro ao ler arquivo XML " + Nomes.get(i), "Erro XML ", JOptionPane.ERROR_MESSAGE);
                StackTraceElement erro[] = ex.getStackTrace();
                objFormIni.setLogSistema("----OpJNorma - mapeamentoMacros ---- \n\n");
                for (StackTraceElement erro1 : erro) {
                    objFormIni.setLogSistema(erro1.toString() + '\n');
                }
                return null;
            } catch (Exception ex) {
                StackTraceElement erro[] = ex.getStackTrace();
                objFormIni.setLogSistema("----OpJNorma - mapeamentoMacros ---- \n\n");
                for (StackTraceElement erro1 : erro) {
                    objFormIni.setLogSistema(erro1.toString() + '\n');
                }
                return null;
            }

        }
        return Cod;
    }

    private void trocaStatusLabels(ExecXML execXML, String status) {
        //macro como condicional
        try {
            if (objFormIni.getObjCriaMacro() != null && objFormIni.getObjCriaMacro().getObjSelecMacro() != null && !objFormIni.isFocused()) {
                objFormIni.getObjCriaMacro().getObjSelecMacro().setLbStatusExecSelectMac(status);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException ex) {

                }
                //inserção de macro
            } else if (objFormIni.getObjCriaMacro() != null && objFormIni.getObjCriaMacro().getObjInsertCondi() != null && objFormIni.getObjCriaMacro().getObjInsertCondi().getObjSelectMacro() != null) {
                objFormIni.getObjCriaMacro().getObjInsertCondi().getObjSelectMacro().setLbStatusExecSelectMac(status);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException ex) {

                }
                //seleção de macro na tela principal
            } else if (objFormIni.getObjSelecMacro() != null) {
                objFormIni.getObjSelecMacro().setLbStatusExecSelectMac(status);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException ex) {

                }
                //execução da macro
            } else {
                objFormIni.setLbExecutando(status);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException ex) {

                }
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormIni.setLogSistema("----OpJNorma - trocaStatusLabel ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormIni.setLogSistema(erro1.toString() + '\n');
            }
        }
    }

    public boolean salvaArqJava(ExecXML execXML, ClasseNorma Norma, String caminhoJava) {
        try {
            if (ClasseNorma.criaPastaEm(System.getProperty("user.dir"), "Java")) {

                if (arqExcluir != null) {
                    arqExcluir.add(execXML.getNomMacro());
                }
                ClasseNorma objNorma = new ClasseNorma();
                String Metodo = execXML.getCodJavaMetodo();
                String Codigo = execXML.getCodJava();
                Codigo = Codigo.replaceAll("@", "&&");
                Codigo = Codigo.replaceAll("::", "||");
                ArrayList<String> nomMacros;
                nomMacros = this.buscaMacrosUtilizadas(execXML.getCodMacro());

                boolean macrosExiste = false;

                for (int i = 0; i < nomMacros.size(); i++) {
                    if (objNorma.verificaMacroExiste(nomMacros.get(i).trim())) {
                        macrosExiste = true;
                    } else {
                        macrosExiste = false;
                        JOptionPane.showMessageDialog(null, "Não foi possivel encontrar o arquivo da macro " + nomMacros.get(i), "Arquivo não encontrado", JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                }

                if (geraArqsJavaNecessario(nomMacros, Norma)) {
                    String nomeArq = caminhoJava + execXML.getNomMacro() + ".java";
                    this.setEndArq(nomeArq);

                    if (!arqJavaExist(execXML.getNomMacro().trim()) || (objFormIni.getExecXML() != null && execXML.getNomMacro().equals(objFormIni.getExecXML().getNomMacro()))) {

                        trocaStatusLabels(execXML, "Gerando " + execXML.getNomMacro() + ".java");

                        String texto = "";

                        texto += "import javax.swing.JOptionPane;\n"
                                + "import java.util.Formatter;\n"
                                + "import java.io.File;\n"
                                + "import java.lang.reflect.InvocationTargetException;\n"
                                + "import java.lang.reflect.Method;\n"
                                + "import java.net.MalformedURLException;\n"
                                + "import java.net.URL;\n"
                                + "import java.net.URLClassLoader;\n"
                                + "public class " + execXML.getNomMacro() + " {\n";

                        texto += "public static int RetornoBool = -1;\n";
                        texto += "public static int TipoRetorno = -1;\n";
                        texto += "public static int Retorno = 0;\n";

                        String retCodVar = mapeamentoValorVariaveis(Norma, execXML.getCodVariaveis(), execXML.getNumReg(), execXML);
                        if (retCodVar != null) {
                            execXML.setCodVariaveis(retCodVar);

                            texto += execXML.getCodVariaveis() + "\n";
                            texto += Metodo + "\n";
                            texto += Codigo + "\n";
                            texto += "}\n";
                            //tentando criar arquivo
                            try {
                                try (Formatter saida = new Formatter(nomeArq)) {
                                    saida.format(texto);
                                    saida.close();
                                }

                            } //mostrando erro em caso se nao for possivel
                            //gerar arquivo
                            catch (Exception erro) {
                                JOptionPane.showMessageDialog(null, "Arquivo nao pode"
                                        + " ser gerado!", "Erro", 0);
                                return false;
                            }
                        } else {
                            return false;
                        }
                    }
                    return true;

                } else {
                    return false;
                }

            } else {
                return false;
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormIni.setLogSistema("----OpJNorma - salvarArqJava ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormIni.setLogSistema(erro1.toString() + '\n');
            }
            return false;
        }
    }

    //dado os nomes das macros, chama a classe de compilação, para compilar todas as macros necessarias
    public boolean reecompilaMacros(ArrayList<String> nomMacros, String caminhoConfig, String caminhoJava) {
        if (!nomMacros.isEmpty()) {
            String[] macrosCompilar = new String[nomMacros.size()];
            for (int i = 0; i < macrosCompilar.length; i++) {
                macrosCompilar[i] = caminhoJava + nomMacros.get(i) + ".java";
            }
            trocaStatusLabels(objFormIni.getExecXML(), "Preparando compilação...");
            Compilar.main(macrosCompilar, caminhoConfig);
            objFormIni.setLogSistema(Compilar.Erros);
            Compilar.Erros = "";
            return Compilar.compilado;//retorna verdadeiro se compilou e false se deu erro
        }
        return true;
    }

    public boolean geraArqsJavaNecessario(ArrayList<String> nomMacros, ClasseNorma Norma) {
        if (objConfi.detectaCompilador() && !nomMacros.isEmpty()) {
            ExecXML execXML;
            for (int i = 0; !nomMacros.isEmpty();) {

                i = nomMacros.size() - 1;
                try {
                    execXML = new ExecXML();
                    execXML.executarXML(nomMacros.get(i).trim() + ".xml");
                    String codMet = salvaArqRetorno(execXML.getNomMacro(), Integer.parseInt(execXML.getNumReg()), execXML);
                    if (!execXML.getCodJavaMetodo().contains("salvaArqRetorno()")) {
                        execXML.setCodJavaMetodo(execXML.getCodJavaMetodo() + codMet);
                    }
                    //retira os ids dos registradores dos codigos
                    String troca = execXML.getCodJava().replaceAll("#", "");
                    execXML.setCodJava(troca);
                    troca = execXML.getCodJavaMetodo().replaceAll("#", "");
                    execXML.setCodJavaMetodo(troca);
                    troca = execXML.getCodVariaveis().replaceAll("#", "");
                    execXML.setCodVariaveis(troca);

                    String dirJava = System.getProperty("user.dir") + "//Java//";
                    if (!salvaArqJava(execXML, Norma, dirJava)) {
                        return false;
                    } else {
                        nomMacros.remove(i);
                    }

                } catch (ParserConfigurationException | SAXException | IOException ex) {
                    JOptionPane.showMessageDialog(null, "Erro ao ler arquivo XML", "Erro XML", JOptionPane.ERROR_MESSAGE);
                    StackTraceElement erro[] = ex.getStackTrace();
                    objFormIni.setLogSistema("----OpJNorma - geraArqsNecessarios ---- \n\n");
                    for (StackTraceElement erro1 : erro) {
                        objFormIni.setLogSistema(erro1.toString() + '\n');
                    }
                    return false;
                } catch (Exception ex) {
                    StackTraceElement erro[] = ex.getStackTrace();
                    objFormIni.setLogSistema("----OpJNorma - geraArqsNecessarios ---- \n\n");
                    for (StackTraceElement erro1 : erro) {
                        objFormIni.setLogSistema(erro1.toString() + '\n');
                    }
                    return false;
                }

            }

        } else if (!objConfi.detectaCompilador()) {
            return false;
        }
        return true;
    }

    public String salvaArqRetorno(String Nome, int NumReg, ExecXML execXML) {
        String texto = "";
        texto += "public static void salvaArqRetorno(){\n";
        texto += "String nomeArq = \"" + Nome + "-2.xml\";\n"
                + "String texto=\"\";\n"
                + "try{\n"
                + "\n"
                + "\n"
                + "Formatter saida = new Formatter(nomeArq);\n"
                + "texto=\"<Retorno>\\n\";\n"
                + "texto+=\"<TipoRetorno>\"+TipoRetorno+\"</TipoRetorno>\\n\";\n"
                + "texto+=\"<ValRetorno>\"+Retorno+\"</ValRetorno>\\n\";\n";
        texto += "texto+=\"<RetRegs>\\n\";\n";
        for (int i = 0; i < NumReg; i++) {
            texto += "texto+=\"<Regs>\"+";
            texto += " #" + execXML.getRegsUsados().get(i);
            texto += " +\"</Regs>\\n\";\n";
        }
        texto += "             texto+=\"<Regs>\"+RetornoBool+\"</Regs>\";\n";
        texto += "             texto+=\"</RetRegs>\\n\";\n";
        texto += "             texto+=\"</Retorno>\";\n"
                + "            saida.format(texto);\n"
                + "            saida.close();\n"
                + "           }\n"
                + "         //mostrando erro em caso se nao for possivel\n"
                + "         //gerar arquivo\n"
                + "         catch(Exception erro){\n"
                + "           JOptionPane.showMessageDialog(null,\"Arquivo nao pode\"+\n"
                + "                \" ser gerado!\",\"Erro\",0);\n"
                + "         }\n"
                + "}\n ";

        return texto;

    }

    //No momento da execução, troca-se as strings (V1..2..3...) que estão como valor dos registradores
    //pelos valores informados pelo usuario na tela principal
    @SuppressWarnings("empty-statement")
    private String mapeamentoValorVariaveis(ClasseNorma Norma, String CodVariaveis, String NumReg, ExecXML execXML) {
        boolean nomIguais = false;
        int pos = -1;
        String reg;
        try {
            //se o numero de registradores carregados for maior ou igual ao numero de registradores necessarios para a macro

            if (Norma.getValorReg().size() >= Integer.parseInt(NumReg) && execXML.getNomMacro().equals(objFormIni.getExecXML().getNomMacro())) {
                int contador = 0;
                for (int i = 0; i < objFormIni.getObjNorma().getIndexMapePrin().size(); i++) {
                    if (contador < objFormIni.getjTabelaRegs().getRowCount()) {
                        reg = objFormIni.getjTabelaRegs().getValueAt(contador, 0).toString();
                        pos = objFormIni.getObjNorma().getIndexMapePrin().indexOf(reg);
                    }
                    if (CodVariaveis.contains("V" + pos)) {
                        CodVariaveis = CodVariaveis.replaceFirst("V" + pos, Norma.getValorReg().get(i).toString());
                    } else {
                        if (contador < objFormIni.getjTabelaRegs().getRowCount()) {
                            i--;
                        } else {
                            break;
                        }
                    }
                    contador++;

                }

                return CodVariaveis;
            } else if (Norma.getValorReg().size() >= Integer.parseInt(NumReg)) {
                for (int i = 0; i < Integer.parseInt(NumReg); i++) {
                    if (CodVariaveis.contains("V" + i)) {
                        CodVariaveis = CodVariaveis.replaceAll("V" + i, "0");
                    }
                }
                return CodVariaveis;
            } else if (Norma.getValorReg().isEmpty() && Norma.getNomeReg().isEmpty()) {
                for (int i = 0; i < Integer.parseInt(NumReg); i++) {
                    if (CodVariaveis.contains("V" + i)) {
                        CodVariaveis = CodVariaveis.replaceAll("V" + i, "0");
                    }
                }
                return CodVariaveis;

            } else {
                return null;
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormIni.setLogSistema("----OpJNorma - mapeamentoVariaveis ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormIni.setLogSistema(erro1.toString() + '\n');
            }
            return null;
        }

    }

    public void salvarArqCaminhoCompiler(String novoCaminho) {
        String nomeArq = "Caminho_Compiler.txt";
        String texto = "";

        //tentando criar arquivo
        try (Formatter saida = new Formatter(nomeArq)) {
            texto = novoCaminho;
            saida.format(texto);
            saida.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Arquivo de Caminho nao pode ser gerado!", "Erro", 0);
            StackTraceElement erro[] = e.getStackTrace();
            objFormIni.setLogSistema("----OpJNorma - salvarArqCaminhoCompiler ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormIni.setLogSistema(erro1.toString() + '\n');
            }
        }
    }

    public String lerArqCaminhoCompiler() {
        String nomeArq = "Caminho_Compiler.txt";
        String linha = "";
        try (FileReader reader = new FileReader(nomeArq); BufferedReader leitor = new BufferedReader(reader)) {

            linha = leitor.readLine();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Impossivel ler arquivo de Caminho...", "Caminho Compilador", JOptionPane.ERROR_MESSAGE);
            StackTraceElement erro[] = e.getStackTrace();
            objFormIni.setLogSistema("----OpJNorma - lerArqCaminhoCompiler ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormIni.setLogSistema(erro1.toString() + '\n');
            }
        }
        return linha;
    }

    public boolean verificaArqCaminhoCompilerExist() {
        return new File("Caminho_Compiler.txt").exists();

    }

    //dado um codigo, busca todas as macros que estão contidas nele
    private ArrayList<String> buscaMacrosUtilizadas(String codMacro) {
        ArrayList<String> Result = new ArrayList<>();
        try {
            String text = "";
            int i = codMacro.indexOf("//");
            if (i != -1) {
                for (; i < codMacro.length(); i++) {
                    if (codMacro.charAt(i) != '/') {
                        while (i < codMacro.length() && codMacro.charAt(i) != '/' && codMacro.charAt(i) != '(') {

                            if (i < codMacro.length() && codMacro.charAt(i) != '(') {
                                text += codMacro.charAt(i);
                            }
                            i++;

                        }
                        if (!Result.contains(text)) {
                            Result.add(text);
                        }
                        text = "";
                        int aux = codMacro.indexOf("//", i);
                        if (aux > i) {
                            i = aux;
                        } else {
                            i = codMacro.length();
                        }
                    }
                }
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormIni.setLogSistema("----OpJNorma - buscaMacrosUtilizadas ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormIni.setLogSistema(erro1.toString() + '\n');
            }

        }
        return Result;
    }

    //Os arquivos de retorno são excluidos depois da execução
    public void excluiArqsTemps(ArrayList<String> arqExcluir) {
        for (int i = 0; i < arqExcluir.size(); i++) {
            File Arqxml2 = new File(System.getProperty("user.dir") + "\\Java\\" + arqExcluir.get(i).trim() + "-2.xml");
            if (Arqxml2.exists()) {
                if (Arqxml2.delete());
            }

        }

    }

    public void limpaValoresTableRegs(ExecXML execXML) {
        objFormIni.limpaValoreRegs();
        objFormIni.getObjNorma().limpaArrayNomeReg();
        objFormIni.getObjNorma().limpaArrayValorReg();
        objFormIni.setRegsCarreg("0");
        if (!objFormIni.getNomMacro().isEmpty()) {
            if (execXML != null) {

                try {
                    execXML.executarXML(objFormIni.getNomMacro().trim() + ".xml");
                    objFormIni.setMacroExec(execXML.getCodMacro().replaceAll("#", ""));
                } catch (ParserConfigurationException | SAXException | IOException ex) {
                    JOptionPane.showMessageDialog(objFormIni, "Não foi possivel encontrar macro...", "Erro de leitura", JOptionPane.ERROR_MESSAGE);
                    StackTraceElement erro[] = ex.getStackTrace();
                    objFormIni.setLogSistema("----OpJNorma - limpaValoresTableRegs ---- \n\n");
                    for (StackTraceElement erro1 : erro) {
                        objFormIni.setLogSistema(erro1.toString() + '\n');
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(objFormIni, "Não foi possivel encontrar macro...", "Erro de leitura", JOptionPane.ERROR_MESSAGE);
                    StackTraceElement erro[] = ex.getStackTrace();
                    objFormIni.setLogSistema("----OpJNorma - limpaValoresTableRegs ---- \n\n");
                    for (StackTraceElement erro1 : erro) {
                        objFormIni.setLogSistema(erro1.toString() + '\n');
                    }
                }
            }
        }
    }

    private boolean arqJavaExist(String nomMacro) {

        return (new File(objFormIni.getObjNorma().caminhoJava + nomMacro.trim() + ".java").exists());

    }

}
