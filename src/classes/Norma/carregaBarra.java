
package classes.Norma;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JProgressBar;
import telas.Norma.FormIni;
import telas.Norma.SelecMacro;

//Classe que faz manipulação de alguns componentes graficos
public class carregaBarra extends Thread {

    private FormIni objFormIni;
    private SelecMacro objSelecMacro;
    private int cont;
    private JProgressBar BarraStatus;

    public carregaBarra(JProgressBar jBarra, FormIni objPrin) {
        objFormIni = objPrin;
        cont = 0;
        BarraStatus = jBarra;
    }

    public void setCont(int cont) {
        this.cont = cont;
    }

    public void progressoBarra(int Inicio, int Fim, String iniciando) {
        int cont = 0;
        String aux = iniciando;
        for (int i = Inicio; i < Fim; i++) {
            BarraStatus.setValue(i);
            iniciando += ".";
            if (cont < 3) {
                if (objFormIni != null) {
                    objFormIni.setLbExecutando(iniciando);
                }
                cont++;
            } else {
                cont = 0;
                iniciando = aux;
            }

            try {
                Thread.sleep(100);
            } catch (Exception e) {

            }
            if (objFormIni != null && objFormIni.isFinalThread()) {
                i = Fim;
                objFormIni.setJpBarra(i);
            }
        }
    }

    public void pontos(String texto) {
        switch (cont) {
            case 0:
                objFormIni.setLbExecutando(texto + ".");
                cont++;
                break;
            case 1:
                objFormIni.setLbExecutando(texto + "..");
                cont++;
                break;
            default:
                objFormIni.setLbExecutando(texto + "...");
                cont = 0;
                break;
        }
        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            Logger.getLogger(carregaBarra.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pontos(String texto, SelecMacro objSelct) {
        switch (cont) {
            case 0:
                objSelct.setLbStatusExecSelectMac(texto + ".");
                break;
            case 1:
                objSelct.setLbStatusExecSelectMac(texto + "..");
                cont++;
                break;
            default:
                objSelct.setLbStatusExecSelectMac(texto + "...");
                cont = 0;
                break;
        }
    }
}
