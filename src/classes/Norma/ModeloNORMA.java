package classes.Norma;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import telas.Norma.FormIni;

public class ModeloNORMA {

    public static void main(String[] args) throws FileNotFoundException {

        ClasseNorma Norma = new ClasseNorma();

        Norma.carregaArqNomes();//Faz a leitura do arquivo listMacros e armazena em um array dentro da classe
        String dir = System.getProperty("user.dir");
        ClasseNorma.criaPastaEm(dir, "Xmls");
        ClasseNorma.criaPastaEm(dir, "Class");
        ClasseNorma.criaPastaEm(dir, "Java");
        final FormIni Form1 = new FormIni(Norma);

        Form1.setExtendedState(JFrame.MAXIMIZED_BOTH);//Para abrir maximizado
        Form1.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        //Adaptador para o fechamento da janela, matando o processo  
        Form1.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (!Form1.getLogSistema().isEmpty()) {
                    File arq = new File("logSistema.txt");
                    if (arq.exists()) {
                        String erros = Form1.lerArqdeLog();
                        erros += "\n\n" + Form1.getLogSistema();
                        Form1.salvarArqLog(erros);
                    } else {
                        Form1.salvarArqLog(Form1.getLogSistema());
                    }
                }
                System.exit(0);

            }
        });

        Form1.setVisible(true);
    }

}
