package classes.Norma;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;
import telas.Norma.Configurar;

public class Compilar {

    private static final int MASK = 0x000000FF;
    private static final int MAGIC_CLASS = 0xCAFEBABE;

    private static final int CONSTANT_UTF8_INFO = 1;
    private static final int CONSTANT_INTEGER_INFO = 3;
    private static final int CONSTANT_FLOAT_INFO = 4;
    private static final int CONSTANT_LONG_INFO = 5;
    private static final int CONSTANT_DOUBLE_INFO = 6;
    private static final int CONSTANT_CLASS_INFO = 7;
    private static final int CONSTANT_STRING_INFO = 8;
    private static final int CONSTANT_FIELDREF_INFO = 9;
    private static final int CONSTANT_METHODREF_INFO = 10;
    private static final int CONSTANT_INTERFACE_METHODREF_INFO = 11;
    private static final int CONSTANT_NAMEANDTYPE_INFO = 12;
    public static boolean compilado;
    public static String Erros = "";

    /**
     * @param args
     * @param Caminho
     */
    public static void main(String[] args, String Caminho) {

        try {

            System.setProperty("java.home", Caminho);

            DiagnosticCollector diagnostics = new DiagnosticCollector();

            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

            if (compiler != null) {

                StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);

                Iterable compilationUnits = fileManager.getJavaFileObjectsFromStrings(Arrays.asList(args));

                JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, diagnostics, null, null, compilationUnits);

                compilado = task.call();

                Set kinds = new HashSet();
                kinds.add(Kind.CLASS);

                Iterable javaFileObjectIterable = null;

                try {
                    javaFileObjectIterable = fileManager.list(StandardLocation.CLASS_PATH, "", kinds, false);

                } catch (IOException e1) {
                    Erros += "---- Compilar ----" + e1.getMessage();
                }

                if (javaFileObjectIterable != null) {
                    for (Iterator it = javaFileObjectIterable.iterator(); it.hasNext();) {
                        JavaFileObject javaFileObject = (JavaFileObject) it.next();
                        String name = null;
                        try {
                            InputStream in = javaFileObject.openInputStream();
                            name = getClassName(in);

                        } catch (IOException e) {
                            e.getMessage();
                        }

                    }
                }

                try {
                    fileManager.close();

                } catch (IOException e) {

                }
                int i = 0;
                for (Iterator it = diagnostics.getDiagnostics().iterator(); it.hasNext();) {
                    if (i == 0) {
                        Erros += "---- Compilar - Diaginostico ---- ";                        
                        Erros += "\nNome: " + it.next().toString() + "\n";
                        i++;
                    }
                    Diagnostic diagnostic = (Diagnostic) it.next();
                    Erros += ("Tipo de Arquivo: " + diagnostic.getKind().name());
                    Erros += ("\nCódigo: " + diagnostic.getCode());
                    Erros += ("\nLinha: " + diagnostic.getLineNumber());
                    Erros += ("\nColuna: " + diagnostic.getColumnNumber());
                    Erros += ("\nPosição inicial: " + diagnostic.getStartPosition());
                    Erros += ("\nPosição: " + diagnostic.getPosition());
                    Erros += ("\nPosição final: " + diagnostic.getEndPosition());
                }

            } else {
                JOptionPane.showMessageDialog(null, "Compilador Java não encontrado !!!", "Compilador", JOptionPane.ERROR_MESSAGE);
                new Configurar().setVisible(true);

            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro de compilação do arquivo " + args[0] + "...", "Execução", JOptionPane.ERROR_MESSAGE);
            Erros += e.getMessage();
        }

    }

    public static String getClassName(final InputStream inputStream) throws IOException {
        Map mapIndexAndObject = new HashMap();

        byte b[] = new byte[10];

        inputStream.read(b, 0, b.length);

        int aux = (MASK & b[0]) << 24;
        aux = aux | (MASK & b[1]) << 16;
        aux = aux | (MASK & b[2]) << 8;
        aux = aux | MASK & b[3];

        if (aux != MAGIC_CLASS) {
            throw new IllegalArgumentException("O arquivo class inválido! ");
        }

        int constant_pool_count = (MASK & b[8]) << 8 | MASK & b[9];

        for (int i = 1; i < constant_pool_count; i++) {
            int tag = inputStream.read();

            switch (tag) {
                case CONSTANT_UTF8_INFO: {
                    b = new byte[2];
                    inputStream.read(b, 0, b.length);

                    int length = (MASK & b[0]) << 8 | MASK & b[1];

                    b = new byte[length];
                    inputStream.read(b, 0, b.length);

                    Reader reader = new InputStreamReader(new ByteArrayInputStream(b), "UTF-8");

                    char cbuf[] = new char[b.length];
                    reader.read(cbuf);

                    mapIndexAndObject.put(i, new String(cbuf));

                    reader.close();

                }
                ;
                break;
                case CONSTANT_INTEGER_INFO: {
                    advanceInputStream(inputStream, 4);

                }
                ;
                break;
                case CONSTANT_FLOAT_INFO: {
                    advanceInputStream(inputStream, 4);

                }
                ;
                break;
                case CONSTANT_LONG_INFO: {
                    advanceInputStream(inputStream, 8);
                    i++;

                }
                ;
                break;
                case CONSTANT_DOUBLE_INFO: {
                    advanceInputStream(inputStream, 8);
                    i++;

                }
                ;
                break;
                case CONSTANT_CLASS_INFO: {
                    b = new byte[2];
                    inputStream.read(b, 0, b.length);

                    aux = (MASK & b[0]) << 8 | MASK & b[1];

                    mapIndexAndObject.put(i, aux);

                }
                ;
                break;
                case CONSTANT_STRING_INFO: {
                    advanceInputStream(inputStream, 2);

                }
                ;
                break;
                case CONSTANT_FIELDREF_INFO: {
                    advanceInputStream(inputStream, 4);

                }
                ;
                break;
                case CONSTANT_METHODREF_INFO: {
                    advanceInputStream(inputStream, 4);

                }
                ;
                break;
                case CONSTANT_INTERFACE_METHODREF_INFO: {
                    advanceInputStream(inputStream, 4);

                }
                ;
                break;
                case CONSTANT_NAMEANDTYPE_INFO: {
                    advanceInputStream(inputStream, 4);

                }
                ;
                break;
            }
        }

        advanceInputStream(inputStream, 2);

        b = new byte[2];
        inputStream.read(b, 0, b.length);

        aux = (MASK & b[0]) << 8 | MASK & b[1];

        int index = (Integer) mapIndexAndObject.get(aux);

        return (String) mapIndexAndObject.get(index);
    }

    /**
     * Avança InputStream sem obter os bytes.
     *
     * @param in InputStream
     * @param length Quantidade a ser avançada.
     * @throws IOException Erros de I/O.
     */
    private static void advanceInputStream(final InputStream in, final int length) throws IOException {
        for (int i = 0; i < length; i++) {
            in.read();
        }
    }
}
