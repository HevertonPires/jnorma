package classes.Norma;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Formatter;
import javax.swing.JOptionPane;
import telas.Norma.CriaMacro;
import telas.Norma.MapeReg;
import telas.Norma.SelecMacro;

public class OperacoesCriaMacro {

    private CriaMacro objCriaMacro;
    private OperacoesJNorma objOpNorma;
    private ClasseNorma objClasseNorma;

    public OperacoesCriaMacro(CriaMacro objCriaMacro, OperacoesJNorma obj, ClasseNorma Norma) {
        this.objCriaMacro = objCriaMacro;
        this.objOpNorma = obj;
        this.objClasseNorma = Norma;

    }

    public CriaMacro getObjCriaMacro() {
        return objCriaMacro;
    }

    public OperacoesJNorma getObjOpNorma() {
        return objOpNorma;
    }

    public ClasseNorma getObjClasseNorma() {
        return objClasseNorma;
    }

    //dada uma string, verifica se ela é um numero inteiro
    public boolean NumValido(String Num) {
        boolean NumValido = true;
        int Aux = 0;
        try {
            Aux = Integer.parseInt(Num);
            if (Aux < 1) {
                NumValido = false;
            }
        } catch (Exception e) {
            NumValido = false;
        }
        return !(!NumValido || Aux == 0 || Num.trim().equals(""));
    }

    public boolean salvarMacro() {
        try {
            String textoArqRet = "";
            String nomeArq = objClasseNorma.caminhoXmls + objCriaMacro.getNomeMacro() + ".xml";
            String nomeMacro = objCriaMacro.getNomeMacro();
            String texto = "";
            String classejava = "";
            int NumReg = objCriaMacro.getNReg();

            String teste = objCriaMacro.getNomeMacro();

            this.buscaNomesMacros();//lista todas macros usadas na macro que vai ser salva

            //realiza a troca da chamada de macro !nomedamaco();, pelo codigo java nomemacro.nomemacro(parametros);
            String textoJava = objOpNorma.mapeamentoMacros(objCriaMacro.getSintaxJava(), objCriaMacro.getArrayNomMacros(), objCriaMacro.getParametInsertMacro(), objCriaMacro);
            if (textoJava != null) {

                objCriaMacro.getArrayNomMacros().clear();

                if (!teste.isEmpty()) {

                    //tentando criar arquivo
                    try {

                        ArrayList<String> Aux = new ArrayList<>();
                        Aux = (ArrayList<String>) objCriaMacro.getArRenameRegs().clone();
                        //inicio
                        classejava += "public static int[] " + nomeMacro + " (";
                        int NumParam = NumReg;
                        for (int i = 0; i < NumParam; i++) {
                            if (i > 0 && i < NumParam) {
                                classejava += " , ";
                            }
                            classejava += "int #" + Aux.get(i);
                        }
                        classejava += " ){\n";
                        classejava += textoJava + "\n return new int[]{";
                        for (int i = 0; i < NumParam; i++) {
                            if (i > 0 && i < NumParam) {
                                classejava += " , ";
                            }
                            classejava += '#' + Aux.get(i);
                        }
                        classejava += " ,RetornoBool";
                        classejava += "};\n"
                                + "}\n";
                        //Fim
                        //Cria a função java, que vai executar o codigo java que foi convertido do codigo norma

                        classejava += "\npublic static void main(String[] args) {\n";
                        classejava += "int vet[];\n";
                        classejava += "vet = " + nomeMacro + "(";
                        for (int i = 0; i < Aux.size(); i++) { //passagem de parametros na chama da função na main
                            if (i > 0 && i < Aux.size()) {
                                classejava += " , ";
                            }
                            classejava += '#' + Aux.get(i);
                        }

                        classejava += " );\n";

                        for (int i = 0; i < Aux.size(); i++) {
                            classejava += '#' + Aux.get(i) + " = vet[" + i + "];\n";
                        }
                        classejava += "RetornoBool = vet[" + Aux.size() + "];\n";
                        classejava += "salvaArqRetorno();\n";

                        classejava += "}\n";

                        Formatter saida = new Formatter(nomeArq);

                        texto = "<Macro>\n";
                        texto += "<NomeMacro>" + objCriaMacro.getNomeMacro() + "</NomeMacro>\n";

                        texto += "<Variaveis>\n";
                        for (int i = 0; i < objCriaMacro.getNReg(); i++) {
                            texto += "public static int #" + objCriaMacro.getArRenameRegs().get(i) + " = V" + i + ";\n";
                        }

                        Aux.clear();

                        texto += "</Variaveis>\n";
                        texto += "<CodPortEstrut>\n" + objCriaMacro.getSintaxNorma() + objOpNorma.getNomMacrosUsadas() + "</CodPortEstrut>\n";
                        texto += "<CodMacroJava>\n" + classejava + "</CodMacroJava>\n";
                        texto += "<CodJavaMetodo>\n" + objCriaMacro.getMetodo() + "</CodJavaMetodo>\n";
                        texto += "<TipoMetodo>" + objCriaMacro.getTipoMetodo() + "</TipoMetodo>";
                        texto += "<NumReg>" + NumReg + "</NumReg>\n";
                        texto += "<varRetorno>" + objCriaMacro.getRegRetorno() + "</varRetorno>\n";
                        for (int i = 0; i < objCriaMacro.getArRenameRegs().size(); i++) {
                            texto += "<Regs>" + objCriaMacro.getArRenameRegs().get(i) + "</Regs>\n";
                        }
                        texto += "</Macro>";
                        objOpNorma.setNomMacrosUsadas("");
                        saida.format(texto);
                        saida.close();

                        JOptionPane.showMessageDialog(null, "Macro [" + nomeMacro + "] salva com sucesso!", "Salvar", 1);
                        return true;
                    } catch (FileNotFoundException | HeadlessException erro) {
                        JOptionPane.showMessageDialog(null, "Macro não pode ser salva!", "Mensagem de erro", JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Digite o nome da macro.", "Inserir Nome", JOptionPane.WARNING_MESSAGE);
                }
                objCriaMacro.getObjClasNorma().getNomeMacro().add(teste);
            } else {
                JOptionPane.showMessageDialog(null, "Macro não pode ser salva!", "Mensagem de erro", JOptionPane.ERROR_MESSAGE);
                return false;
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objCriaMacro.getObjFormPrincipal().setLogSistema("----OpCriaMacro - SalvarMacro ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objCriaMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
            return false;
        }
        return true;
    }

    //dado um nome, verifica se essa macro existe 
    public boolean verificaMacroExiste(String nomeMacro) {
        String linha = null;
        File arq = new File(objClasseNorma.caminhoXmls);
        if (arq.exists()) {
            File[] arquivos = arq.listFiles();
            for (File arquivo : arquivos) {
                if (arquivo.exists()) {
                    linha = arquivo.getName().replaceAll(".xml", "");
                    if (linha.toUpperCase().equals(nomeMacro.toUpperCase())) {
                        return true;
                    }
                } else {
                    return false;
                }

            }

        }
        return false;

    }

    //função usada para criar o metodo que vai manipular a macro que é usada como condicional
    public String criarMetodo(String nomMacro, ArrayList<String> Parametros, String TipoMetodo, ExecXML execXML) {
        try {
            String cJava = "";

            int NumParam = macronumparam.MacroNumParam.NumParamMacros(nomMacro);//pega o numero de paramentros
            if (NumParam != -1) {
                cJava += "\npublic static " + TipoMetodo + " " + nomMacro + "(";
                for (int i = 0; i < NumParam; i++) {
                    if (i > 0) {
                        cJava += " , ";
                    }
                    cJava += "int #" + Parametros.get(i);
                }

                cJava += " ){\n";
                cJava += "int Vet[];\n"
                        + "Vet = " + nomMacro + "." + nomMacro + "(";
                for (int i = 0; i < NumParam; i++) {
                    if (i > 0) {
                        cJava += " , ";
                    }
                    cJava += '#' + Parametros.get(i);
                }
                cJava += " );\n";
                if (TipoMetodo.equals("boolean")) {//se o retorno da macro for bool
                    cJava += "if(Vet[" + (NumParam) + "] == 0){\n"//pega a ultima posição do vetor retornado que guarda o retorno Bool
                            + "return false;\n }\n"
                            + "else{ \n"
                            + "return true;\n }\n }\n";
                } else {
                    int pos = execXML.getRegsUsados().indexOf(execXML.getVarsRetorno().get(0));//pega a posição do registrador de retorno
                    cJava += "return Vet[" + pos + "];\n}\n";
                }

            }

            return cJava;

        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objCriaMacro.getObjFormPrincipal().setLogSistema("----OpCriaMacro - CriaMetodo ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objCriaMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
            return null;
        }
    }

    public void replaceText(String sai, String ent, SelecMacro objSelMacro, MapeReg objMapR, ExecXML execXML) {
        try {
            if (objSelMacro.isMacroCond()) {//Macro como condicional
                String troca;

                troca = execXML.getCodJavaMetodo();
                troca = troca.replaceAll('#' + sai + " ", ent);
                //mapeia o registrador selecionado pelo primeiro registrador da macro

                int pos = objCriaMacro.getArRenameRegs().indexOf(ent);//pega a posição do registrador no array que foi criado no momento da criação da macro
                try {
                    //inseri no array com base na posição
                    if (!(pos > objCriaMacro.getParametMacroCond().size())) {
                        objCriaMacro.getParametMacroCond().add(pos, ent);
                    } else {
                        objCriaMacro.getParametMacroCond().add(ent);
                    }

                    if (objMapR.getControlReplace() == 1) {
                        String cod = criarMetodo(execXML.getNomMacro(), objCriaMacro.getParametMacroCond(), execXML.getTipoMetodo(), execXML);
                        objCriaMacro.setMetodo(cod);//quando ctrl replace == 1 eu chamo criar macro...
                        objCriaMacro.getParametMacroCond().clear();
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(objCriaMacro, "Verifique se numeros de Registradores estão certo !!!", "Erro Acesso", JOptionPane.ERROR_MESSAGE);
                }
            } else if (objSelMacro != null) {//Inserindo Macro

                //mapeamento dos codigos
                String troca = execXML.getCodJava().replaceAll('#' + sai + " ", ent);
                String troca2 = execXML.getCodMacro().replaceAll(sai + " ", ent);

                //inseri os parametros da macro inserida
                if (!objCriaMacro.isEntrou() && Integer.parseInt(execXML.getNumReg()) <= objMapR.getControlReplace()) {
                    objCriaMacro.getParametInsertMacro().add(ent);
                    objCriaMacro.setEntrou(true);
                } else {
                    objCriaMacro.getParametInsertMacro().add(ent);
                }
                execXML.setCodJava(troca);
                execXML.setCodMacro(troca2);

                if (objMapR.getControlReplace() == 1) {
                    objCriaMacro.setEntrou(false);
                }

            } else {
                String troca;
                troca = objCriaMacro.getSintaxJava();
                troca = troca.replaceAll('#' + sai + " ", ent);
                objCriaMacro.setSintaxJava(troca);

                troca = objCriaMacro.getSintaxNorma();
                troca = troca.replaceAll("#" + sai + " ", ent);
                objCriaMacro.setSintaxNormaExib(troca);
                objCriaMacro.setSintaxNorma(troca);

            }
        } catch (HeadlessException | NumberFormatException e) {
            StackTraceElement erro[] = e.getStackTrace();
            objCriaMacro.getObjFormPrincipal().setLogSistema("----OpCriaMacro - replaceText ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objCriaMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
        }
    }

    public void fechaInstrucao(String Condicao) {

        objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + "\n}\n");
        objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText() + " )\n");
        objCriaMacro.setSintaxNorma(objCriaMacro.getSintaxNorma() + " )\n");

        if (!objCriaMacro.getPiInsAbertas().isEmpty()) {
            objCriaMacro.getPiInsAbertas().pop();
        }
        objCriaMacro.getPiInsFechadas().push("Fim-" + Condicao);

        objCriaMacro.getEstadoCodJava().add(objCriaMacro.getSintaxJava());
        objCriaMacro.getEstadoCodNorma().add(objCriaMacro.getSintaxNorma());

        objCriaMacro.getInstAbertas().setText(String.valueOf(objCriaMacro.getPiInsAbertas().size()));//atualiza os labels da tela CriaMacro
        objCriaMacro.getInstFinal().setText(String.valueOf(objCriaMacro.getPiInsFechadas().size()));
        objCriaMacro.getLbcontInsAbertas().setText(String.valueOf(objCriaMacro.getPiInsAbertas().size()));
        objCriaMacro.getInstAbertas().repaint();
        objCriaMacro.getInstFinal().repaint();
        objCriaMacro.getLbcontInsAbertas().repaint();

        JOptionPane.showMessageDialog(objCriaMacro, "Instrução Finalizada: Fim - " + Condicao, "Finalizando Instrução", JOptionPane.INFORMATION_MESSAGE);

    }

    public void insertReturn(String V, int T) {
        if (T == 0) {
            objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + "TipoRetorno = 0\n;");
            objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + "Retorno = " + objCriaMacro.getValorSelect() + " ;\n");
        }
        objCriaMacro.getEstadoCodJava().add(objCriaMacro.getSintaxJava());
        objCriaMacro.getEstadoCodNorma().add(objCriaMacro.getSintaxNorma());
    }

    public void insertReturn(int tipoRetorno) {
        if (tipoRetorno == 1) {//Verdadeiro
            objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + "TipoRetorno = 2;\n");
            objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + "RetornoBool = 1;\n");
            if (!objCriaMacro.getPiInsAbertas().isEmpty() && (objCriaMacro.getPiInsAbertas().contains("Ate") || objCriaMacro.getPiInsAbertas().contains("Enquanto"))) {
                objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + "break;\n");
            }
            objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText() + "Verdadeiro \n");
            objCriaMacro.setSintaxNorma(objCriaMacro.getSintaxNorma() + "Verdadeiro \n");
            objCriaMacro.getRetornosBool().add(true);
        } else {//Falso
            objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + "TipoRetorno = 2;\n");
            objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + "RetornoBool = 0;\n");
            if (!objCriaMacro.getPiInsAbertas().isEmpty() && (objCriaMacro.getPiInsAbertas().contains("Ate") || objCriaMacro.getPiInsAbertas().contains("Enquanto"))) {
                objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + "break;\n");
            }
            objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText() + "Falso \n");
            objCriaMacro.setSintaxNorma(objCriaMacro.getSintaxNorma() + "Falso \n");
            objCriaMacro.getRetornosBool().add(false);
        }
        objCriaMacro.getEstadoCodJava().add(objCriaMacro.getSintaxJava());
        objCriaMacro.getEstadoCodNorma().add(objCriaMacro.getSintaxNorma());
    }

    public boolean verificaMacro_em_Condicao(String valorSelect) {
        if (objCriaMacro != null && objCriaMacro.getObjInsertCondi() != null && objCriaMacro.getObjInsertCondi().isMacroCond()) {
            return valorSelect != null && valorSelect.contains("(") && valorSelect.contains(")");
        }
        return false;
    }

    public void regTesteDifZero(String valorSelct, int test) {
        String Adicionar = null, id = "";
        if (!verificaMacro_em_Condicao(valorSelct)) {
            id = "#";
        }
        if (objCriaMacro.getInsertCond() == 1) {
            objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText().replaceAll("#", "") + "nao" + valorSelct + " = 0");
            objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText().replaceAll("#", ""));
            objCriaMacro.setSintaxNorma(objCriaMacro.getSintaxNorma() + "nao " + id + valorSelct + " = 0");
            if ("Se".equals(objCriaMacro.getGuardaLoopsCond().get(objCriaMacro.getGuardaLoopsCond().size() - 1)) || "Enquanto".equals(objCriaMacro.getGuardaLoopsCond().get(objCriaMacro.getGuardaLoopsCond().size() - 1))) {
                objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + id + valorSelct + "!=0");
                objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + ")");
            } else if ("Ate".equals(objCriaMacro.getGuardaLoopsCond().get(objCriaMacro.getGuardaLoopsCond().size() - 1))) {
                objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + id + valorSelct + "== 0");
                objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + "; )");
            }
            objCriaMacro.setInsertCond(0);
            if (objCriaMacro.getObjInsertCondi() != null && !objCriaMacro.getObjInsertCondi().isClikVoltar()) {
                objCriaMacro.getEstadoCodJava().add(objCriaMacro.getSintaxJava());
                objCriaMacro.getEstadoCodNorma().add(objCriaMacro.getSintaxNorma());
            }
        }
    }

    public void regTesteZero(String valorSelect, int test) {
        String Adicionar = null, id = "";

        if (!verificaMacro_em_Condicao(valorSelect)) {
            id = "#";
        }
        if (objCriaMacro.getInsertCond() == 1) {
            objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText().replaceAll("#", "") + valorSelect + " = 0");
            objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText().replaceAll("#", ""));
            objCriaMacro.setSintaxNorma(objCriaMacro.getSintaxNorma() + id + valorSelect + " = 0");
            if ("Se".equals(objCriaMacro.getGuardaLoopsCond().get(objCriaMacro.getGuardaLoopsCond().size() - 1)) || "Enquanto".equals(objCriaMacro.getGuardaLoopsCond().get(objCriaMacro.getGuardaLoopsCond().size() - 1))) {
                objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + id + valorSelect + " == 0");
                objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + ")");
            } else if ("Ate".equals(objCriaMacro.getGuardaLoopsCond().get(objCriaMacro.getGuardaLoopsCond().size() - 1))) {
                objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + id + valorSelect + " != 0");
                objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + "; )");
            }
            objCriaMacro.setInsertCond(0);
            if (objCriaMacro.getObjInsertCondi() != null && !objCriaMacro.getObjInsertCondi().isClikVoltar()) {
                objCriaMacro.getEstadoCodJava().add(objCriaMacro.getSintaxJava());
                objCriaMacro.getEstadoCodNorma().add(objCriaMacro.getSintaxNorma());
            }
        }
    }

    public void regTestesComOperadoresLogicos(String text) {
        objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText() + text.replaceAll("#", ""));
        objCriaMacro.setSintaxNorma(objCriaMacro.getSintaxNorma() + text);

        if (objCriaMacro.getPiInsAbertas().lastElement().equals("Ate")) {
            text = text.replaceAll(" E ", "::");//for inverte as condiçoes && vira || e vice versa
            text = text.replaceAll(" OU ", "@");
            text = text.replaceAll("=", " != ");
            text = text.replaceAll("nao", "!");
            text = text.replaceAll("false", "true");

            objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + text + "; )");
        } else {
            text = text.replaceAll(" E ", " @ ");
            text = text.replaceAll(" OU ", " :: ");
            text = text.replaceAll("=", " == ");
            text = text.replaceAll("nao", "!");

            objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + text + " )");
        }
        if (objCriaMacro.getObjInsertCondi() != null && !objCriaMacro.getObjInsertCondi().isClikVoltar()) {
            objCriaMacro.getEstadoCodJava().add(objCriaMacro.getSintaxJava());
            objCriaMacro.getEstadoCodNorma().add(objCriaMacro.getSintaxNorma());
        }
    }

    public void testeCondicaoBoolean(String cond) {
        if (objCriaMacro != null && objCriaMacro.getPiInsAbertas().lastElement().equals("Ate")) {
            if (cond.equals("false")) {
                objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText() + objCriaMacro.getValorSelect() + " = false");
                objCriaMacro.setSintaxNorma(objCriaMacro.getSintaxNorma() + objCriaMacro.getValorSelect() + " = false");
                objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + objCriaMacro.getValorSelect() + " == false; )");
            } else {
                objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText() + "nao" + objCriaMacro.getValorSelect() + " = false");
                objCriaMacro.setSintaxNorma(objCriaMacro.getSintaxNorma() + "nao " + objCriaMacro.getValorSelect() + " = false");
                objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + objCriaMacro.getValorSelect() + " != false; )");
            }
        } else if (cond.equals("false")) {
            objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText() + objCriaMacro.getValorSelect() + " = false");
            objCriaMacro.setSintaxNorma(objCriaMacro.getSintaxNorma() + objCriaMacro.getValorSelect() + " = false");
            objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + objCriaMacro.getValorSelect() + " == false )");
        } else {
            objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText() + " nao" + objCriaMacro.getValorSelect() + " = false");
            objCriaMacro.setSintaxNorma(objCriaMacro.getSintaxNorma() + " nao " + objCriaMacro.getValorSelect() + " = false");
            objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + objCriaMacro.getValorSelect() + " != false )");
        }
        objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText().replaceAll("#", ""));
        objCriaMacro.getEstadoCodJava().add(objCriaMacro.getSintaxJava());
        objCriaMacro.getEstadoCodNorma().add(objCriaMacro.getSintaxNorma());

    }

    public void regMaisUm() {
        objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText() + objCriaMacro.getValorSelect() + " + 1\n");
        objCriaMacro.setSintaxNorma(objCriaMacro.getSintaxNorma() + "#" + objCriaMacro.getValorSelect() + " + 1\n");
        objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + '#' + objCriaMacro.getValorSelect() + " ++;\n");
        objCriaMacro.getEstadoCodJava().add(objCriaMacro.getSintaxJava());
        objCriaMacro.getEstadoCodNorma().add(objCriaMacro.getSintaxNorma());
    }

    public void regMenosUm() {
        objCriaMacro.getSintaxNormaExib().setText(objCriaMacro.getSintaxNormaExib().getText() + objCriaMacro.getValorSelect() + " - 1\n ");
        objCriaMacro.setSintaxNorma(objCriaMacro.getSintaxNorma() + "#" + objCriaMacro.getValorSelect() + " - 1\n ");
        objCriaMacro.setSintaxJava(objCriaMacro.getSintaxJava() + '#' + objCriaMacro.getValorSelect() + " --;\n");
        objCriaMacro.getEstadoCodJava().add(objCriaMacro.getSintaxJava());
        objCriaMacro.getEstadoCodNorma().add(objCriaMacro.getSintaxNorma());
    }

    private void buscaNomesMacros() {
        try {
            String codMacro = objCriaMacro.getSintaxNormaExib().getText();
            String text = "";
            int i = codMacro.indexOf('!');
            if (i != -1) {
                for (; i < codMacro.length(); i++) {
                    if (codMacro.charAt(i) == '!') {
                        while (i < codMacro.length() && codMacro.charAt(i) != ' ' && codMacro.charAt(i) != '\n') {
                            i++;
                            if (i < codMacro.length() && codMacro.charAt(i) != '(') {
                                text += codMacro.charAt(i);
                            }
                        }
                        objCriaMacro.getArrayNomMacros().add(text);
                        text = "";
                        i--;
                        int aux = codMacro.indexOf("!", i);
                        if (aux > i) {
                            i = aux;
                            i--;
                        } else {
                            i = codMacro.length();
                        }
                    }
                }
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objCriaMacro.getObjFormPrincipal().setLogSistema("----OpCriaMacro - buscaNomesMacros ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objCriaMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
        }
    }

}
