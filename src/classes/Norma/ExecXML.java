package classes.Norma;

import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExecXML {

    private String CodMacro;
    private String CodVariaveis;
    private String CodJava;
    private String TipoMetodo;
    private String NumReg;
    private String NomMacro;
    private String NomMacrosUsadas;
    private String CodJavaMetodo;
    private ArrayList<String> varsRetorno;
    private ArrayList<String> RegsUsados;

    public String getNomMacrosUsadas() {
        return NomMacrosUsadas;
    }

    public String getCodJavaMetodo() {
        return CodJavaMetodo;
    }

    public ArrayList getVarsRetorno() {
        return varsRetorno;
    }

    public void setVarsRetorno(ArrayList varsRetorno) {
        this.varsRetorno = varsRetorno;
    }

    public void setCodJavaMetodo(String CodJavaMetodo) {
        this.CodJavaMetodo = CodJavaMetodo;
    }

    public String getCodMacro() {
        return CodMacro;
    }

    public void setCodMacro(String CodMacro) {
        this.CodMacro = CodMacro;
    }

    public String getCodVariaveis() {
        return CodVariaveis;
    }

    public void setCodVariaveis(String CodVariaveis) {
        this.CodVariaveis = CodVariaveis;
    }

    public String getCodJava() {
        return CodJava;
    }

    public void setCodJava(String CodJava) {
        this.CodJava = CodJava;
    }

    public String getTipoMetodo() {
        return TipoMetodo;
    }

    public ArrayList<String> getRegsUsados() {
        return RegsUsados;
    }

    public void setTipoMetodo(String TipoMetodo) {
        this.TipoMetodo = TipoMetodo;
    }

    public String getNumReg() {
        return NumReg;
    }

    public void setNumReg(String NumReg) {
        this.NumReg = NumReg;
    }

    public String getNomMacro() {
        return NomMacro;
    }

    public void setNomMacro(String NomMacro) {
        this.NomMacro = NomMacro;
    }

    public void executarXML(String nome) throws ParserConfigurationException, SAXException, IOException, Exception {
        
        setCodJavaMetodo("");
        RegsUsados = new ArrayList<>();
        varsRetorno = new ArrayList<>();
        nome = "file:///" + System.getProperty("user.dir") + "\\Xmls\\" + nome;
        DocumentBuilderFactory bF = DocumentBuilderFactory.newInstance();

        DocumentBuilder dBuilder = bF.newDocumentBuilder();
        Document d = dBuilder.parse(nome);
        d.normalize();

        NodeList rootNodes = d.getElementsByTagName("Macro");
        Node root = rootNodes.item(0);
        Element rootElement = (Element) root;

        NodeList ListMacro = rootElement.getElementsByTagName("NomeMacro");
        Node NMacro = ListMacro.item(0);
        Element NomeMacro = (Element) NMacro;

        NodeList ListCodMacro = rootElement.getElementsByTagName("CodPortEstrut");
        Node CoMacro = ListCodMacro.item(0);
        Element CodiMacro = (Element) CoMacro;
        CodMacro = CoMacro.getTextContent();

        NodeList ListCodVar = rootElement.getElementsByTagName("Variaveis");
        Node CoJavaVar = ListCodVar.item(0);
        Element CodVar = (Element) CoJavaVar;
        CodVariaveis = CoJavaVar.getTextContent();

        NodeList ListCodJava = rootElement.getElementsByTagName("CodMacroJava");
        Node CoJavaMacro = ListCodJava.item(0);
        Element CodiJva = (Element) CoJavaMacro;
        CodJava = CoJavaMacro.getTextContent();

        NodeList listCodJavaMet = rootElement.getElementsByTagName("CodJavaMetodo");
        Node CoMet = listCodJavaMet.item(0);
        Element CodiJvaMet = (Element) CoMet;
        CodJavaMetodo = CoMet.getTextContent();

        NodeList TipoMet = rootElement.getElementsByTagName("TipoMetodo");
        Node CoTipoMetodo = TipoMet.item(0);
        Element TipMet = (Element) CoTipoMetodo;
        this.TipoMetodo = CoTipoMetodo.getTextContent();

        NodeList ListRegist = rootElement.getElementsByTagName("NumReg");
        Node NumeroReg = ListRegist.item(0);
        Element NReg = (Element) NumeroReg;
        NumReg = NumeroReg.getTextContent();

        NodeList varRet = rootElement.getElementsByTagName("ret");
        for (int i = 0; i < varRet.getLength(); i++) {
            Element ValorReg = (Element) varRet.item(i);
            this.varsRetorno.add(ValorReg.getTextContent());
        }
        
        NodeList RetRegs = rootElement.getElementsByTagName("Regs");
        for (int i = 0; i < RetRegs.getLength(); i++) {
            Element ValorReg = (Element) RetRegs.item(i);
            this.RegsUsados.add(ValorReg.getTextContent());
        }

        NomMacro = NomeMacro.getTextContent();

    }
}
