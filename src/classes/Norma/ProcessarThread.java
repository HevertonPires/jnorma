package classes.Norma;

import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import telas.Norma.FormIni;

public class ProcessarThread extends Thread {

    private FormIni objFormIni;
    private int cont;
    private carregaBarra objCarrBarra;

    public ProcessarThread(FormIni Aces) {
        objFormIni = Aces;
        objCarrBarra = new carregaBarra(objFormIni.getJpBarra(), objFormIni);
    }

    public carregaBarra getObjCarrBarra() {
        return objCarrBarra;
    }

    @Override
    @SuppressWarnings("empty-statement")

    public void run() {
        try {
            String caminhoJava = System.getProperty("user.dir") + "\\Java\\";

            objCarrBarra.start();
            objCarrBarra.progressoBarra(2, 7, "Iniciando");
            objFormIni.getObjOpNorma().getArqExcluir().clear();
            objFormIni.setLbExecutando("Gerando Arquivo Java...");
            objCarrBarra.progressoBarra(8, 16, "Gerando Arquivo Java");
            if (objFormIni.getObjOpNorma().salvaArqJava(objFormIni.getExecXML(), objFormIni.getObjNorma(), caminhoJava)) {

                if (clickCancelar()) {
                    return;
                }
                objFormIni.setLbExecutando("Arquivo Java gerado com sucesso...");

                objCarrBarra.progressoBarra(17, 19, "Arquivo Java gerado com sucesso");

                String caminhoCompiler = objFormIni.getObjOpNorma().lerArqCaminhoCompiler();

                if (clickCancelar()) {
                    return;
                }

                if (!caminhoCompiler.isEmpty()) {
                    objFormIni.getObjConfig().setJtCaminhoComp(caminhoCompiler);
                }

                if (clickCancelar()) {
                    return;
                }
                //gera os arquivos .class necessarios
                if (objFormIni.getObjOpNorma().reecompilaMacros(objFormIni.getObjOpNorma().getArqExcluir(), objFormIni.getObjConfig().getJtCaminhoComp(), caminhoJava)) {

                    int valor = 20;
                    while (valor < 29) {
                        if (valor < 25) {
                            objCarrBarra.progressoBarra(valor, valor + 3, "Compilando");
                            valor += 3;
                        } else {
                            objCarrBarra.pontos("Compilando");
                            valor++;
                        }
                    }

                    if (clickCancelar()) {
                        return;
                    }
                    objFormIni.setLbExecutando("Compilando...");
                    objCarrBarra.progressoBarra(30, 35, "Compilando");
                    try {
                        //executa macro
                        Process procExec = Runtime.getRuntime().exec("java " + objFormIni.getNomMacro(), new String[]{""}, new File(caminhoJava));

                        if (clickCancelar()) {
                            return;
                        }
                        objFormIni.setLbExecutando("Preparando para executar Macro...");

                        objCarrBarra.progressoBarra(36, 45, "Executando Macro");

                        if (clickCancelar()) {
                            return;
                        }
                        valor = 45;

                        while (procExec.isAlive()) {//enquanto estiver executando a macro

                            if (clickCancelar()) {
                                return;
                            }
                            if (valor < 60) {
                                objCarrBarra.progressoBarra(valor, valor + 3, "Executando");
                                valor += 4;
                            } else {
                                objCarrBarra.pontos("Executando");
                            }

                        }
                        if (valor < 60) {
                            objCarrBarra.progressoBarra(valor, 59, "Terminado execução");
                        }
                        if (procExec.exitValue() == 0) {
                            ClasseNorma.moveArquivo();
                            objFormIni.setLbExecutando("Gerando Arquivo de Retorno...");
                            objCarrBarra.progressoBarra(60, 65, "Gerando Arquivo de Retorno");
                            if (clickCancelar()) {
                                procExec.destroy();
                                return;
                            }
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException ex) {

                            }
                            objFormIni.setLbExecutando("Arquivo gerado com sucesso...");

                            if (clickCancelar()) {
                                procExec.destroy();
                                return;
                            }
                            objCarrBarra.progressoBarra(66, 71, "Arquivo gerado com sucesso");
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException ex) {

                            }

                            if (clickCancelar()) {
                                procExec.destroy();
                                return;
                            }
                            objFormIni.setLbExecutando("Lendo arquivo de retorno...");

                            objFormIni.getObjOpNorma().leArqRetorno(objFormIni.getObjOpNorma().getNomeArq());

                            objCarrBarra.progressoBarra(72, 80, "Lendo arquivo de retorno");
                            if (!objFormIni.getObjOpNorma().isErro()) {

                                if (clickCancelar()) {
                                    procExec.destroy();
                                    return;
                                }

                                objCarrBarra.progressoBarra(81, 84, "Verificando retorno");
                                objCarrBarra.progressoBarra(85, 87, "Preparando exibição");
                                objCarrBarra.progressoBarra(87, 100, "Finalizando");
                                objFormIni.setJpBarra(100);
                                objFormIni.setLbExecutando("Finalizado !!!");

                                DefaultTableModel model = (DefaultTableModel) objFormIni.getjTabelaRegs().getModel();

                                if (clickCancelar()) {
                                    procExec.destroy();
                                    return;
                                }

                                if (objFormIni.getObjOpNorma().getTipoRetorno() != 2) {
                                    int j = 0;
                                    
                                    for (int i = 0; i < objFormIni.getjTabelaRegs().getRowCount(); i++) {
                                        String reg = objFormIni.getjTabelaRegs().getValueAt(i, 0).toString();
                                        int pos = objFormIni.getObjNorma().getIndexMapePrin().indexOf(reg);
                                        if (pos != -1) {
                                            model.setValueAt(objFormIni.getObjOpNorma().getValorRegs().get(pos), i, 1);

                                        }else{
                                            model.setValueAt("0", i, 1);
                                        }
                                    }
                                } else if (objFormIni.getObjOpNorma().getValorRegs().get(objFormIni.getObjOpNorma().getValorRegs().size() - 1) == 1) {
                                    model.setValueAt("1", 0, 1);//Verdadeiro

                                    for (int i = 1; i < model.getRowCount(); i++) {
                                        model.setValueAt(objFormIni.getObjOpNorma().getValorRegs().get(i), i, 1);
                                    }
                                } else {
                                    model.setValueAt("0", 0, 1);//Falso

                                    for (int i = 1; i < model.getRowCount(); i++) {
                                        model.setValueAt(objFormIni.getObjOpNorma().getValorRegs().get(i), i, 1);
                                    }
                                }

                                if (objFormIni.getObjOpNorma().getTipoRetorno() == 2) {

                                    if (objFormIni.getObjOpNorma().getValorRegs().get(objFormIni.getObjOpNorma().getValorRegs().size() - 1).equals(1)) {
                                        JOptionPane.showMessageDialog(null, "Resultado da Execução da Macro " + objFormIni.getObjOpNorma().getNomeArq() + " : VERDADEIRO", "Retorno Booleano", JOptionPane.INFORMATION_MESSAGE);
                                    } else if (objFormIni.getObjOpNorma().getValorRegs().get(objFormIni.getObjOpNorma().getValorRegs().size() - 1).equals(0)) {
                                        JOptionPane.showMessageDialog(null, "Resultado da Execução da Macro " + objFormIni.getObjOpNorma().getNomeArq() + " : FALSO", "Retorno Booleano", JOptionPane.INFORMATION_MESSAGE);
                                    }
                                }
                                if (clickCancelar()) {
                                    procExec.destroy();
                                    return;
                                }

                                objFormIni.getObjOpNorma().excluiArqsTemps(objFormIni.getObjOpNorma().getArqExcluir());
                                objFormIni.getObjOpNorma().getArqExcluir().clear();
                                objFormIni.getBtnCancelExec().setVisible(false);
                                objFormIni.getBtnReexecutar().setVisible(true);

                            } else {
                                objFormIni.executandoMacroErro();
                            }
                        } else {
                            JOptionPane.showMessageDialog(objFormIni, "Falha na execução da macro !!!", "Execução", JOptionPane.ERROR_MESSAGE);

                        }

                    } catch (IOException | HeadlessException ex) {
                        JOptionPane.showMessageDialog(objFormIni, "Falha no processo de execução do arquivo !!!", "Geração do arquivo java", JOptionPane.ERROR_MESSAGE);
                        StackTraceElement erro[] = ex.getStackTrace();
                        objFormIni.setLogSistema("----OpJNorma - Erro de execução ---- \n\n");
                        for (StackTraceElement erro1 : erro) {
                            objFormIni.setLogSistema(erro1.toString() + '\n');
                        }
                    }
                    objFormIni.getObjOpNorma().getValorRegs().clear();

                } else {
                    JOptionPane.showMessageDialog(objFormIni, "Falha na compilação do arquivo !!!", "Compilação", JOptionPane.ERROR_MESSAGE);
                    new File(caminhoJava + objFormIni.getNomMacro() + ".java").delete();
                    objFormIni.getBtnCancelExec().setVisible(false);
                    objFormIni.setLbExecutando("Falha na compilação !!!");
                }

            } else {
                objFormIni.setLbExecutando("Não foi possivel criar aquivo Java");
                objFormIni.getBtnCancelExec().setVisible(false);
                objFormIni.getBtnLimpaEstado().setEnabled(true);
                return;
            }
            objFormIni.getBtnExcluirMacro().setEnabled(true);
            objFormIni.getBtnLimpaEstado().setEnabled(true);
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormIni.setLogSistema("----OpJNorma - Tela principal ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormIni.setLogSistema(erro1.toString() + '\n');
            }
        }
    }

    private boolean clickCancelar() {
        if (objFormIni.isFinalThread()) {
            objFormIni.operacoesFinalThread();
            this.interrupt();
            return true;
        }
        return false;
    }

}
