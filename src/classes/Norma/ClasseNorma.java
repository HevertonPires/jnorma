package classes.Norma;

import java.io.File;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class ClasseNorma {
    private ArrayList<String> indexMapePrin = new ArrayList<>();
    private ArrayList ValorReg = new ArrayList();
    private ArrayList<String> NomeReg = new ArrayList();
    private ArrayList<String> NomeMacro = new ArrayList();
    public final String caminhoXmls = System.getProperty("user.dir") + "\\Xmls\\";
    public final String caminhoClass = System.getProperty("user.dir") + "\\Class\\";
    public final String caminhoJava = System.getProperty("user.dir") + "\\Java\\";

    public ArrayList getValorReg() {
        return ValorReg;
    }

    public void setValorReg(ArrayList ValorReg) {
        this.ValorReg = ValorReg;
    }

    public ArrayList<String> getIndexMapePrin() {
        return indexMapePrin;
    }

    public void setIndexMapePrin(ArrayList<String> indexMapePrin) {
        this.indexMapePrin = indexMapePrin;
    }

    public ArrayList<String> getNomeReg() {
        return NomeReg;
    }

    public void setNomeReg(ArrayList<String> NomeReg) {
        this.NomeReg = NomeReg;
    }

    public ArrayList<String> getNomeMacro() {
        return NomeMacro;
    }

    public void setNomeMacro(ArrayList<String> NomeMacro) {
        this.NomeMacro = NomeMacro;
    }

    public void limpaArrayValorReg() {
        getValorReg().clear();
    }

    public void limpaArrayNomeReg() {
        getNomeReg().clear();
    }

    public static void moveArquivo() {
        if (!new File(System.getProperty("user.dir") + "\\Class\\").isDirectory()) {
            criaPastaEm(System.getProperty("user.dir"), "Class");
        }
        File arq = new File(System.getProperty("user.dir") + "\\Java\\");//Diretorio que armazena os arquivos .java
        File[] files = arq.listFiles();//lista todos os arquivos que estão dentro desse diretorio
        for (File file : files) {
            if (file.getAbsolutePath().toUpperCase().endsWith(".CLASS")) {//pergunta se o arquivo do vetor de arquivos e um .class

                File arquivo = new File(System.getProperty("user.dir") + "\\Java\\" + file.getName());//instancia o arquivo .class

                File dir = new File(System.getProperty("user.dir") + "\\Class\\");//instancia o diretorio onde vai ser armazenado o .class

                boolean ok = arquivo.renameTo(new File(dir, "\\" + arquivo.getName()));//Faz a movimentação do arquivo para dir = diretorio da pasta Class + // + nome do arquivo 
                if (!ok) {//se não foi movido com sucesso 
                    if (file.exists()) {
                        file.delete();
                    }
                }
            }
        }

    }

    public static boolean criaPastaEm(String dir, String NomePast) {
        String fullPath = dir + "\\" + NomePast;
        File newDir = new File(fullPath);
        if (!(newDir.exists()) && newDir.mkdir()) {//se a pasta não existe e for possivel criar a pasta
            return true;
        } else if (newDir.exists()) {//se a pasta existe
            return true;
        } else {
            return false;
        }
    }

    public void carregaArqNomes() {
        getNomeMacro().clear();
        File arq = new File(caminhoXmls);
        //Arquivo existe
        if (arq.exists()) {
           //pega lista de arquivos no diretorio dos Xmls
            File[] arquivos = arq.listFiles();
            if (arquivos.length != 0) {//se o diretorio não estiver vazio
                for (File arquivo : arquivos) {
                    String nome = arquivo.getName().replaceAll(".xml", "");
                    if (verificaMacroExiste(nome)) {//verifica se o arquivo existe
                        this.getNomeMacro().add(nome);
                    } else {
                        JOptionPane.showMessageDialog(null, "A macro " + nome + " não existe !!!", "Não foi possivel encontrar", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                this.getNomeMacro().sort(null);
            }

        }
    }

    public boolean verificaMacroExiste(String NomeMacro) {
        File Arq = new File(caminhoXmls + NomeMacro + ".xml");
        return Arq.exists();

    }

}
