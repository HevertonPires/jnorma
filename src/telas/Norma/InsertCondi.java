package telas.Norma;

import classes.Norma.OperacoesJNorma;
import classes.Norma.ClasseNorma;
import java.util.ArrayList;
import java.util.Stack;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

public class InsertCondi extends javax.swing.JDialog {

    private boolean clikVoltar;
    private ClasseNorma objClasNorma;
    private CriaMacro objCriaMacro;
    private SelecMacro objSelectMacro;
    private int selectCond;
    private int test;
    private boolean setcondit;
    private ArrayList<String> RegsCarregList = new ArrayList<>();
    private OperacoesJNorma objOpNorma;
    private boolean macroCond;
    private boolean clickAnd;
    private boolean clickOr;
    private int controlSelecao;
    private int contParentAbert;
    private Stack<String> PiEstCondi;

    public void setSelectCond(int selectCond) {
        this.selectCond = selectCond;
    }

    public InsertCondi(ClasseNorma O, CriaMacro P, boolean S, OperacoesJNorma obj) {

        clikVoltar = false;
        this.objSelectMacro = null;
        this.objCriaMacro = P;
        this.objClasNorma = O;
        this.selectCond = -1;
        this.setcondit = S;
        this.test = -1;
        this.objOpNorma = obj;
        clickAnd = false;
        clickOr = false;
        this.carregaLista();
        initComponents();
        jlSelRegistradores.setEnabled(false);
        TesteCondDifFalse.setEnabled(false);
        TesteFalse.setEnabled(false);
        btnAND.setEnabled(false);
        btnOR.setEnabled(false);
        btnFechaParent.setEnabled(false);
        btnOk.setEnabled(false);
        PiEstCondi = new Stack<>();
        //btnDesfazerCond.setVisible(false);
    }

    public InsertCondi() {

        this.objSelectMacro = null;
        this.carregaLista();
        clikVoltar = false;
        clickAnd = false;
        clickOr = false;
        initComponents();
        jlSelRegistradores.setEnabled(false);
        TesteCondDifFalse.setEnabled(false);
        TesteFalse.setEnabled(false);
        btnAND.setEnabled(false);
        btnOR.setEnabled(false);
        btnOk.setEnabled(false);
        PiEstCondi = new Stack<>();
        // btnDesfazerCond.setVisible(false);

    }

    public boolean isMacroCond() {
        return macroCond;
    }

    public boolean isClickAnd() {
        return clickAnd;
    }

    public SelecMacro getObjSelectMacro() {
        return objSelectMacro;
    }

    public boolean isClickOr() {
        return clickOr;
    }

    public JRadioButton getTesteCondi0() {
        return TesteCondi0;
    }

    public JRadioButton getTesteCond1() {
        return TesteCond1;
    }

    public JRadioButton getTesteCondDifFalse() {
        return TesteCondDifFalse;
    }

    public JRadioButton getTesteFalse() {
        return TesteFalse;
    }

    public JList getJlSelRegistradores() {
        return jlSelRegistradores;
    }

    public ClasseNorma getObjClasNorma() {
        return objClasNorma;
    }

    public void setObjClasNorma(ClasseNorma objClasNorma) {
        this.objClasNorma = objClasNorma;
    }

    public CriaMacro getObjCriaMacro() {
        return objCriaMacro;
    }

    public void setObjCriaMacro(CriaMacro objCriaMacro) {
        this.objCriaMacro = objCriaMacro;
    }

    public int getSelectCond() {
        return selectCond;
    }

    public int getTest() {
        return test;
    }

    public void setTest(int test) {
        this.test = test;
    }

    public boolean isSetcondit() {
        return setcondit;
    }

    public void setSetcondit(boolean setcondit) {
        this.setcondit = setcondit;
    }

    public ArrayList<String> getRegsCarregList() {
        return RegsCarregList;
    }

    public void setRegsCarregList(ArrayList<String> Regs) {
        this.RegsCarregList = Regs;
    }

    public OperacoesJNorma getObjOpNorma() {
        return objOpNorma;
    }

    public void setObjOpNorma(OperacoesJNorma objOpNorma) {
        this.objOpNorma = objOpNorma;
    }

    public boolean isClikVoltar() {
        return clikVoltar;
    }

    public boolean getCondicaoValida() {
        return !(jlSelRegistradores.isSelectionEmpty() && (!TesteCondi0.isSelected() || !TesteFalse.isSelected()));
    }

    public void carregaLista() {
        RegsCarregList = (ArrayList<String>) objCriaMacro.getArRenameRegs().clone();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel10 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jlSelRegistradores = new javax.swing.JList();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnOk = new javax.swing.JButton();
        btnVoltar = new javax.swing.JButton();
        btnMacro_Condicao = new javax.swing.JToggleButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        TesteFalse = new javax.swing.JRadioButton();
        TesteCondi0 = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        TesteCond1 = new javax.swing.JRadioButton();
        TesteCondDifFalse = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        btnAND = new javax.swing.JButton();
        btnOR = new javax.swing.JButton();
        btnFechaParent = new javax.swing.JButton();
        btnAbreParent = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        lbCondicao = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        btnDesfazerCond = new javax.swing.JButton();

        jLabel10.setText("jLabel10");

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Inserir Condições");
        setModal(true);
        setResizable(false);

        jlSelRegistradores.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jlSelRegistradores.setModel(new javax.swing.AbstractListModel() {
            public int getSize() {
                return Integer.valueOf(RegsCarregList.size());
            }
            public Object getElementAt(int i) {
                Object elemento = RegsCarregList.get(i);
                return elemento;
            }
        });
        jlSelRegistradores.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jlSelRegistradoresValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(jlSelRegistradores);

        jLabel1.setText("Selecione o registrador:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Inserir Condição");

        btnOk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/ok.png"))); // NOI18N
        btnOk.setText("OK");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        btnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/voltar.png"))); // NOI18N
        btnVoltar.setText("Voltar");
        btnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });

        btnMacro_Condicao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/inserirMacro.png"))); // NOI18N
        btnMacro_Condicao.setText("Inserir Macro");
        btnMacro_Condicao.setToolTipText("Insere uma macro como condição da operação");
        btnMacro_Condicao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMacro_CondicaoActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/jNorma.png"))); // NOI18N
        jLabel3.setText("Máquina Universal NORMA");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        TesteFalse.setText("Registrador = false");
        TesteFalse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TesteFalseActionPerformed(evt);
            }
        });

        TesteCondi0.setText("Registrador = 0");
        TesteCondi0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TesteCondi0ActionPerformed(evt);
            }
        });

        jLabel2.setText("Comparação inteira:");

        jLabel5.setText("Comparação Boleana:");

        TesteCond1.setText("não(Registrador = 0)");
        TesteCond1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TesteCond1ActionPerformed(evt);
            }
        });

        TesteCondDifFalse.setText("Registrador = true");
        TesteCondDifFalse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TesteCondDifFalseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(TesteFalse)
                        .addGap(28, 28, 28)
                        .addComponent(TesteCondDifFalse)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(TesteCondi0)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(TesteCond1)
                        .addGap(38, 38, 38))))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {TesteCondi0, TesteFalse});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TesteCondi0)
                    .addComponent(TesteCond1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TesteFalse)
                    .addComponent(TesteCondDifFalse))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {TesteCondi0, TesteFalse});

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setText("Operadores logicos:");

        btnAND.setText("E (AND)");
        btnAND.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnANDActionPerformed(evt);
            }
        });

        btnOR.setText("OU (OR)");
        btnOR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnORActionPerformed(evt);
            }
        });

        btnFechaParent.setText(")");
        btnFechaParent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFechaParentActionPerformed(evt);
            }
        });

        btnAbreParent.setText("(");
        btnAbreParent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbreParentActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(btnAbreParent)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnFechaParent)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAND)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnOR)
                .addGap(38, 38, 38))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAND, btnOR});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAND)
                    .addComponent(btnOR)
                    .addComponent(btnFechaParent)
                    .addComponent(btnAbreParent))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAND, btnOR});

        jLabel7.setText("Condição:");

        jLabel8.setText("(");

        jLabel9.setText(")");

        btnDesfazerCond.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/desfazer_1.png"))); // NOI18N
        btnDesfazerCond.setText("Desfazer");
        btnDesfazerCond.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesfazerCondActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(btnMacro_Condicao)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnVoltar)
                .addGap(60, 60, 60))
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(209, 209, 209))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(338, 338, 338))))
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(3, 3, 3)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbCondicao, javax.swing.GroupLayout.PREFERRED_SIZE, 618, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDesfazerCond))
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 416, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnMacro_Condicao, btnOk, btnVoltar});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(15, 15, 15)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(lbCondicao, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel8)
                    .addComponent(btnDesfazerCond))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2))
                .addGap(18, 18, 18)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnMacro_Condicao)
                    .addComponent(btnVoltar)
                    .addComponent(btnOk))
                .addGap(38, 38, 38))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnMacro_Condicao, btnOk, btnVoltar});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnDesfazerCond, jLabel7, jLabel8, jLabel9, lbCondicao});

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void TesteCondi0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TesteCondi0ActionPerformed
        if (objSelectMacro == null || (objSelectMacro != null && objSelectMacro.getExecXML().getTipoMetodo().equals("int"))) {
            if (!clickAnd && !clickOr) {
                this.setSelectCond(0);
            } else {
                this.setSelectCond(4);
            }
            TesteFalse.setSelected(false);
            TesteCondDifFalse.setSelected(false);
            TesteCond1.setSelected(false);
            jlSelRegistradores.setEnabled(true);
        } else {
            TesteCond1.setSelected(false);
            TesteCondi0.setSelected(false);
            jlSelRegistradores.setEnabled(false);
        }
    }//GEN-LAST:event_TesteCondi0ActionPerformed

    private void TesteFalseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TesteFalseActionPerformed
        if (objSelectMacro == null || (objSelectMacro != null && objSelectMacro.getExecXML().getTipoMetodo().equals("boolean"))) {
            if (!clickAnd && !clickOr) {
                this.setSelectCond(2);
            } else {
                this.setSelectCond(4);
            }
            TesteCond1.setSelected(false);
            TesteCondi0.setSelected(false);
            TesteCondDifFalse.setSelected(false);
            jlSelRegistradores.setEnabled(true);
        }else{
            TesteCondDifFalse.setSelected(false);
            TesteFalse.setSelected(false);
            jlSelRegistradores.setEnabled(false);
        }
    }//GEN-LAST:event_TesteFalseActionPerformed

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        try {
            if (contParentAbert == 0) {//se todos os parenteses estiverem fechados
                if (!lbCondicao.getText().isEmpty()) {//se extir alguma condição
                    clikVoltar = false;
                    if (!getObjCriaMacro().getEstadoCodJava().isEmpty()) {//modifica o estado
                        getObjCriaMacro().getEstadoCodJava().remove(getObjCriaMacro().getEstadoCodJava().size() - 1);
                        getObjCriaMacro().getEstadoCodNorma().remove(getObjCriaMacro().getEstadoCodNorma().size() - 1);
                    }
                    String condicao;
                    if (jlSelRegistradores.getSelectedValue() != null) {
                        condicao = jlSelRegistradores.getSelectedValue().toString();
                    } else {
                        condicao = lbCondicao.getText();
                    }
                    getObjCriaMacro().setValorSelect(condicao);
                    switch (this.getSelectCond()) {
                        case 0:
                            getObjCriaMacro().getObjOpCriaMacro().regTesteZero(condicao, getTest());
                            break;
                        case 1:
                            getObjCriaMacro().getObjOpCriaMacro().regTesteDifZero(condicao, getTest());
                            break;
                        case 2:
                            getObjCriaMacro().regTesteFalse();
                            break;
                        case 3:
                            getObjCriaMacro().regTesteDifFalse();
                            break;
                        case 4:
                            getObjCriaMacro().getObjOpCriaMacro().regTestesComOperadoresLogicos(lbCondicao.getText());
                            break;
                    }
                    this.dispose();

                } else {
                    JOptionPane.showMessageDialog(this, "Nenhuma condição inserida !!!", "Inserir Condição", 0);
                }

            } else {
                JOptionPane.showMessageDialog(this, "Ainda resta(m) " + contParentAbert + " parênteses aberto(s) na condição", "Fechamento de condição", 0);
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objCriaMacro.getObjFormPrincipal().setLogSistema("----InsertCond - btnOK ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objCriaMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
        }
    }//GEN-LAST:event_btnOkActionPerformed

    private void btnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed
        clikVoltar = true;
        bVoltar();

        this.dispose();
    }//GEN-LAST:event_btnVoltarActionPerformed

    private void bVoltar() {
        //Caso uma macro que ia ser inserida e ja foi mapeada e o seu nome inserido nas macros usadas
        //clica em voltar entao o seu nome tem que ser retirado da string que guarda seu nome para não gerar replicação
        try {
            if (objSelectMacro != null && objSelectMacro.getObjMapeReg() != null && !objSelectMacro.getObjMapeReg().isClickVolta()) {
                String nomRetirado = objOpNorma.getNomMacrosUsadas().replace(RegsCarregList.get(0), "");
                objOpNorma.setNomMacrosUsadas(nomRetirado);
            }
            getObjCriaMacro().getBtnEnquanto().setEnabled(true);
            getObjCriaMacro().getBtnInsertMacro().setEnabled(true);
            getObjCriaMacro().getBtnPara().setEnabled(true);
            getObjCriaMacro().getBtnSalvar().setEnabled(true);
            getObjCriaMacro().getBtnSe().setEnabled(true);
            getObjCriaMacro().getBtnFinalizaInstrucao().setEnabled(true);
            getObjCriaMacro().getBtnFaca().setEnabled(false);

            if (getObjCriaMacro().getEstadoCodJava().size() > 0) {
                getObjCriaMacro().desfazOperacao();

                if (!objCriaMacro.getIdOperacoes().isEmpty()) {
                    getObjCriaMacro().getIdOperacoes().remove(getObjCriaMacro().getIdOperacoes().size() - 1);
                }
                getObjCriaMacro().controleButtons();

            } else {
                getObjCriaMacro().limpaMaquina();

            }
            clikVoltar = false;
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objCriaMacro.getObjFormPrincipal().setLogSistema("----InsertCond - bVoltar ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objCriaMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
        }
    }

    private void btnMacro_CondicaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMacro_CondicaoActionPerformed
        try {
            int pos = 0;
            this.setTest(3);
            macroCond = true;
            if (!getObjCriaMacro().getEstadoCodJava().isEmpty()) {
                getObjCriaMacro().getEstadoCodJava().remove(getObjCriaMacro().getEstadoCodJava().size() - 1);
                getObjCriaMacro().getEstadoCodNorma().remove(getObjCriaMacro().getEstadoCodNorma().size() - 1);
            }
            getObjCriaMacro().getEstadoCodJava().add(getObjCriaMacro().getSintaxJava());
            getObjCriaMacro().getEstadoCodNorma().add(getObjCriaMacro().getSintaxNorma());
            if (!getObjCriaMacro().getEstadoCodJava().isEmpty()) {
                pos = getObjCriaMacro().getEstadoCodJava().size() - 1;
            }

            objSelectMacro = new SelecMacro(getObjClasNorma(), getObjCriaMacro(), 3, getObjCriaMacro().getP1(), this, getObjOpNorma());
            objSelectMacro.setMacroCond(true);
            objSelectMacro.setVisible(true);
            if (objSelectMacro != null && objSelectMacro.getObjMapeReg() != null && !objSelectMacro.getObjMapeReg().isErroMap()) {

                TesteFalse.setEnabled(true);
                TesteCondDifFalse.setEnabled(true);
                jlSelRegistradores.repaint();
                jlSelRegistradores.requestFocus();
                if (!getObjCriaMacro().getEstadoCodJava().isEmpty()) {
                    getObjCriaMacro().getEstadoCodJava().remove(pos);
                    getObjCriaMacro().getEstadoCodNorma().remove(pos);
                }

                getObjCriaMacro().getEstadoCodJava().add(getObjCriaMacro().getSintaxJava());
                getObjCriaMacro().getEstadoCodNorma().add(getObjCriaMacro().getSintaxNorma());
                if (TesteCondi0.isSelected()) {
                    this.setSelectCond(0);
                } else if (TesteCond1.isSelected()) {
                    this.setSelectCond(1);
                } else if (TesteFalse.isSelected()) {
                    this.setSelectCond(2);
                } else if (TesteCondDifFalse.isSelected()) {
                    this.setSelectCond(3);
                } else {
                    setTest(-1);
                    setSelectCond(-1);
                }
                String aux = "";
                if (!RegsCarregList.isEmpty()) {
                    aux = RegsCarregList.get(0);
                }
                carregaLista();
                if (macroCond && !objSelectMacro.isClickVoltarSelMacro()) {
                    RegsCarregList.add(0, aux);
                }
                jlSelRegistradores.repaint();
                jlSelRegistradores.updateUI();
                jlSelRegistradores.clearSelection();
                jlSelRegistradores.setEnabled(false);

            } else {

                if (!objCriaMacro.getIdOperacoes().isEmpty() && !macroCond) {
                    objCriaMacro.getIdOperacoes().remove(objCriaMacro.getIdOperacoes().size() - 1);
                }
                objCriaMacro.controleButtons();
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objCriaMacro.getObjFormPrincipal().setLogSistema("----InsertCond - btnMacro_Cond ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objCriaMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
        }
    }//GEN-LAST:event_btnMacro_CondicaoActionPerformed

    private void TesteCond1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TesteCond1ActionPerformed
        if (objSelectMacro == null || (objSelectMacro != null && objSelectMacro.getExecXML().getTipoMetodo().equals("int"))) {
            if (!clickAnd && !clickOr) {
                this.setSelectCond(1);
            } else {
                this.setSelectCond(4);
            }
            TesteFalse.setSelected(false);
            TesteCondDifFalse.setSelected(false);
            TesteCondi0.setSelected(false);
            jlSelRegistradores.setEnabled(true);
        } else {
            TesteCond1.setSelected(false);
            TesteCondi0.setSelected(false);
            jlSelRegistradores.setEnabled(false);
        }
    }//GEN-LAST:event_TesteCond1ActionPerformed

    private void TesteCondDifFalseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TesteCondDifFalseActionPerformed
        if (objSelectMacro == null || (objSelectMacro != null && objSelectMacro.getExecXML().getTipoMetodo().equals("boolean"))) {
            if (!clickAnd && !clickOr) {
                this.setSelectCond(3);
            } else {
                this.setSelectCond(4);
            }
            TesteCond1.setSelected(false);
            TesteCondi0.setSelected(false);
            TesteFalse.setSelected(false);
            jlSelRegistradores.setEnabled(true);
        } else {
            TesteFalse.setSelected(false);
            TesteCondDifFalse.setSelected(false);
            jlSelRegistradores.setEnabled(false);
        }
    }//GEN-LAST:event_TesteCondDifFalseActionPerformed

    public void voltaEstadoIni() {
        jlSelRegistradores.clearSelection();
        btnMacro_Condicao.setEnabled(true);
        RegsCarregList.clear();
        carregaLista();
        jlSelRegistradores.repaint();
        jlSelRegistradores.updateUI();
        controlSelecao = 0;
        jlSelRegistradores.setEnabled(false);
        TesteCond1.setSelected(false);
        TesteCondi0.setSelected(false);
        TesteFalse.setSelected(false);
        TesteCondDifFalse.setSelected(false);
        TesteCond1.setEnabled(true);
        TesteCondi0.setEnabled(true);
        TesteFalse.setEnabled(false);
        TesteCondDifFalse.setEnabled(false);
        btnAND.setEnabled(false);
        btnOR.setEnabled(false);
        btnOk.setEnabled(false);
    }

    private void btnORActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnORActionPerformed
        clickOr = true;
        this.setSelectCond(4);
        voltaEstadoIni();
        lbCondicao.setText(lbCondicao.getText() + " OU ");
        PiEstCondi.push(lbCondicao.getText());
    }//GEN-LAST:event_btnORActionPerformed

    private void btnANDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnANDActionPerformed
        clickAnd = true;
        this.setSelectCond(4);
        voltaEstadoIni();
        lbCondicao.setText(lbCondicao.getText() + " E ");
        PiEstCondi.push(lbCondicao.getText());

    }//GEN-LAST:event_btnANDActionPerformed

    private void jlSelRegistradoresValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jlSelRegistradoresValueChanged
        try {
            String condicaoF = "", condicaoDifF = "", id = "";
            if (controlSelecao == 0 && jlSelRegistradores.getSelectedValue() != null) {//se não tiver selecionado nada ainda
                if (!(objCriaMacro.getObjOpCriaMacro().verificaMacro_em_Condicao(jlSelRegistradores.getSelectedValue().toString()))) {//se não tiver nenhuma macro na condição
                    id = "#";
                }
                if (TesteCond1.isSelected()) {//Registrador != 0
                    condicaoDifF = "nao(" + id;
                    condicaoF = " = 0)";
                } else if (TesteCondi0.isSelected()) {//Registrador = 0
                    condicaoDifF = "(" + id;
                    condicaoF = " = 0)";

                } else if (TesteCondDifFalse.isSelected()) {//Registrador = true
                    condicaoDifF = "nao(" + id;
                    condicaoF = " = false)";
                } else if (TesteFalse.isSelected()) {//Registrador = false
                    condicaoDifF = "(" + id;
                    condicaoF = " = false)";
                }
                if (jlSelRegistradores.getSelectedValue() != null) {

                    lbCondicao.setText(lbCondicao.getText() + condicaoDifF + this.jlSelRegistradores.getSelectedValue().toString() + condicaoF);
                    controlSelecao++;
                    btnMacro_Condicao.setEnabled(false);
                    PiEstCondi.push(lbCondicao.getText());
                    if (contParentAbert == 0) {
                        btnOk.setEnabled(true);
                    }
                }
                btnAND.setEnabled(true);
                btnOR.setEnabled(true);

            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objCriaMacro.getObjFormPrincipal().setLogSistema("----InsertCond - SelectCond ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objCriaMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
        }

    }//GEN-LAST:event_jlSelRegistradoresValueChanged

    private void btnAbreParentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbreParentActionPerformed
        btnFechaParent.setEnabled(true);
        lbCondicao.setText(lbCondicao.getText() + " ( ");
        contParentAbert++;
        btnOk.setEnabled(false);
        PiEstCondi.push(lbCondicao.getText());
    }//GEN-LAST:event_btnAbreParentActionPerformed

    private void btnFechaParentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFechaParentActionPerformed
        if (contParentAbert > 1) {
            contParentAbert--;
        } else {
            contParentAbert = 0;
            btnFechaParent.setEnabled(false);
            btnOk.setEnabled(true);
        }
        lbCondicao.setText(lbCondicao.getText() + " ) ");
        PiEstCondi.push(lbCondicao.getText());

    }//GEN-LAST:event_btnFechaParentActionPerformed

    private void enabled_Buttons(boolean status) {
        btnAND.setEnabled(status);
        btnAbreParent.setEnabled(status);
        btnFechaParent.setEnabled(status);
        btnMacro_Condicao.setEnabled(status);
        btnOR.setEnabled(status);
        btnOk.setEnabled(status);
        TesteCond1.setEnabled(status);
        TesteCondi0.setEnabled(status);
    }

    private void btnDesfazerCondActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesfazerCondActionPerformed
        try {
            if (!PiEstCondi.isEmpty()) {
                enabled_Buttons(true);
                if (PiEstCondi.lastElement().equals(")")) {
                    contParentAbert++;
                } else if (PiEstCondi.lastElement().equals("(")) {
                    contParentAbert--;
                }
                PiEstCondi.pop();

                if (!PiEstCondi.isEmpty()) {
                    lbCondicao.setText(PiEstCondi.lastElement());
                    lbCondicao.repaint();
                } else {
                    lbCondicao.setText("");
                    lbCondicao.repaint();
                }
                controlSelecao = 0;
                jlSelRegistradores.clearSelection();
                TesteCond1.setSelected(false);
                TesteCondDifFalse.setSelected(false);
                TesteCondi0.setSelected(false);
                TesteFalse.setSelected(false);
                jlSelRegistradores.clearSelection();
                jlSelRegistradores.setEnabled(false);
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objCriaMacro.getObjFormPrincipal().setLogSistema("----InsertCond - desfazerCond ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objCriaMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
        }
    }//GEN-LAST:event_btnDesfazerCondActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(() -> {
            new InsertCondi().setVisible(true);
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton TesteCond1;
    private javax.swing.JRadioButton TesteCondDifFalse;
    private javax.swing.JRadioButton TesteCondi0;
    private javax.swing.JRadioButton TesteFalse;
    private javax.swing.JButton btnAND;
    private javax.swing.JButton btnAbreParent;
    private javax.swing.JButton btnDesfazerCond;
    private javax.swing.JButton btnFechaParent;
    private javax.swing.JToggleButton btnMacro_Condicao;
    private javax.swing.JButton btnOR;
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnVoltar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JList jlSelRegistradores;
    private javax.swing.JLabel lbCondicao;
    // End of variables declaration//GEN-END:variables

}
