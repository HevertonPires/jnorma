package telas.Norma;

import classes.Norma.OperacoesJNorma;
import classes.Norma.ExecXML;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class MapeReg extends javax.swing.JDialog {

    private SelecMacro objSelecMacro;
    private CriaMacro objCriaMacro;
    private FormIni objFormIni;
    private ArrayList<String> RegsOr = new ArrayList<String>();
    private ArrayList<String> RegsDest = new ArrayList<String>();
    private ArrayList<String> RegsAux = new ArrayList<String>();
    private int controlReplace;
    private String codMacroMap;
    private String codJavaMap;
    private boolean erroMap;
    private OperacoesJNorma objOpNorma;
    private ExecXML execXML;
    private boolean voltar;
    private boolean clickVolta;
    private int pos;
    private int quantRemove = 0;

    public MapeReg(SelecMacro S, FormIni I, OperacoesJNorma obj, ExecXML execXML) {
        this.setModal(true);
        clickVolta = false;
        this.objFormIni = I;
        this.execXML = execXML;
        objOpNorma = obj;
        this.objSelecMacro = S;
        this.carregaLista();
        this.controlReplace = this.RegsDest.size();
        if (objFormIni.getObjNorma().getValorReg().size() < controlReplace) {
            JOptionPane.showMessageDialog(this, "Registrador(es) carregado(s) ( " + objFormIni.getObjNorma().getValorReg().size() + " )\nPara execução é preciso carregar mais ( "
                    + (controlReplace - objFormIni.getObjNorma().getValorReg().size()) + " ) registrador(es)", "Carregamento Incopativel", JOptionPane.INFORMATION_MESSAGE);
            erroMap = true;
            objSelecMacro.setClickVoltarSelMacro(true);
            RegsOr.clear();
            RegsDest.clear();
            objSelecMacro.dispose();

        } else {
            erroMap = false;
            initComponents();
            btnMapear.setEnabled(false);
            cbPersonal.setSelected(false);
            cbMapeAuto.setSelected(false);
            RegsEnt.setSelectedIndex(0);
            RegsSai.setSelectedIndex(0);
            this.lblResto.setText(String.valueOf(this.controlReplace));
            this.codJavaMap = objFormIni.getExecXML().getCodJava();
            this.codMacroMap = objFormIni.getMacroExec();
            RegsSai.setEnabled(false);
            RegsEnt.setEnabled(false);
        }
    }

    public MapeReg(SelecMacro S, CriaMacro I, OperacoesJNorma obj, ExecXML execXML) {
        this.setModal(true);
        clickVolta = false;
        this.objCriaMacro = I;
        this.objSelecMacro = S;
        this.execXML = execXML;
        objOpNorma = obj;
        this.carregaLista();
        this.controlReplace = this.RegsDest.size();
        if (objCriaMacro.getNReg() < controlReplace) {
            JOptionPane.showMessageDialog(this, "Numero de Registradores abaixo do necessario .Por favor acrescente mais "
                    + (controlReplace - objCriaMacro.getNReg()) + " Registrador(s)", "Numero de registradores incopativeis", JOptionPane.INFORMATION_MESSAGE);
            erroMap = true;
            // objSelecMacro.dispose();
            RegsOr.clear();
            RegsDest.clear();
        } else {
            erroMap = false;
            initComponents();
            btnMapear.setEnabled(false);
            cbMapeAuto.setSelected(false);
            cbPersonal.setSelected(false);
            RegsEnt.setSelectedIndex(0);
            RegsSai.setSelectedIndex(0);
            this.lblResto.setText(String.valueOf(this.controlReplace));
            this.codJavaMap = this.objCriaMacro.getCodJava();
            this.codMacroMap = this.objCriaMacro.getCodMacro();
            RegsSai.setEnabled(false);
            RegsEnt.setEnabled(false);
        }

    }

    public MapeReg() {
        this.setModal(true);
        this.carregaLista();
        clickVolta = false;
        cbPersonal.setSelected(false);
        RegsEnt.setEnabled(false);
        this.controlReplace = this.RegsDest.size();
        initComponents();
        btnMapear.setEnabled(false);
        this.lblResto.setText(String.valueOf(this.controlReplace));
        RegsEnt.setSelectedIndex(0);
        RegsSai.setSelectedIndex(0);
        RegsSai.setEnabled(false);
    }

    public void setExecXML(ExecXML execXML) {
        this.execXML = execXML;
    }

    public boolean isClickVolta() {
        return clickVolta;
    }

    public int getControlReplace() {
        return controlReplace;
    }

    public String getCodJavaMap() {
        return codJavaMap;
    }

    public SelecMacro getObjSelecMacro() {
        return objSelecMacro;
    }

    public void setObjSelecMacro(SelecMacro objSelecMacro) {
        this.objSelecMacro = objSelecMacro;
    }

    public CriaMacro getObjCriaMacro() {
        return objCriaMacro;
    }

    public void setObjCriaMacro(CriaMacro objCriaMacro) {
        this.objCriaMacro = objCriaMacro;
    }

    public FormIni getObjFormIni() {
        return objFormIni;
    }

    public void setObjFormIni(FormIni objFormIni) {
        this.objFormIni = objFormIni;
    }

    public ArrayList<String> getRegsOr() {
        return RegsOr;
    }

    public void setRegsOr(ArrayList<String> RegsOr) {
        this.RegsOr = RegsOr;
    }

    public ArrayList<String> getRegsDest() {
        return RegsDest;
    }

    public void setRegsDest(ArrayList<String> RegsDest) {
        this.RegsDest = RegsDest;
    }

    public OperacoesJNorma getObjOpNorma() {
        return objOpNorma;
    }

    public void setObjOpNorma(OperacoesJNorma objOpNorma) {
        this.objOpNorma = objOpNorma;
    }

    public boolean isErroMap() {
        return erroMap;
    }

    public void carregaLista() {
        RegsDest.clear();
        RegsOr.clear();

        if (this.getObjCriaMacro() == null) {//Mapeamento principal
            carregaRegsSai(execXML.getRegsUsados());
            carregaRegsEnt(getObjFormIni().getObjNorma().getNomeReg());
            RegsAux = (ArrayList<String>) RegsOr.clone();
        } else {//inserção de macro

            carregaRegsSai(execXML.getRegsUsados());
            carregaRegsEnt(objCriaMacro.getArRenameRegs());

        }

    }

    private void carregaRegsSai(ArrayList Regs) {
        RegsDest = (ArrayList<String>) Regs.clone();

    }

    private void carregaRegsEnt(ArrayList<String> arRenameRegs) {
        RegsOr = (ArrayList<String>) arRenameRegs.clone();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        RegsEnt = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        RegsSai = new javax.swing.JList();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnMapear = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        lblResto = new javax.swing.JLabel();
        btnVoltar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel13 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        cbMapeAuto = new javax.swing.JCheckBox();
        jLabel6 = new javax.swing.JLabel();
        cbPersonal = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Mapeamento de Registradores");
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Mapeamento de Registradores");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Mapeie os registradores utilizados na macro:");

        RegsEnt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        RegsEnt.setModel(new javax.swing.AbstractListModel() {
            public int getSize() {
                return Integer.valueOf(RegsOr.size());
            }
            public Object getElementAt(int i) {
                Object elemento = RegsOr.get(i);
                return elemento;
            }
        });
        jScrollPane1.setViewportView(RegsEnt);

        RegsSai.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        RegsSai.setModel(new javax.swing.AbstractListModel() {
            public int getSize() {
                return Integer.valueOf(RegsDest.size());
            }
            public Object getElementAt(int i) {
                Object elemento = RegsDest.get(i);
                return elemento;
            }
        });
        jScrollPane2.setViewportView(RegsSai);

        jLabel3.setText("Registradores Usados:");

        jLabel4.setText("Registradores na Macro:");

        btnMapear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/mapear.png"))); // NOI18N
        btnMapear.setText("Mapear");
        btnMapear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMapearActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(102, 0, 255));
        jLabel5.setText("Mapeamentos Restantes:");

        lblResto.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 1, 14)); // NOI18N
        lblResto.setForeground(new java.awt.Color(51, 51, 255));
        lblResto.setText("0");

        btnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/voltar.png"))); // NOI18N
        btnVoltar.setText("Voltar");
        btnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/jNorma.png"))); // NOI18N
        jLabel13.setText("Máquina Universal NORMA");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        cbMapeAuto.setText("Automático");
        cbMapeAuto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbMapeAutoActionPerformed(evt);
            }
        });

        jLabel6.setText("Tipo de Mapeamento:");

        cbPersonal.setText("Personalizado");
        cbPersonal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbPersonalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbPersonal)
                    .addComponent(cbMapeAuto))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(cbMapeAuto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbPersonal))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnMapear, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(126, 126, 126)
                                .addComponent(jLabel4))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(jLabel13)))
                .addContainerGap(44, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblResto, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnMapear, btnVoltar});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jScrollPane1, jScrollPane2});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13)
                .addGap(13, 13, 13)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(btnMapear))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(30, 30, 30)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(btnVoltar)
                    .addComponent(lblResto))
                .addContainerGap(29, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnMapear, btnVoltar});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jScrollPane1, jScrollPane2});

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnMapearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMapearActionPerformed
        try {
            if (!RegsEnt.isSelectionEmpty() && !RegsSai.isSelectionEmpty()) {
                if (this.controlReplace != 0) {
                    if (RegsSai.isSelectedIndex(0)) {
                        if (this.getObjCriaMacro() == null) {//mapeamento principal
                            mapeamentoPrincipal();
                        } else {
                            mapeamentoInsert_Cond_Macros();
                        }
                        this.controlReplace--;
                        this.lblResto.setText(String.valueOf(this.controlReplace));
                        //Fazer jDialog com os botoes de visualizar, restaurar e sair, para caso queira restaurar o mapeamento.
                        if (controlReplace == 0) {
                            JOptionPane.showMessageDialog(this, "Todos os registradores mapeados com sucesso !!!", "Mapeamento completado", 1);
                            dispose();
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Por favor realize o mapeamento dos registradores da aba ( Registradores na Macro ) de forma ordenada !!!", "Mapeamento Macros", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {

                    JOptionPane.showMessageDialog(this, "Todos os registradores da macro já foram mapeados !!!", "Mapeamento completado", 1);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Por Favor selecione o(s) registradore(s) para realizar o mapeamento.", "Mapeamento incompleto", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objSelecMacro.getObjFormPrincipal().setLogSistema("----MapeReg - btnMapear ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objSelecMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
        }

    }//GEN-LAST:event_btnMapearActionPerformed

    private void mapeamentoPrincipal() {
        try {
            if (RegsEnt.getSelectedIndex() != -1 && RegsSai.getSelectedIndex() != -1 && execXML.getVarsRetorno().contains(RegsSai.getSelectedValue().toString())) {
                if (cbMapeAuto.isSelected()) {
                    objFormIni.addRetorno_arPosRetornoTab(quantRemove);
                } else {
                    objFormIni.addRetorno_arPosRetornoTab(RegsAux.indexOf(RegsEnt.getSelectedValue()));
                    
                }
            }
            String troca;
            //inicio
            troca = execXML.getCodJava().replaceAll('#' + RegsSai.getSelectedValue().toString() + " ", RegsEnt.getSelectedValue().toString());
            execXML.setCodJava(troca);
            troca = execXML.getCodMacro().replaceAll("#" + RegsSai.getSelectedValue().toString() + " ", RegsEnt.getSelectedValue().toString() + " ");
            execXML.setCodMacro(troca);
            objFormIni.setMacroExec(troca);
            //fim 
            //troca nos codigos norma e java, os registradores selecionados pelo usuario, pelo primeiro registrador da macro
            if (!execXML.getCodJavaMetodo().isEmpty()) {
                troca = execXML.getCodJavaMetodo().replaceAll('#' + RegsSai.getSelectedValue().toString() + " ", RegsEnt.getSelectedValue().toString());
                execXML.setCodJavaMetodo(troca);
            }
            if (!execXML.getCodVariaveis().isEmpty()) {
                troca = execXML.getCodVariaveis().replaceFirst('#' + RegsSai.getSelectedValue().toString() + " ", RegsEnt.getSelectedValue().toString());
                execXML.setCodVariaveis(troca);
            }
            if (!RegsOr.isEmpty() && !RegsDest.isEmpty()) {
                String sel = getRegsOr().get(RegsEnt.getSelectedIndex());
                getRegsOr().remove(RegsEnt.getSelectedIndex());
                getRegsDest().remove(0);
                objFormIni.getObjNorma().getIndexMapePrin().add(sel);
                RegsSai.repaint();
                RegsEnt.repaint();
                quantRemove++;
            }

        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objSelecMacro.getObjFormPrincipal().setLogSistema("----MapeReg - mapePrincipal ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objSelecMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
        }
    }

    private void mapeamentoInsert_Cond_Macros() {
        try {
            getObjCriaMacro().getObjOpCriaMacro().replaceText(RegsSai.getSelectedValue().toString(), RegsEnt.getSelectedValue().toString(), getObjSelecMacro(), this, execXML);
            if (getObjSelecMacro() != null && getObjSelecMacro().getObjInserirCond() != null) {//Mactro como condicional
                ArrayList<String> Aux = new ArrayList<>();
                Aux = getObjSelecMacro().getObjInserirCond().getRegsCarregList();//pega os registradores usados na inserção da condição
                if (!Aux.isEmpty()) {
                    String func = Aux.get(0);
                    //substitui var0..1,2... pelo registrador selecionado
                    func = func.replaceFirst("var" + pos, "#" + RegsEnt.getSelectedValue().toString());
                    pos++;
                    if (controlReplace == 1) {
                        getObjOpNorma().setNomMacrosUsadas(getObjOpNorma().getNomMacrosUsadas() + "\n//" + func);
                        pos = 0;
                    }
                    getObjSelecMacro().getObjInserirCond().getRegsCarregList().clear();
                    getObjSelecMacro().getObjInserirCond().getRegsCarregList().add(0, func);
                    getObjSelecMacro().getObjInserirCond().getJlSelRegistradores().repaint();
                    getObjSelecMacro().getObjInserirCond().getJlSelRegistradores().requestFocus();
                }
            }
            if (!execXML.getCodVariaveis().isEmpty()) {
                String troca = execXML.getCodVariaveis().replaceFirst('#' + RegsSai.getSelectedValue().toString(), RegsEnt.getSelectedValue().toString());
                execXML.setCodVariaveis(troca);
            }
            if (!RegsOr.isEmpty() && !RegsDest.isEmpty()) {
                getRegsOr().remove(RegsEnt.getSelectedIndex());
                getRegsDest().remove(0);
                RegsSai.repaint();
                RegsEnt.repaint();
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objSelecMacro.getObjFormPrincipal().setLogSistema("----MapeReg - mapeInsert_Cond_Macro ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objSelecMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
        }
    }

    private void btnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed
        try {
            if (JOptionPane.showConfirmDialog(this, "Tem certeza que deseja voltar ?", "Sair", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0) {
                pos = 0;
                clickVolta = true;
                cbMapeAuto.setSelected(false);
                if (!(objCriaMacro != null && objSelecMacro.getObjInserirCond() != null && objCriaMacro.getObjInsertCondi().isMacroCond())) {
                    if (getObjCriaMacro() == null) {//mapeamento na tela principal, clicando em voltar
                        objOpNorma.limpaValoresTableRegs(execXML);
                        objFormIni.getArPosRetornoTab().clear();
                    } else {// clicando em voltar no mapeamento na inserção de macros na tela de criar macro
                        objCriaMacro.desfazOperacao();
                        if (!objCriaMacro.getIdOperacoes().isEmpty()) {
                            objCriaMacro.getIdOperacoes().remove(objCriaMacro.getIdOperacoes().size() - 1);
                        }
                        objCriaMacro.controleButtons();
                        if (objSelecMacro != null) {
                            objSelecMacro.setClickVoltarSelMacro(true);
                        }
                    }
                } else if (objCriaMacro != null && objSelecMacro.getObjInserirCond() != null && objCriaMacro.getObjInsertCondi().isMacroCond()) {
                    objCriaMacro.getObjInsertCondi().getRegsCarregList().clear();
                }

                dispose();
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objSelecMacro.getObjFormPrincipal().setLogSistema("----MapeReg - btnVoltar ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objSelecMacro.getObjFormPrincipal().setLogSistema(erro1.toString() + '\n');
            }
        }

    }//GEN-LAST:event_btnVoltarActionPerformed

    private void cbMapeAutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbMapeAutoActionPerformed
        cbMapeAuto.setSelected(true);
        cbPersonal.setSelected(false);
        RegsEnt.setEnabled(false);
        int contEnt = 0;
        int contSai = 0;

        while (cbMapeAuto.isSelected() && controlReplace != 0) {
            if (contEnt < RegsDest.size()) {
                RegsEnt.setSelectedIndex(contEnt);
            }
            if (contSai < RegsOr.size()) {
                RegsSai.setSelectedIndex(contSai);
            }
            btnMapearActionPerformed(evt);
        }

    }//GEN-LAST:event_cbMapeAutoActionPerformed

    private void cbPersonalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbPersonalActionPerformed
        btnMapear.setEnabled(true);
        cbMapeAuto.setSelected(false);
        cbPersonal.setSelected(true);
        RegsEnt.setEnabled(true);
    }//GEN-LAST:event_cbPersonalActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MapeReg.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MapeReg.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MapeReg.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MapeReg.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new MapeReg().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList RegsEnt;
    private javax.swing.JList RegsSai;
    private javax.swing.JButton btnMapear;
    private javax.swing.JButton btnVoltar;
    private javax.swing.JCheckBox cbMapeAuto;
    private javax.swing.JCheckBox cbPersonal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lblResto;
    // End of variables declaration//GEN-END:variables

}
