package telas.Norma;

import classes.Norma.OperacoesJNorma;
import classes.Norma.ClasseNorma;
import classes.Norma.OperacoesCriaMacro;
import java.awt.HeadlessException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

public class CriaMacro extends javax.swing.JFrame {

    private String regRetorno = "";//armazena os registradores de retorno da macro principal
    private String SintaxNorma = "";//armazena o codigo fonte na sintax Norma
    private String TipoMetodo;//armazena o tipo do metodo que vai ser criado bool ou in
    private SelecMacro objSelecMacro;
    private boolean entrou;
    private ClasseNorma objClasNorma;
    private FormIni objFormPrincipal;
    private int InsertCond;//armazena se uma condição foi inserida
    private String ValorSelect;//valor selecionado na tela de inserção de condição
    private String Metodo;//armazena o metodo criado, na inserção de uma macro como condição
    private Stack<String> PiInsAbertas;//pilha que armazena as condiçoes que foram abertas
    private Stack<String> PiInsFechadas;//pilha que armazena as condiçoes finalizadas
    private ArrayList<String> ParametMacroCond;//array que armazena os registradores das macros que sao usadas como condicional
    private ArrayList<Boolean> RetornosBool;//array que armazena os retornos booleanos
    private ArrayList<String> GuardaLoopsCond;//array que armazena os loops e condicionais inseridos
    private ArrayList<String> ParametInsertMacro;//array que armazena os registradores da macro que esta sendo criada
    private ArrayList<String> EstadoCodNorma;//array que armazena estados do codigo norma, para que possa recuperar algo que foi desfeito
    private ArrayList<String> EstadoCodJava;//array que armazena estados do codigo java, para que possa recuperar algo que foi desfeito
    private ArrayList<Integer> idOperacoes;//array que armazena um numero identificador de cada instrução da maquina Norma
    private ArrayList<String> arrayNomMacros;//array que armazena os nomes das macros inseridas 
    private ArrayList<String> arRenameRegs;//array que armazena os nomes do registradores 
    private OperacoesJNorma objOpNorma;
    private OperacoesCriaMacro objOpCriaMacro;
    private boolean loopAberto;//armazerna se tem ou nao um loop aberto
    private boolean salvo;//armazena se a macro ja foi salva
    private InsertCondi objInsertCondi;
    private DefaultTableModel jtInsAbeModel;
    private int vOrdem;
    private int Linha;
    private String SintaxJava;//armazena o codigo java gerado na construção da macro
    private CriaMacro objNovo;
    private ArrayList<String> regsRecebeRet;//array que armazena quais registradores recebe o retorno de uma macro inserida
    private int estadoNReg;//armazena o ultimo valor inserido no campo

    public void limpaMaquina() {
        bloq_desbloq_Buttons(true);
        estadoInicialButtons();
        SintaxNormaExib.setText(null);
        SintaxJava = "";
        SintaxNorma = "";
        Metodo = "";
        ValorSelect = "";
        objInsertCondi = null;
        objSelecMacro = null;
        entrou = false;
        loopAberto = false;
        salvo = false;
        regRetorno = "";
        RetornosBool.clear();
        ParametInsertMacro.clear();
        ParametMacroCond.clear();
        arrayNomMacros.clear();
        GuardaLoopsCond.clear();
        EstadoCodJava.clear();
        EstadoCodNorma.clear();
        PiInsAbertas.clear();
        PiInsFechadas.clear();
        idOperacoes.clear();
        limpaJTable();
        lbInstFinalizada.setText("0");
        lbInstAbertas.setText("0");
        lbcontInsAbertas.setText("0");
        vOrdem = 0;
        Linha = 0;
        NReg.setEnabled(true);
        NomeMacro.setEnabled(true);
        objOpNorma.setNomMacrosUsadas("");
        regsRecebeRet.clear();
        estadoNReg = 0;

    }

    public CriaMacro(ClasseNorma classeNorma, FormIni formIni, OperacoesJNorma operacoesJNorma) {
        this.objOpCriaMacro = new OperacoesCriaMacro(this, operacoesJNorma, classeNorma);
        this.objOpNorma = operacoesJNorma;
        this.objClasNorma = classeNorma;
        this.objFormPrincipal = formIni;
        this.InsertCond = -1;
        initComponents();
        inicializaVariaveis();
        bloq_desbloq_Buttons(false);
        jtNomRegs.setEnabled(false);
        btnSalvarRename.setVisible(false);
        btnLimparRename.setVisible(false);

    }

    public CriaMacro() {
        initComponents();
        inicializaVariaveis();
        bloq_desbloq_Buttons(false);
        jtNomRegs.setEnabled(false);
        btnSalvarRename.setVisible(false);
        btnLimparRename.setVisible(false);

    }

    private void estadoInicialButtons() {
        btInsRetBool.setEnabled(false);
        btInsRetInt.setEnabled(false);
        btnFaca.setEnabled(false);
        btnFinalizaInstrucao.setEnabled(false);
        btnNao.setEnabled(false);
        btnRegMenosUm.setEnabled(true);
        btnRegMaisUm.setEnabled(true);
        btnSalvar.setEnabled(true);
        btnSenao.setEnabled(false);
        btnLimpar.setEnabled(true);
    }

    private void inicializaVariaveis() {
        PiInsAbertas = new Stack<>();
        PiInsFechadas = new Stack<>();
        ParametMacroCond = new ArrayList<>();
        RetornosBool = new ArrayList<>();
        GuardaLoopsCond = new ArrayList<>();
        ParametInsertMacro = new ArrayList<>();
        EstadoCodNorma = new ArrayList<>();
        EstadoCodJava = new ArrayList<>();
        idOperacoes = new ArrayList();
        arrayNomMacros = new ArrayList<>();
        arRenameRegs = new ArrayList<>();
        entrou = false;
        Metodo = "";
        SintaxJava = "";
        jtInsAbeModel = (DefaultTableModel) jtInsAbertas.getModel();
        regsRecebeRet = new ArrayList<>();
        TipoMetodo = "int";
        estadoNReg = 0;

    }

    //setts
    public void setLoopAberto(boolean loopAberto) {
        this.loopAberto = loopAberto;
    }

    public void limpaRegRetorno() {
        regRetorno = "";
    }

    public void setRegRetorno(List<String> regRetorno) {
        for (int i = 0; i < regRetorno.size(); i++) {
            this.regRetorno += "<ret>" + regRetorno.get(i) + "</ret>\n";
        }

    }

    public void setNReg(int NReg) {
        this.NReg.setText(String.valueOf(NReg));
    }

    public void setNomeMacro(String NomeMacro) {
        this.NomeMacro.setText(NomeMacro);
    }

    public void setPiInsAbertas(Stack<String> PiInsAbertas) {
        this.PiInsAbertas = PiInsAbertas;
    }

    public void setEntrou(boolean entrou) {
        this.entrou = entrou;
    }

    public void setSintaxNormaExib(String S) {
        this.SintaxNormaExib.setText(this.SintaxNormaExib.getText() + S);
    }

    public void setSintaxJava(String S) {
        this.SintaxJava = S;
    }

    public void setSintaxNorma(String SintaxNorma) {
        this.SintaxNorma = SintaxNorma;
    }

    public void setValorSelect(String V) {
        this.ValorSelect = V;
    }

    public void setInsertCond(int InsertCond) {
        this.InsertCond = InsertCond;
    }

    //gettes
    public JLabel getInstFinal() {
        return lbInstFinalizada;
    }

    public ArrayList<String> getRegsRecebeRet() {
        return regsRecebeRet;
    }

    public ArrayList<String> getArRenameRegs() {
        return arRenameRegs;
    }

    public SelecMacro getObjSelecMacro() {
        return objSelecMacro;
    }

    public String getSintaxNorma() {
        return SintaxNorma;
    }

    public JLabel getInstAbertas() {
        return lbInstAbertas;
    }

    public JLabel getLbcontInsAbertas() {
        return lbcontInsAbertas;
    }

    public String getRegRetorno() {
        return regRetorno;
    }

    public boolean isLoopAberto() {
        return loopAberto;
    }

    public ArrayList<Boolean> getRetornosBool() {
        return RetornosBool;
    }

    public String getCodMacro() {
        return this.SintaxNormaExib.getText();
    }

    public FormIni getObjFormPrincipal() {
        return objFormPrincipal;
    }

    public InsertCondi getObjInsertCondi() {
        return objInsertCondi;
    }

    public String getCodJava() {
        return this.SintaxJava;
    }

    public String getValorSelect() {
        return this.ValorSelect;
    }

    public JTextArea getSintaxNormaExib() {
        return SintaxNormaExib;
    }

    public JButton getBtnFinalizaInstrucao() {
        return btnFinalizaInstrucao;
    }

    public JButton getBtnFaca() {
        return btnFaca;
    }

    public JButton getBtnEnquanto() {
        return btnEnquanto;
    }

    public JButton getBtnInsertMacro() {
        return btnInsertMacro;
    }

    public JButton getBtnPara() {
        return btnAte;
    }

    public JButton getBtnSalvar() {
        return btnSalvar;
    }

    public JButton getBtnSe() {
        return btnSe;
    }

    public ArrayList<String> getArrayNomMacros() {
        return arrayNomMacros;
    }

    public void setMetodo(String Metodo) {
        this.Metodo = Metodo;
    }

    public Stack<String> getPiInsAbertas() {
        return PiInsAbertas;
    }

    public ArrayList<String> getGuardaLoopsCond() {
        return GuardaLoopsCond;
    }

    public String getSintaxJava() {
        return SintaxJava;
    }

    public String getTipoMetodo() {
        return TipoMetodo;
    }

    public String getNomeMacro() {
        return NomeMacro.getText();
    }

    public FormIni getP1() {
        return objFormPrincipal;
    }

    public int getInsertCond() {
        return InsertCond;
    }

    public String getMetodo() {
        return Metodo;
    }

    public ArrayList<String> getParametInsertMacro() {
        return ParametInsertMacro;
    }

    public ArrayList<String> getEstadoCodJava() {
        return EstadoCodJava;
    }

    public Stack<String> getPiInsFechadas() {
        return PiInsFechadas;
    }

    public ArrayList<Integer> getIdOperacoes() {
        return idOperacoes;
    }

    public int getNReg() {
        int aux = Integer.valueOf(NReg.getText());
        return aux;
    }

    public ClasseNorma getObjClasNorma() {
        return objClasNorma;
    }

    public ArrayList<String> getParametMacroCond() {
        return ParametMacroCond;
    }

    public ArrayList<String> getEstadoCodNorma() {
        return EstadoCodNorma;
    }

    public OperacoesCriaMacro getObjOpCriaMacro() {
        return objOpCriaMacro;
    }

    public boolean isEntrou() {
        return entrou;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane4 = new javax.swing.JScrollPane();
        jPanel7 = new javax.swing.JPanel();
        jSeparator2 = new javax.swing.JSeparator();
        NomeMacro = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        SintaxNormaExib = new javax.swing.JTextArea();
        btnNovo = new javax.swing.JButton();
        NReg = new javax.swing.JTextField();
        lbValidNumReg = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnRegMaisUm = new javax.swing.JButton();
        btnRegMenosUm = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btDesfazer = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnFaca = new javax.swing.JButton();
        btnSe = new javax.swing.JButton();
        btnEnquanto = new javax.swing.JButton();
        btnSenao = new javax.swing.JButton();
        btnNao = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btnAte = new javax.swing.JButton();
        btnInsertMacro = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnVoltar = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtNomRegs = new javax.swing.JTable();
        btnSalvarRename = new javax.swing.JButton();
        btnLimparRename = new javax.swing.JButton();
        lbEstado = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        btInsRetInt = new javax.swing.JToggleButton();
        btnFinalizaInstrucao = new javax.swing.JButton();
        lbcontInsAbertas = new javax.swing.JLabel();
        btInsRetBool = new javax.swing.JButton();
        btnLimpar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        lbValidNomMac = new javax.swing.JLabel();
        btnSalvar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jtInsAbertas = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lbInstFinalizada = new javax.swing.JLabel();
        lbInstAbertas = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Criar Macro");
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        jScrollPane4.setBorder(null);

        NomeMacro.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                NomeMacroFocusLost(evt);
            }
        });

        SintaxNormaExib.setEditable(false);
        SintaxNormaExib.setColumns(20);
        SintaxNormaExib.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        SintaxNormaExib.setRows(5);
        jScrollPane1.setViewportView(SintaxNormaExib);

        btnNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/add2.png"))); // NOI18N
        btnNovo.setText("Novo");
        btnNovo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnNovoMouseClicked(evt);
            }
        });
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        NReg.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                NRegFocusLost(evt);
            }
        });
        NReg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NRegActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel2MouseClicked(evt);
            }
        });

        btnRegMaisUm.setText("Registrador + 1");
        btnRegMaisUm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegMaisUmActionPerformed(evt);
            }
        });

        btnRegMenosUm.setText("Registrador - 1");
        btnRegMenosUm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegMenosUmActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Operações com registradores:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(btnRegMaisUm)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRegMenosUm, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnRegMaisUm, btnRegMenosUm});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(btnRegMaisUm, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRegMenosUm, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnRegMaisUm, btnRegMenosUm});

        jLabel1.setText("Digite sua macro:");

        btDesfazer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/desfazer_1.png"))); // NOI18N
        btDesfazer.setText("Desfazer");
        btDesfazer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDesfazerActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel1MouseClicked(evt);
            }
        });

        btnFaca.setText("Faça");
        btnFaca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFacaActionPerformed(evt);
            }
        });

        btnSe.setText("Se");
        btnSe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeActionPerformed(evt);
            }
        });

        btnEnquanto.setText("Enquanto");
        btnEnquanto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnquantoActionPerformed(evt);
            }
        });

        btnSenao.setText("Senão");
        btnSenao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSenaoActionPerformed(evt);
            }
        });

        btnNao.setText("Não");
        btnNao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNaoActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Operações básicas:");
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });

        btnAte.setText("Até");
        btnAte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAteActionPerformed(evt);
            }
        });

        btnInsertMacro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/inserirMacro.png"))); // NOI18N
        btnInsertMacro.setText("Inserir Macro");
        btnInsertMacro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertMacroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnInsertMacro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnAte, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                            .addComponent(btnSe, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                            .addComponent(btnFaca, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnEnquanto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                            .addComponent(btnSenao, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                            .addComponent(btnNao, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE))))
                .addContainerGap())
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAte, btnEnquanto, btnFaca, btnNao, btnSe, btnSenao});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAte, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEnquanto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSe, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSenao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFaca, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnInsertMacro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAte, btnEnquanto, btnFaca, btnNao, btnSe, btnSenao});

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/jNorma.png"))); // NOI18N
        jLabel13.setText("Máquina Universal NORMA");

        jLabel7.setText("Codigo Norma: ");

        btnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/voltar.png"))); // NOI18N
        btnVoltar.setText("Voltar");
        btnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Nome(s) dos Registradores:"));
        jPanel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel6MouseClicked(evt);
            }
        });

        jtNomRegs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nº", "Nomes"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtNomRegs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtNomRegsMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jtNomRegs);
        if (jtNomRegs.getColumnModel().getColumnCount() > 0) {
            jtNomRegs.getColumnModel().getColumn(0).setPreferredWidth(10);
        }

        btnSalvarRename.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/salvar.png"))); // NOI18N
        btnSalvarRename.setToolTipText("Salvar Nomes");
        btnSalvarRename.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarRenameActionPerformed(evt);
            }
        });

        btnLimparRename.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/limpar.png"))); // NOI18N
        btnLimparRename.setToolTipText("Limpar Nomes");
        btnLimparRename.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparRenameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnSalvarRename)
                            .addComponent(btnLimparRename))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(lbEstado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnLimparRename, btnSalvarRename});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(btnSalvarRename)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLimparRename)
                        .addGap(37, 37, 37)
                        .addComponent(lbEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnLimparRename, btnSalvarRename});

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel3MouseClicked(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Operações complementares:");

        btInsRetInt.setText(" Retorno Inteiro");
        btInsRetInt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btInsRetIntActionPerformed(evt);
            }
        });

        btnFinalizaInstrucao.setText("Finalizar Instrução");
        btnFinalizaInstrucao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinalizaInstrucaoActionPerformed(evt);
            }
        });

        lbcontInsAbertas.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lbcontInsAbertas.setForeground(new java.awt.Color(51, 51, 255));
        lbcontInsAbertas.setText("0");

        btInsRetBool.setText("Retorno Booleano");
        btInsRetBool.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btInsRetBoolActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel5)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btInsRetBool, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btInsRetInt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnFinalizaInstrucao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbcontInsAbertas)
                .addGap(4, 4, 4))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFinalizaInstrucao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbcontInsAbertas, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btInsRetInt, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btInsRetBool)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btInsRetBool, btInsRetInt, btnFinalizaInstrucao});

        btnLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/limpar.png"))); // NOI18N
        btnLimpar.setText("Limpar");
        btnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Nome da macro:");

        lbValidNomMac.setToolTipText("Erro");

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/salvar.png"))); // NOI18N
        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("Número de Registradores:");

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Instruções Abertas:"));

        jtInsAbertas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Ordem", "Instrução"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtInsAbertas.setGridColor(new java.awt.Color(0, 0, 0));
        jScrollPane3.setViewportView(jtInsAbertas);
        if (jtInsAbertas.getColumnModel().getColumnCount() > 0) {
            jtInsAbertas.getColumnModel().getColumn(0).setMaxWidth(50);
        }

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Finalizadas:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("Abertas: ");

        lbInstFinalizada.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lbInstFinalizada.setForeground(new java.awt.Color(51, 51, 255));
        lbInstFinalizada.setText("0");

        lbInstAbertas.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lbInstAbertas.setForeground(new java.awt.Color(51, 51, 255));
        lbInstAbertas.setText("0");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbInstFinalizada, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbInstAbertas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {lbInstAbertas, lbInstFinalizada});

        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(lbInstFinalizada, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel9)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(lbInstAbertas, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {lbInstAbertas, lbInstFinalizada});

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(119, 119, 119))
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(NomeMacro, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbValidNomMac, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(NReg, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbValidNumReg, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnNovo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSalvar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btDesfazer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLimpar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnVoltar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(455, 455, 455)
                        .addComponent(jLabel13))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(70, 70, 70)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 596, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addGap(22, 22, 22)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btDesfazer, btnLimpar, btnNovo, btnSalvar, btnVoltar});

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {lbValidNomMac, lbValidNumReg});

        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lbValidNumReg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(NomeMacro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel6)
                            .addComponent(NReg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbValidNomMac)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnLimpar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btDesfazer, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnNovo)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jLabel1))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 483, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {NReg, NomeMacro, btDesfazer, btnLimpar, btnNovo, btnSalvar, btnVoltar, lbValidNomMac, lbValidNumReg});

        jScrollPane4.setViewportView(jPanel7);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 669, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(1327, 708));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
// a cada macro inserida o registrador(es) selecionado vai ser adicionado dentro do array

    public void addRegs(List<String> reg) {
        try {
            this.regsRecebeRet.addAll(reg);//adiciona a lista de registradores selecionados
            String replaceAll = "";
            for (int i = 0; i < reg.size(); i++) {
                replaceAll = SintaxNorma.replaceFirst("@", '#' + reg.get(i));//substitui o id de registrador pelo registrador selecionado
                this.setSintaxNorma(replaceAll);
            }
            SintaxNormaExib.setText(SintaxNorma.replaceAll("#", ""));//remove para exibição o id de registradores
            SintaxNormaExib.repaint();
            if (!EstadoCodNorma.isEmpty()) {
                EstadoCodNorma.remove(EstadoCodNorma.size() - 1);
            }
            EstadoCodNorma.add(replaceAll);
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormPrincipal.setLogSistema("----CriaMcro - addRegs ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormPrincipal.setLogSistema(erro1.toString() + '\n');
            }
        }
    }

    //remove todas as linhas da tabela que armazena os nomes dos registradores
    private void restauraRenameRegs() {
        DefaultTableModel model = (DefaultTableModel) jtNomRegs.getModel();
        if (jtNomRegs.isEditing()) {
            jtNomRegs.getCellEditor().stopCellEditing();
        }
        arRenameRegs.clear();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    //remove dos os valores inseridos na tabela dos nomes dos registradores
    private void limpaJTable() {
        if (jtInsAbertas.isEditing()) {
            jtInsAbertas.getCellEditor().stopCellEditing();
        }
        for (int i = 0; i < jtInsAbeModel.getRowCount(); i++) {
            jtInsAbeModel.removeRow(i);
        }
    }
//com base nos estado que estao dentro dos array, é inserido o codigo que estava anteriormente no text Area

    private void desfazCodMacro_CodJava() {
        try {
            if (EstadoCodJava.size() >= 2) {

                EstadoCodJava.remove(EstadoCodJava.size() - 1);
                setSintaxJava(EstadoCodJava.get(EstadoCodJava.size() - 1));

            } else if (EstadoCodJava.size() == 1) {
                EstadoCodJava.remove(0);
                setSintaxJava("");
            } else if (EstadoCodJava.isEmpty()) {
                setSintaxJava("");
            }

            if (EstadoCodNorma.size() >= 2) {

                EstadoCodNorma.remove(EstadoCodNorma.size() - 1);
                SintaxNorma = EstadoCodNorma.get(EstadoCodNorma.size() - 1);
                SintaxNormaExib.setText(SintaxNorma.replaceAll("#", ""));

            } else if (EstadoCodNorma.size() == 1) {
                EstadoCodNorma.remove(0);
                SintaxNormaExib.setText("");
                SintaxNorma = "";
            } else if (EstadoCodNorma.isEmpty()) {
                SintaxNormaExib.setText("");
                SintaxNorma = "";
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormPrincipal.setLogSistema("----CriaMacro - desfazCodMacro_CodJava ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormPrincipal.setLogSistema(erro1.toString() + '\n');
            }
        }
    }

    //modifica as pilhas de instruçoes abertas e fechadas e tambem o array de ids
    public void desfazOperacao() {
        try {
            desfazCodMacro_CodJava();

            if (!PiInsFechadas.isEmpty()) {
                if ("Fim-Ate".equals(PiInsFechadas.lastElement()) && idOperacoes.get(idOperacoes.size() - 1) == 15) {
                    PiInsAbertas.push("Ate");
                    PiInsFechadas.pop();
                    setLoopAberto(true);
                    addJtInsAbertas("Ate");

                } else if ("Fim-Enquanto".equals(PiInsFechadas.lastElement()) && idOperacoes.get(idOperacoes.size() - 1) == 15) {
                    PiInsAbertas.push("Enquanto");
                    PiInsFechadas.pop();
                    setLoopAberto(true);
                    addJtInsAbertas("Enquanto");

                } else if ("Fim-Se".equals(PiInsFechadas.lastElement()) && idOperacoes.get(idOperacoes.size() - 1) == 15) {
                    PiInsAbertas.push("Se");
                    PiInsFechadas.pop();
                    addJtInsAbertas("Se");

                }

            }
            if (!idOperacoes.isEmpty() && (idOperacoes.get(idOperacoes.size() - 1) == 2 || idOperacoes.get(idOperacoes.size() - 1) == 6 || idOperacoes.get(idOperacoes.size() - 1) == 7)) {
                if (jtInsAbeModel.getRowCount() != 0) {
                    jtInsAbeModel.removeRow(jtInsAbeModel.getRowCount() - 1);
                    vOrdem--;
                    Linha--;
                }
                if (!GuardaLoopsCond.isEmpty()) {
                    GuardaLoopsCond.remove(GuardaLoopsCond.size() - 1);
                    setLoopAberto(false);
                }
                if (!PiInsAbertas.isEmpty()) {
                    PiInsAbertas.pop();
                    setLoopAberto(false);
                }
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormPrincipal.setLogSistema("----CriaMacro - desfazOperacao ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormPrincipal.setLogSistema(erro1.toString() + '\n');
            }
        }

    }

    public void regTesteFalse() {
        objOpCriaMacro.testeCondicaoBoolean("false");
    }

    public void regTesteDifFalse() {
        objOpCriaMacro.testeCondicaoBoolean("difFalse");
    }

    private void btnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparActionPerformed
        limpaMaquina();
    }//GEN-LAST:event_btnLimparActionPerformed

    private void btnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed

        objFormPrincipal.setObjNorma(objClasNorma);
        if (!salvo && !SintaxNormaExib.getText().isEmpty()) {
            if (JOptionPane.showConfirmDialog(this, "Macro não foi salva ! Tem certeza que deseja voltar ? ", "Sair", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                limpaMaquina();
                dispose();
            }
        } else {

            this.dispose();

        }

    }//GEN-LAST:event_btnVoltarActionPerformed

    private void estadoFaçaButton() {
        btnAte.setEnabled(true);
        btnEnquanto.setEnabled(true);
        btnFinalizaInstrucao.setEnabled(true);
        btnInsertMacro.setEnabled(true);
        btnNao.setEnabled(true);
        btnRegMaisUm.setEnabled(true);
        btnRegMenosUm.setEnabled(true);
        btnSe.setEnabled(true);
        btnSenao.setEnabled(true);
        btInsRetBool.setEnabled(true);
        btInsRetInt.setEnabled(true);
        btnSalvar.setEnabled(false);
    }

    private void btnFacaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFacaActionPerformed

        if (!objOpCriaMacro.NumValido(NReg.getText())) {
            JOptionPane.showMessageDialog(null, "Número Inválido ou campo Vazio", "Erro", 0);
            NReg.requestFocus();
        } else {
            estadoFaçaButton();

            SintaxNormaExib.setText(SintaxNormaExib.getText() + " Faca\n ");
            setSintaxNorma(SintaxNorma + " Faca\n ");
            if (!this.GuardaLoopsCond.isEmpty()) {
                setSintaxJava(getSintaxJava() + "{\n");
                EstadoCodJava.add(getSintaxJava());
            }
            EstadoCodNorma.add(SintaxNorma);

            idOperacoes.add(1);

        }


    }//GEN-LAST:event_btnFacaActionPerformed

    private void estadoEnquantoButton() {
        btnAte.setEnabled(false);
        btnEnquanto.setEnabled(false);
        btnFinalizaInstrucao.setEnabled(false);
        btnInsertMacro.setEnabled(false);
        btnNao.setEnabled(false);
        btnAte.setEnabled(false);
        btnRegMaisUm.setEnabled(false);
        btnRegMenosUm.setEnabled(false);
        btnSe.setEnabled(false);
        btnSenao.setEnabled(false);
        btInsRetBool.setEnabled(false);
        btInsRetInt.setEnabled(false);
        btnFaca.setEnabled(true);
        btnSalvar.setEnabled(false);
    }

    private void btnEnquantoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnquantoActionPerformed
        boolean nomVal = verificaString(getNomeMacro());
        boolean numVal = objOpCriaMacro.NumValido(NReg.getText());

        if (!numVal || !nomVal) {
            if (!numVal) {
                JOptionPane.showMessageDialog(null, "Número Inválido ou campo Vazio", "Erro", 0);
                NReg.requestFocus();
            } else if (!nomVal) {
                JOptionPane.showMessageDialog(null, "Nome Inválido ou campo Vazio", "Erro", 0);
                NomeMacro.requestFocus();
            }
        } else {
            estadoEnquantoButton();

            this.InsertCond = 1;

            idOperacoes.add(6);
            PiInsAbertas.push("Enquanto");
            GuardaLoopsCond.add("Enquanto");
            addJtInsAbertas("Enquanto");
            loopAberto = true;
            SintaxNormaExib.setText(SintaxNormaExib.getText() + "( Enquanto ");
            setSintaxNorma(SintaxNorma + "( Enquanto ");
            setSintaxJava(getSintaxJava() + "while(");

            EstadoCodJava.add(getSintaxJava());
            EstadoCodNorma.add(SintaxNorma);

            objInsertCondi = new InsertCondi(objClasNorma, this, false, objOpNorma);
            objInsertCondi.setVisible(true);

            lbInstAbertas.setText(String.valueOf(PiInsAbertas.size()));
            lbcontInsAbertas.setText(String.valueOf(PiInsAbertas.size()));
            lbInstFinalizada.setText(String.valueOf(PiInsFechadas.size()));
            lbInstFinalizada.repaint();
            lbInstAbertas.repaint();
            lbcontInsAbertas.repaint();

        }
    }//GEN-LAST:event_btnEnquantoActionPerformed

    private void estadoSeButton() {
        btnAte.setEnabled(false);
        btnEnquanto.setEnabled(false);
        btnFinalizaInstrucao.setEnabled(false);
        btnInsertMacro.setEnabled(false);
        btnNao.setEnabled(false);
        btnAte.setEnabled(false);
        btnRegMaisUm.setEnabled(false);
        btnRegMenosUm.setEnabled(false);
        btnSe.setEnabled(false);
        btnSenao.setEnabled(false);
        btInsRetBool.setEnabled(false);
        btnFaca.requestFocus();
        btInsRetInt.setEnabled(false);
        btnFaca.setEnabled(true);
        btnSalvar.setEnabled(false);
    }

    private void btnSeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeActionPerformed
        boolean nomVal = verificaString(getNomeMacro());
        boolean numVal = objOpCriaMacro.NumValido(NReg.getText());

        if (!numVal || !nomVal) {
            if (!numVal) {
                JOptionPane.showMessageDialog(null, "Número Inválido ou campo Vazio", "Erro", 0);
                NReg.requestFocus();
            } else if (!nomVal) {
                JOptionPane.showMessageDialog(null, "Nome Inválido ou campo Vazio", "Erro", 0);
                NomeMacro.requestFocus();
            }
        } else {
            estadoSeButton();

            this.InsertCond = 1;
            idOperacoes.add(7);
            PiInsAbertas.push("Se");
            GuardaLoopsCond.add("Se");
            addJtInsAbertas("Se");

            SintaxNormaExib.setText(SintaxNormaExib.getText() + "( Se  ");
            setSintaxNorma(SintaxNorma + "( Se  ");
            setSintaxJava(getSintaxJava() + "if(");

            EstadoCodJava.add(getSintaxJava());
            EstadoCodNorma.add(SintaxNorma);

            objInsertCondi = new InsertCondi(objClasNorma, this, false, objOpNorma);
            objInsertCondi.setVisible(true);

            lbInstAbertas.setText(String.valueOf(PiInsAbertas.size()));
            lbcontInsAbertas.setText(String.valueOf(PiInsAbertas.size()));
            lbInstFinalizada.setText(String.valueOf(PiInsFechadas.size()));
            lbInstFinalizada.repaint();
            lbInstAbertas.repaint();
            lbcontInsAbertas.repaint();

        }

    }//GEN-LAST:event_btnSeActionPerformed

    private void estadoSenaoButton() {
        btnAte.setEnabled(false);
        btnEnquanto.setEnabled(false);
        btnFinalizaInstrucao.setEnabled(false);
        btnInsertMacro.setEnabled(false);
        btnNao.setEnabled(false);
        btnAte.setEnabled(false);
        btnRegMaisUm.setEnabled(false);
        btnRegMenosUm.setEnabled(false);
        btnSe.setEnabled(false);
        btnSenao.setEnabled(false);
        btInsRetBool.setEnabled(false);
        btInsRetInt.setEnabled(false);
        btnFaca.setEnabled(true);
        btnSalvar.setEnabled(false);
    }

    private void btnSenaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSenaoActionPerformed
        if (!objOpCriaMacro.NumValido(NReg.getText())) {
            JOptionPane.showMessageDialog(null, "Número Inválido ou campo Vazio", "Erro", 0);
            NReg.requestFocus();
        } else if ("Se".equals(this.GuardaLoopsCond.get(this.GuardaLoopsCond.size() - 1))) {

            estadoSeButton();
            SintaxNormaExib.setText(SintaxNormaExib.getText() + "Senao ");
            setSintaxNorma(SintaxNorma + "Senao ");
            setSintaxJava(getSintaxJava() + "}\nelse");

            idOperacoes.add(3);
            EstadoCodJava.add(getSintaxJava());
            EstadoCodNorma.add(SintaxNorma);

        }

    }//GEN-LAST:event_btnSenaoActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        try {
            if (verificaString(getNomeMacro()) && objOpCriaMacro.NumValido(NReg.getText())) {

                SelecReg objSelecReg = new SelecReg(this, 5, null);
                limpaRegRetorno();

                if (!SintaxNormaExib.getText().isEmpty()) {
                    if (!this.NomeMacro.getText().isEmpty()) {
                        if (!objSelecReg.isClickVoltar()) {
                            if (!objOpCriaMacro.verificaMacroExiste(NomeMacro.getText())) {
                                if (!TipoMetodo.equals("boolean")) {
                                    objSelecReg.setVisible(true);
                                }
                                salvarMacrobtnSalvar();

                            } else if (JOptionPane.showConfirmDialog(this, "Macro já existe... Deseja substitui-la ? ", "Macro existente", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
                                new File(objClasNorma.caminhoXmls + NomeMacro.getText() + ".xml").delete();
                                new File(objClasNorma.caminhoClass + NomeMacro.getText() + ".class").delete();
                                new File(objClasNorma.caminhoJava + NomeMacro.getText() + ".java").delete();
                                for (int i = 0; i < objClasNorma.getNomeMacro().size(); i++) {
                                    if (objClasNorma.getNomeMacro().get(i).toUpperCase().equals(NomeMacro.getText().toUpperCase())) {
                                        objClasNorma.getNomeMacro().remove(i);
                                    }
                                }
                                if (!TipoMetodo.equals("boolean")) {
                                    objSelecReg.setVisible(true);
                                }
                                salvarMacrobtnSalvar();

                            }
                            NomeMacro.setEnabled(false);
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Nome da macro não inserido, por favor digite o nome da macro.", "Inserir Nome", JOptionPane.WARNING_MESSAGE);
                        NomeMacro.requestFocus();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Não é possivel salvar uma macro vazia!!!", "Digite sua macro", JOptionPane.ERROR_MESSAGE);
                }
            } else if (!verificaString(getNomeMacro())) {
                JOptionPane.showMessageDialog(this, "Por favor use apenas letras, numeros ou underline (_), sendo que o nome não pode começar com numero e nem com underline", "Inserir Nome", JOptionPane.WARNING_MESSAGE);
                NomeMacro.requestFocus();
            } else {
                JOptionPane.showMessageDialog(this, "Número de registradores inserido não é um numero inteiro maior que 0 válido.", "Inserir número de registradores", JOptionPane.WARNING_MESSAGE);
                NReg.requestFocus();
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormPrincipal.setLogSistema("----CriaMacro - salvarMacro ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormPrincipal.setLogSistema(erro1.toString() + '\n');
            }
        }

    }//GEN-LAST:event_btnSalvarActionPerformed

    private void salvarMacrobtnSalvar() {
        if (PiInsAbertas.isEmpty()) {
            if (!SintaxNormaExib.getText().isEmpty()) {
                salvo = objOpCriaMacro.salvarMacro();
                if (salvo) {
                    btnSalvar.setEnabled(false);
                    NReg.setEnabled(false);
                    NomeMacro.setEnabled(false);
                } else {
                    btnSalvar.setEnabled(true);
                    NReg.setEnabled(true);
                    NomeMacro.setEnabled(true);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Codigo Norma vazio, Por favor digite sua macro !!!", "Informe sua macro", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Por favor finalize todas as instruções abertas !!!", "Finaliza Instrução", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void btnRegMaisUmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegMaisUmActionPerformed
        boolean nomVal = verificaString(getNomeMacro());
        boolean numVal = objOpCriaMacro.NumValido(NReg.getText());

        if (!numVal || !nomVal) {
            if (!numVal) {
                JOptionPane.showMessageDialog(null, "Número Inválido ou campo Vazio", "Erro", 0);
                NReg.requestFocus();
            } else if (!nomVal) {
                JOptionPane.showMessageDialog(null, "Nome Inválido ou campo Vazio", "Erro", 0);
                NomeMacro.requestFocus();
            }
        } else {
            SelecReg P3 = new SelecReg(objClasNorma, this, 0);
            P3.setVisible(true);
            if (!P3.isClickVoltar()) {
                idOperacoes.add(9);
            }

        }


    }//GEN-LAST:event_btnRegMaisUmActionPerformed

    private void btnRegMenosUmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegMenosUmActionPerformed
        boolean nomVal = verificaString(getNomeMacro());
        boolean numVal = objOpCriaMacro.NumValido(NReg.getText());

        if (!numVal || !nomVal) {
            if (!numVal) {
                JOptionPane.showMessageDialog(null, "Número Inválido ou campo Vazio", "Erro", 0);
                NReg.requestFocus();
            } else if (!nomVal) {
                JOptionPane.showMessageDialog(null, "Nome Inválido ou campo Vazio", "Erro", 0);
                NomeMacro.requestFocus();
            }
        } else {
            SelecReg P3 = new SelecReg(objClasNorma, this, 1);
            P3.setVisible(true);
            if (!P3.isClickVoltar()) {
                idOperacoes.add(11);
            }
        }


    }//GEN-LAST:event_btnRegMenosUmActionPerformed

    private void btnNaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNaoActionPerformed
        if (!objOpCriaMacro.NumValido(NReg.getText())) {
            JOptionPane.showMessageDialog(null, "Número Inválido ou campo Vazio", "Erro", 0);
            NReg.requestFocus();
        } else {
            SintaxNormaExib.setText(SintaxNormaExib.getText() + " nao ");
            setSintaxNorma(SintaxNorma + " nao ");
            setSintaxJava(getSintaxJava() + "!");

            idOperacoes.add(4);
            EstadoCodJava.add(getSintaxJava());
            EstadoCodNorma.add(SintaxNorma);

        }
        btnSalvar.setEnabled(false);


    }//GEN-LAST:event_btnNaoActionPerformed

    private void estadoAteButton() {
        btnAte.setEnabled(false);
        btnEnquanto.setEnabled(false);
        btnFinalizaInstrucao.setEnabled(false);
        btnInsertMacro.setEnabled(false);
        btnNao.setEnabled(false);
        btnAte.setEnabled(false);
        btnRegMaisUm.setEnabled(false);
        btnRegMenosUm.setEnabled(false);
        btnSe.setEnabled(false);
        btnSenao.setEnabled(false);
        btInsRetBool.setEnabled(false);
        btInsRetInt.setEnabled(false);
        btnFaca.setEnabled(true);
        btnSalvar.setEnabled(false);
    }

    private void addJtInsAbertas(String cond) {
        jtInsAbeModel.addRow(new Object[]{});
        jtInsAbeModel.setValueAt(cond, Linha, 1);
        jtInsAbeModel.setValueAt(vOrdem + 1, Linha, 0);
        vOrdem++;
        Linha++;
    }

    private void btnAteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAteActionPerformed

        boolean nomVal = verificaString(getNomeMacro());
        boolean numVal = objOpCriaMacro.NumValido(NReg.getText());

        if (!numVal || !nomVal) {
            if (!numVal) {
                JOptionPane.showMessageDialog(null, "Número Inválido ou campo Vazio", "Erro", 0);
                NReg.requestFocus();
            } else if (!nomVal) {
                JOptionPane.showMessageDialog(null, "Nome Inválido ou campo Vazio", "Erro", 0);
                NomeMacro.requestFocus();
            }
        } else {

            estadoAteButton();
            this.InsertCond = 1;
            idOperacoes.add(2);
            PiInsAbertas.push("Ate");
            GuardaLoopsCond.add("Ate");
            loopAberto = true;
            addJtInsAbertas("Ate");

            SintaxNormaExib.setText(SintaxNormaExib.getText() + "( Ate  ");
            setSintaxNorma(SintaxNorma + "( Ate  ");
            setSintaxJava(getSintaxJava() + "for( ;");

            EstadoCodJava.add(getSintaxJava());
            EstadoCodNorma.add(SintaxNorma);

            objInsertCondi = new InsertCondi(objClasNorma, this, true, objOpNorma);
            objInsertCondi.setVisible(true);
            if (!objInsertCondi.isClikVoltar()) {

            }
            lbInstAbertas.setText(String.valueOf(PiInsAbertas.size()));
            lbcontInsAbertas.setText(String.valueOf(PiInsAbertas.size()));
            lbInstFinalizada.setText(String.valueOf(PiInsFechadas.size()));
            lbInstFinalizada.repaint();
            lbInstAbertas.repaint();
            lbcontInsAbertas.repaint();

        }
    }//GEN-LAST:event_btnAteActionPerformed

    private void btInsRetIntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btInsRetIntActionPerformed
        SelecReg P3 = new SelecReg(objClasNorma, this, 4);
        P3.setVisible(true);
        if (!P3.isClickVoltar()) {
            idOperacoes.add(13);
        }


    }//GEN-LAST:event_btInsRetIntActionPerformed

    private void estadoInsMacroButton() {
        btnFaca.setEnabled(true);
        btnSalvar.setEnabled(true);
    }

    private void btnInsertMacroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertMacroActionPerformed
        boolean nomVal = verificaString(getNomeMacro());
        boolean numVal = objOpCriaMacro.NumValido(NReg.getText());

        if (!numVal || !nomVal) {
            if (!numVal) {
                JOptionPane.showMessageDialog(null, "Número Inválido ou campo Vazio", "Erro", 0);
                NReg.requestFocus();
            } else if (!nomVal) {
                JOptionPane.showMessageDialog(null, "Nome Inválido ou campo Vazio", "Erro", 0);
                NomeMacro.requestFocus();
            }
        } else {

            objSelecMacro = new SelecMacro(objClasNorma, this, 2, objFormPrincipal, objOpNorma);
            objSelecMacro.setVisible(true);
            if (objSelecMacro != null && !objSelecMacro.isClickVoltarSelMacro()) {
                idOperacoes.add(8);
                estadoInsMacroButton();

                new SelecReg(this, 6, objSelecMacro.getExecXML()).setVisible(true);

            }
        }


    }//GEN-LAST:event_btnInsertMacroActionPerformed

    private void btnFinalizaInstrucaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinalizaInstrucaoActionPerformed

        if (!PiInsAbertas.isEmpty()) {

            objOpCriaMacro.fechaInstrucao(PiInsAbertas.lastElement());
            btnSalvar.setEnabled(true);
            idOperacoes.add(15);
            if (jtInsAbeModel.getRowCount() != 0) {
                jtInsAbeModel.removeRow(jtInsAbeModel.getRowCount() - 1);
                vOrdem--;
                Linha--;
            }
        }


    }//GEN-LAST:event_btnFinalizaInstrucaoActionPerformed

    private void bloq_desbloq_Buttons(boolean estado) {

        btnAte.setEnabled(estado);
        btnEnquanto.setEnabled(estado);
        btnSe.setEnabled(estado);
        btnAte.setEnabled(estado);
        btnInsertMacro.setEnabled(estado);
        btInsRetBool.setEnabled(estado);
        btInsRetInt.setEnabled(estado);
        btnFaca.setEnabled(estado);
        btnFinalizaInstrucao.setEnabled(estado);
        btnNao.setEnabled(estado);
        btnRegMenosUm.setEnabled(estado);
        btnRegMaisUm.setEnabled(estado);
        btnSalvar.setEnabled(estado);
        btnSenao.setEnabled(estado);

    }


    private void btDesfazerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDesfazerActionPerformed
        if (!SintaxNormaExib.getText().isEmpty()) {
            desfazOperacao();
            if (salvo) {
                btnSalvar.setEnabled(true);
            }
            if (idOperacoes.size() >= 1) {
                idOperacoes.remove(idOperacoes.size() - 1);
            } else {
                limpaMaquina();
            }
            controleButtons();
            lbInstAbertas.setText(String.valueOf(PiInsAbertas.size()));
            lbcontInsAbertas.setText(String.valueOf(PiInsAbertas.size()));
            lbInstFinalizada.setText(String.valueOf(PiInsFechadas.size()));
            lbInstFinalizada.repaint();
            lbInstAbertas.repaint();
            lbcontInsAbertas.repaint();
        }
    }//GEN-LAST:event_btDesfazerActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        objNovo = new CriaMacro(objClasNorma, objFormPrincipal, objOpNorma);
        objNovo.setExtendedState(JFrame.MAXIMIZED_BOTH);
        objNovo.setVisible(true);
    }//GEN-LAST:event_btnNovoActionPerformed

    private void jPanel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseClicked

        btnRegMaisUm.requestFocus();
    }//GEN-LAST:event_jPanel2MouseClicked

    private void NRegFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_NRegFocusLost
        try {
            if (!objOpCriaMacro.NumValido(NReg.getText())) {

                if (NReg.getText().isEmpty()) {
                    Icon icone = new ImageIcon("atencao.png");
                    lbValidNumReg.setIcon(icone);
                    lbValidNumReg.setToolTipText("Campo vazio !");
                    NReg.setToolTipText("Campo vazio !");
                    jtNomRegs.setEnabled(false);
                    btnSalvarRename.setVisible(false);
                    btnLimparRename.setVisible(false);

                } else {
                    Icon icone = new ImageIcon("erro.png");
                    lbValidNumReg.setIcon(icone);
                    lbValidNumReg.setToolTipText("Numero inválido !");
                    NReg.setToolTipText("Insira um número inteiro !");
                    btnSalvar.setEnabled(false);
                    jtNomRegs.setEnabled(false);
                    btnSalvarRename.setVisible(false);
                    btnLimparRename.setVisible(false);
                }

            } else if (SintaxNormaExib.getText().isEmpty() && arRenameRegs.isEmpty()) {

                restauraRenameRegs();
                inicializaNomesRegs();
                bloq_desbloq_Buttons(false);
                Icon icone = new ImageIcon("valid.png");
                lbValidNumReg.setIcon(icone);
                lbValidNumReg.setToolTipText("Numero válido !");
                NReg.setToolTipText("Numero válido !");
                lbEstado.setText("");
                jtNomRegs.setEnabled(true);
                btnSalvarRename.setVisible(true);
                btnLimparRename.setVisible(true);
                btnSalvar.setEnabled(true);
                estadoNReg = getNReg();

            } else if (getNReg() > estadoNReg) {//se o novo valor for maior que o anterior
                bloq_desbloq_Buttons(false);
                inicializaNomesRegs();
                arRenameRegs.clear();
                btnSalvarRename.setVisible(true);
                NReg.setEnabled(false);
                jtNomRegs.setEnabled(true);
                Icon icone = new ImageIcon("valid.png");
                lbValidNumReg.setIcon(icone);
                lbValidNumReg.setToolTipText("Numero válido !");
                NReg.setToolTipText("Numero válido !");
                lbEstado.setText("Não renomei registradores já inseridos !");
                btnSalvar.setEnabled(true);
                estadoNReg = getNReg();

            } else {
                //busca todos os registradores que ja estao sendo usados na macro que esta sendo criada
                ArrayList<String> regsUsando = buscaRegsUsado(arRenameRegs, SintaxNorma);
                int quant = regsUsando.size();

                if (quant > getNReg()) {//se todos os registradores que estavam carregados estao sendo usado
                    Icon icone = new ImageIcon("erro.png");
                    lbValidNumReg.setIcon(icone);
                    lbValidNumReg.setToolTipText("Todos os registradores ja estao sendo usado");
                    NReg.setToolTipText("Todos os registradores já estão sendo usados");

                } else if (quant <= getNReg() && getNReg() <= estadoNReg) {//se a quantidade de registradores usados for menor
                    //que a quantidade solicitada, e o novo valor informado 
                    //nao seja maior que o anterior
                    bloq_desbloq_Buttons(true);

                    if (regsUsando.isEmpty()) {
                        for (int i = 0; i < getNReg(); i++) {
                            regsUsando.add(arRenameRegs.get(i));
                        }
                    }
                    restauraRenameRegs();
                    arRenameRegs = regsUsando;
                    DefaultTableModel model = (DefaultTableModel) jtNomRegs.getModel();
                    inicializaNomesRegs();
                    for (int i = 0; i < regsUsando.size(); i++) {
                        model.setValueAt(regsUsando.get(i), i, 1);
                    }
                    Icon icone = new ImageIcon("valid.png");
                    lbValidNumReg.setIcon(icone);
                    lbValidNumReg.setToolTipText("Numero válido !");
                    NReg.setToolTipText("Numero válido !");
                    lbEstado.setText("Registradore(s) renomeado(s)!!!");
                    btnSalvar.setEnabled(true);
                    estadoNReg = getNReg();
                    estadoInicialButtons();

                }
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormPrincipal.setLogSistema("----CriaMacro - NRegFocusLost ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormPrincipal.setLogSistema(erro1.toString() + '\n');
            }
        }
    }//GEN-LAST:event_NRegFocusLost

    private ArrayList buscaRegsUsado(ArrayList<String> Regs, String Cod) {
        ArrayList<String> regsUsando = new ArrayList<>();
        try {
            for (int i = 0; i < Regs.size(); i++) {
                if ((!ParametInsertMacro.isEmpty() && ParametInsertMacro.contains(Regs.get(i))) || (!ParametMacroCond.isEmpty() && ParametMacroCond.contains(Regs.get(i)))) {
                    regsUsando.add(Regs.get(i));
                } else if (Cod.contains("#" + Regs.get(i) + " ")) {
                    regsUsando.add(Regs.get(i));
                }

            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormPrincipal.setLogSistema("----CriaMacro - buscaRegsUsados ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormPrincipal.setLogSistema(erro1.toString() + '\n');
            }
        }
        return regsUsando;
    }

    private void inicializaNomesRegs() {
        DefaultTableModel model = (DefaultTableModel) jtNomRegs.getModel();

        String nomes = "";
        for (int i = jtNomRegs.getRowCount(); i < getNReg(); i++) {
            nomes = String.valueOf(i + 1);
            model.addRow(new String[]{nomes});

        }

    }

    private void btInsRetBoolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btInsRetBoolActionPerformed
        TipoRetornoBool RetBool = new TipoRetornoBool(this, true);
        RetBool.setVisible(true);
        if (!RetBool.isClickVoltar()) {
            objOpCriaMacro.insertReturn(RetBool.isTipoRetorno());
            idOperacoes.add(14);
            TipoMetodo = "boolean";
        }
    }//GEN-LAST:event_btInsRetBoolActionPerformed

    private void NomeMacroFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_NomeMacroFocusLost
        if (verificaString(getNomeMacro())) {
            Icon icone = new ImageIcon("valid.png");
            lbValidNomMac.setIcon(icone);
            NomeMacro.setToolTipText("Nome válido !");
            lbValidNomMac.setToolTipText("Nome válido !");

        } else if (getNomeMacro().isEmpty()) {
            Icon icone = new ImageIcon("atencao.png");
            lbValidNomMac.setIcon(icone);
            NomeMacro.setToolTipText("Campo Vazio...");
            lbValidNomMac.setToolTipText("Campo Vazio...");
            btnSalvar.setEnabled(false);
        } else {
            Icon icone = new ImageIcon("erro.png");
            lbValidNomMac.setIcon(icone);
            NomeMacro.setToolTipText("Não use caracteres especiais(!'\"-.,@). Também não é possivel começar o nome com ( _ ) ou numeros !!!");
            lbValidNomMac.setToolTipText("Nome inválido !");
            btnSalvar.setEnabled(false);
        }

    }//GEN-LAST:event_NomeMacroFocusLost

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        SintaxNormaExib.requestFocus();
    }//GEN-LAST:event_formMouseClicked

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        btnAte.requestFocus();
    }//GEN-LAST:event_jLabel2MouseClicked

    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseClicked
        btnAte.requestFocus();
    }//GEN-LAST:event_jPanel1MouseClicked

    private void jPanel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseClicked
        btnFinalizaInstrucao.requestFocus();
    }//GEN-LAST:event_jPanel3MouseClicked

    private void btnNovoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNovoMouseClicked
        btnNovo.requestFocus();
    }//GEN-LAST:event_btnNovoMouseClicked

    private void btnSalvarRenameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarRenameActionPerformed
        if (JOptionPane.showConfirmDialog(this, "Depois de salvar não será possivel alterar o(s) nome(s) novamente.\nDeseja realmente manter esse(s) nome(s)?", "Renomeamento", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION) {
            if (jtNomRegs.isEditing()) {
                jtNomRegs.getCellEditor().stopCellEditing();
            }
            DefaultTableModel model = (DefaultTableModel) jtNomRegs.getModel();
            if (nomesRegsValidos(model)) {
                btnSalvarRename.setVisible(false);
                btnLimparRename.setVisible(false);
                jtNomRegs.setEnabled(false);
                btnLimpar.setEnabled(true);
                NReg.setEnabled(true);
                jtNomRegs.clearSelection();
                lbEstado.setText("Registradore(s) renomeado(s)!!!");
                bloq_desbloq_Buttons(true);
                estadoInicialButtons();
            }
        }

    }//GEN-LAST:event_btnSalvarRenameActionPerformed

    private void btnLimparRenameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparRenameActionPerformed
        if (jtNomRegs.isEditing()) {
            jtNomRegs.getCellEditor().stopCellEditing();
        }
        DefaultTableModel model = (DefaultTableModel) jtNomRegs.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            model.setValueAt(null, i, 1);
        }
    }//GEN-LAST:event_btnLimparRenameActionPerformed

    private void jPanel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseClicked
        jtNomRegs.requestFocus();
    }//GEN-LAST:event_jPanel6MouseClicked

    private void jtNomRegsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtNomRegsMouseClicked
        jtNomRegs.requestFocus();
    }//GEN-LAST:event_jtNomRegsMouseClicked

    private void NRegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NRegActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NRegActionPerformed
    //verifica se a string dentro jTable é uma string valida e armazena no array
    private boolean nomesRegsValidos(DefaultTableModel model) {

        boolean nomesValidos = false;
        for (int i = 0; i < getNReg(); i++) {
            String pal = (String) model.getValueAt(i, 1);
            if (verificaString(pal) && !(arRenameRegs.contains(pal.toUpperCase())) && !(arRenameRegs.contains(pal.toLowerCase()))) {
                nomesValidos = true;
                arRenameRegs.add((String) model.getValueAt(i, 1));
            } else {
                nomesValidos = false;
                jtNomRegs.setRowSelectionInterval(i, i);
                lbEstado.setText("Valor Inválido ou duplicado");
                i = getNReg();
                arRenameRegs.clear();
            }
        }
        return nomesValidos;

    }

    //verifica se uma dada string é valida de acordo com a expressao
    private boolean verificaString(String nome) {
        if (nome != null && !nome.isEmpty()) {
            try {
                if (!nome.isEmpty()) {
                    //converte a primeira pos. da string para int, para ver se a string começa com um inteiro
                    //caso a converção for feita entao retorna falso, pois um nome de registrador não pode começar com numero
                    int A = Integer.parseInt(Character.toString(nome.charAt(0)));
                }
                return false;
            } catch (NumberFormatException | HeadlessException e) {
                //caso a conversao de erro, entao o começo pode ser um caractere
                if (nome.charAt(0) != '_') {//verifica se não esta começamdo dom underline
                    int cont = 0;
                    String valor = "";
                    Pattern verfic = Pattern.compile("[A-Za-z0-9_]+");//classe que vai realizar a verificação de acordo com a expressao passada
                    //essa expressao indica que so se pode ter letras numeros e underline
                    Matcher mat = verfic.matcher(nome);
                    while (mat.find()) {//enquando não chegar no fim da string
                        cont++;
                        if (cont > 1) {//se a string tiver duas partes
                            return false;
                        }
                        valor += mat.group();//concatena caractere por caractere

                    }
                    if (nome.length() > valor.length()) {//compara se a string informada no começo é a mesma formada na execução

                        return false;
                    }
                } else {

                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    public void controleButtons() {
        if (!idOperacoes.isEmpty()) {
            if (!SintaxNormaExib.getText().isEmpty()) {
                if (idOperacoes.get(idOperacoes.size() - 1) == 1) {
                    bloq_desbloq_Buttons(false);
                    estadoFaçaButton();
                } else if (idOperacoes.get(idOperacoes.size() - 1) == 3) {
                    bloq_desbloq_Buttons(false);
                    estadoSenaoButton();
                } else if (idOperacoes.get(idOperacoes.size() - 1) == 2) {
                    bloq_desbloq_Buttons(false);
                    estadoAteButton();
                } else if (idOperacoes.get(idOperacoes.size() - 1) == 6) {
                    bloq_desbloq_Buttons(false);
                    estadoEnquantoButton();
                } else if (idOperacoes.get(idOperacoes.size() - 1) == 7) {
                    bloq_desbloq_Buttons(false);
                    estadoSeButton();
                } else if (idOperacoes.get(idOperacoes.size() - 1) == 8) {
                    if (getSintaxNormaExib().getText().isEmpty()) {
                        estadoInicialButtons();
                    } else {
                        bloq_desbloq_Buttons(true);
                    }
                } else {
                    bloq_desbloq_Buttons(true);
                }
            }
        } else {
            bloq_desbloq_Buttons(true);
            estadoInicialButtons();
            limpaMaquina();
        }
    }

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(() -> {
            new CriaMacro().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField NReg;
    private javax.swing.JTextField NomeMacro;
    private javax.swing.JTextArea SintaxNormaExib;
    private javax.swing.JButton btDesfazer;
    private javax.swing.JButton btInsRetBool;
    private javax.swing.JToggleButton btInsRetInt;
    private javax.swing.JButton btnAte;
    private javax.swing.JButton btnEnquanto;
    private javax.swing.JButton btnFaca;
    private javax.swing.JButton btnFinalizaInstrucao;
    private javax.swing.JButton btnInsertMacro;
    private javax.swing.JButton btnLimpar;
    private javax.swing.JButton btnLimparRename;
    private javax.swing.JButton btnNao;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnRegMaisUm;
    private javax.swing.JButton btnRegMenosUm;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JButton btnSalvarRename;
    private javax.swing.JButton btnSe;
    private javax.swing.JButton btnSenao;
    private javax.swing.JButton btnVoltar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTable jtInsAbertas;
    private javax.swing.JTable jtNomRegs;
    private javax.swing.JLabel lbEstado;
    private javax.swing.JLabel lbInstAbertas;
    private javax.swing.JLabel lbInstFinalizada;
    private javax.swing.JLabel lbValidNomMac;
    private javax.swing.JLabel lbValidNumReg;
    private javax.swing.JLabel lbcontInsAbertas;
    // End of variables declaration//GEN-END:variables

}
//ID Operações
/*
1-Faça
2-Ate
3-Senao
4-Nao
6-Enquanto
7-Se
8-Iserir Macro
9-Registrador+1
11-Registrador-1
13-Retorno Inteiro
14-Retorno Bool
15-Finalizar instrução
 */
