package telas.Norma;

import javax.swing.JFileChooser;

public class SeletorCompilador extends javax.swing.JDialog {

    private String Caminho;
    private Configurar objConfigurar;

    public SeletorCompilador(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    public SeletorCompilador(Configurar config) {
        Caminho = null;
        objConfigurar = config;
        initComponents();
        telaVisible();

    }

    private void telaVisible() {
        if (jFCompiler.showDialog(jFCompiler, "Selecionar") == JFileChooser.APPROVE_OPTION) {
            Caminho = jFCompiler.getSelectedFile().getAbsolutePath();
            this.dispose();
        } else {
            Caminho = objConfigurar.getJtCaminhoComp();
            this.dispose();

        }
    }

    public String getCaminho() {
        return Caminho;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFCompiler = new javax.swing.JFileChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Selecione a pasta do compilador Java (JDK)");
        setModal(true);

        jFCompiler.setCurrentDirectory(new java.io.File("C:\\Program Files"));
        jFCompiler.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jFCompiler, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jFCompiler, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(() -> {
            SeletorCompilador dialog = new SeletorCompilador(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser jFCompiler;
    // End of variables declaration//GEN-END:variables
}
