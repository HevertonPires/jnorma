package telas.Norma;

import classes.Norma.ClasseNorma;
import classes.Norma.ExecXML;
import java.awt.HeadlessException;
import java.io.IOException;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class InformacoesMacros extends JDialog {

    private ClasseNorma objNorma;
    private SelecMacro objSelecMac;
    private ExecXML objExecXML;
    private boolean entrouTelInform;
    private FormIni objFormIni;
    private DefaultListModel model;

    public InformacoesMacros(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

    }

    public InformacoesMacros(ClasseNorma Norma, SelecMacro Sel, FormIni Ini) {
        initComponents();
        this.objFormIni = Ini;
        this.objSelecMac = Sel;
        this.COdNorma.setEditable(false);
        this.jtRegRet.setEditable(false);
        this.jtTipoRet.setEditable(false);
        this.NumReg.setEditable(false);
        this.objNorma = Norma;
        objNorma.carregaArqNomes();
        listaMacros.addItem("Selecione uma macro...");
        for (int i = 0; i < objNorma.getNomeMacro().size(); i++) {
            listaMacros.addItem(objNorma.getNomeMacro().get(i));
        }
        model = new DefaultListModel();
        jlUtilMacros.setModel(model);

    }

    public JButton getBtnSelcExec() {
        return btnSelcExec;
    }

    public boolean isEntrouTelInform() {
        return entrouTelInform;
    }

    public void setEntrouTelInform(boolean entrouTelInform) {
        this.entrouTelInform = entrouTelInform;
    }

    public JButton getBtnExcluir() {
        return btnExcluir;
    }

    public SelecMacro getObjSelecMac() {
        return objSelecMac;
    }

    public void setObjSelecMac(SelecMacro objSelecMac) {
        this.objSelecMac = objSelecMac;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator2 = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        COdNorma = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        NumReg = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btVoltar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        listaMacros = new javax.swing.JComboBox<>();
        btnExcluir = new javax.swing.JButton();
        btnSelcExec = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jlUtilMacros = new javax.swing.JList<>();
        jLabel2 = new javax.swing.JLabel();
        jtTipoRet = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jtRegRet = new javax.swing.JTextField();

        jTextArea3.setColumns(20);
        jTextArea3.setRows(5);
        jScrollPane2.setViewportView(jTextArea3);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Informações Macros");
        setModal(true);

        COdNorma.setColumns(20);
        COdNorma.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        COdNorma.setRows(5);
        jScrollPane1.setViewportView(COdNorma);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Codigo Norma: ");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Nome Macro:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Nº Registradores:");

        btVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/voltar.png"))); // NOI18N
        btVoltar.setText("Voltar");
        btVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVoltarActionPerformed(evt);
            }
        });

        listaMacros.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        listaMacros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listaMacrosActionPerformed(evt);
            }
        });

        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/remover.png"))); // NOI18N
        btnExcluir.setText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        btnSelcExec.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/executarS.png"))); // NOI18N
        btnSelcExec.setText("Selecionar");
        btnSelcExec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelcExecActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/jNorma.png"))); // NOI18N
        jLabel7.setText("Máquina Universal NORMA");

        jScrollPane3.setViewportView(jlUtilMacros);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Está sendo utilizada em:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(54, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );

        jLabel5.setText("Tipo Retorno:");

        jLabel6.setText("Reg(s) Retorno:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jSeparator1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addGap(310, 310, 310))
            .addGroup(layout.createSequentialGroup()
                .addGap(65, 65, 65)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap(1018, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 732, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(84, 84, 84)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(101, 101, 101))))
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(listaMacros, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(NumReg, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtTipoRet, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtRegRet)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSelcExec)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExcluir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btVoltar)
                .addGap(39, 39, 39))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btVoltar, btnExcluir, btnSelcExec});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(listaMacros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(NumReg, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSelcExec)
                    .addComponent(btnExcluir)
                    .addComponent(btVoltar)
                    .addComponent(jtTipoRet, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jtRegRet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
                        .addGap(27, 27, 27))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {NumReg, jtRegRet, jtTipoRet, listaMacros});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btVoltar, btnExcluir, btnSelcExec});

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVoltarActionPerformed
        dispose();
    }//GEN-LAST:event_btVoltarActionPerformed

    //verifica se uma macro esta sendo usada em outra
    private boolean verificaNomeMacCod(ExecXML execXML, String nomeMacro) {
       
        try {
            if (execXML.getCodMacro().contains(nomeMacro)) {//verifica se dentro do codigo contem esse nome
                int pos = execXML.getCodMacro().indexOf("//");//pega a posição do //
                if (pos != -1 && pos < execXML.getCodMacro().length()) {//verifica se achou algo
                    pos = execXML.getCodMacro().indexOf(nomeMacro, pos);//a partir da pos, proucura o nome
                    if (pos != -1 && execXML.getCodMacro().charAt(pos - 1) == '/') {//verifica so o char da pos-1 é um /
                        int posf = execXML.getCodMacro().indexOf(" ", pos);//a partir da pos pega se a pos final ate chegar no espaço
                        return (posf - pos) == nomeMacro.length();//verifica se o tamanho das strings sao iguais
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormIni.setLogSistema("----InfMacros - verificaNomeMacroCond ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormIni.setLogSistema(erro1.toString()+'\n');
            }
            return false;
        }
    }
    private void listaMacrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listaMacrosActionPerformed
        try {
            jtRegRet.setText("");
            if (model != null) {
                model.clear();
            }
            objExecXML = new ExecXML();
            try {
                objExecXML.executarXML(listaMacros.getSelectedItem().toString() + ".xml");//executa o xml da macro selecionada
                NumReg.setText(objExecXML.getNumReg());//seta o numro de registradores 
                COdNorma.setText("\n" + objExecXML.getCodMacro().replaceAll("#", ""));//retira o id de registradores do codNorma
                for (int i = 0; i < objExecXML.getVarsRetorno().size(); i++) {//seta os registradores de retorno 
                    if (i > 0) {
                        jtRegRet.setText(jtRegRet.getText() + ",");
                    }
                    jtRegRet.setText(jtRegRet.getText() + objExecXML.getVarsRetorno().get(i).toString());
                }
                jtTipoRet.setText(objExecXML.getTipoMetodo());//seta o tipo do retorno
                int i = 0;
                ExecXML execXML2 = new ExecXML();
                while (objNorma.getNomeMacro().size() > i) {//realiza busca dentro de cada macro, verificando se ela esta sendo usada por ela
                    try {
                        execXML2.executarXML(objNorma.getNomeMacro().get(i).trim() + ".xml");

                        if (verificaNomeMacCod(execXML2, objExecXML.getNomMacro())) {
                            model.addElement(execXML2.getNomMacro());
                        }
                        i++;
                    } catch (ParserConfigurationException | SAXException | IOException ex) {
                        model.removeElement(execXML2.getNomMacro());
                    }
                }

            } catch (ParserConfigurationException | SAXException | IOException ex) {
                if (listaMacros.getSelectedItem().toString().equals("Selecione uma macro...")) {
                    NumReg.setText("");
                    COdNorma.setText("");
                    jtRegRet.setText("");
                    jtTipoRet.setText("");
                    jlUtilMacros.removeAll();

                } else {
                    if ("ParserConfigurationException".equals(ex.getMessage()) || "SAXException".equals(ex.getMessage())) {
                        JOptionPane.showMessageDialog(this, "Macro selecionada não pode ser lida com sucesso !!!", "Erro de leitura", JOptionPane.ERROR_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(this, "O arquivo XML não encontrado!!!", "Arquivo", JOptionPane.ERROR_MESSAGE);
                    }

                    erroEncontrarMacro();

                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "O arquivo XML apresenta problemas!!!", "Erro de leitura", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormIni.setLogSistema("----InfMacros - listMacros ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormIni.setLogSistema(erro1.toString()+'\n');
            }
        }

    }//GEN-LAST:event_listaMacrosActionPerformed

    private void erroEncontrarMacro() {
        listaMacros.removeItemAt(listaMacros.getSelectedIndex());
        if (objNorma.getNomeMacro().contains(listaMacros.getSelectedItem())) {
            objNorma.getNomeMacro().remove(listaMacros.getSelectedItem());
        }
        listaMacros.setSelectedItem("Selecione uma macro...");
        listaMacros.repaint();

    }

    public String retornaMensagem() {
        String msg;

        if (model.isEmpty()) {
            msg = "Deseja realmente excluir ?";
        } else {
            msg = "Macro(s):\n";
            for (int i = 0; i < model.size(); i++) {
                msg += " " + model.get(i).toString() + "\n";
            }
            msg += "\nAo excluir a macro [" + objExecXML.getNomMacro() + "], toda(s) a(s) macro(s) acima ira(m) parar de funcionar\n\nDeseja realmente excluir ?";
        }

        return msg;
    }

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        try {
            if (getObjSelecMac() != null) {
                if (!objFormIni.getNomMacro().equals(listaMacros.getSelectedItem().toString())) {
                    if (!listaMacros.getSelectedItem().toString().equals("Selecione uma macro...")) {
                        if (JOptionPane.showConfirmDialog(this, retornaMensagem(), "Excluir", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION) {

                            String Nome = listaMacros.getSelectedItem().toString();
                            if (getObjSelecMac().excluirMacro(Nome)) {
                                objNorma.getNomeMacro().remove(listaMacros.getSelectedItem().toString());
                                listaMacros.removeItemAt(listaMacros.getSelectedIndex());
                                listaMacros.repaint();
                                listaMacros.setSelectedItem("Selecione uma macro...");

                            } else {
                                JOptionPane.showMessageDialog(this, "Não foi possivel excluir Macro...", "Excluir", JOptionPane.ERROR_MESSAGE);
                            }

                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Por favor selecione a macro que deseja excluir...", "Excluir", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Macro selecionada esta sendo usada, e não pode ser excluida, por favor limpe o estado para exclui-la...", "Excluir", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormIni.setLogSistema("----InfMacros - excluirMacro ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormIni.setLogSistema(erro1.toString()+'\n');
            }
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnSelcExecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelcExecActionPerformed
        try {
            if (listaMacros.getSelectedIndex() != 0 && objNorma.verificaMacroExiste(listaMacros.getSelectedItem().toString())) {
                objFormIni.restauraRegistradores();
                objSelecMac.setFormTipo(1);
                getObjSelecMac().getObjFormPrincipal().setExecXML(objExecXML);
                objSelecMac.getObjOpNorma().setNomeArq(listaMacros.getSelectedItem().toString());
                objSelecMac.setReturnReg(Integer.parseInt(objExecXML.getNumReg()));
                getObjSelecMac().getObjFormPrincipal().setNomMacro(listaMacros.getSelectedItem().toString());
                getObjSelecMac().getObjFormPrincipal().addNlinhas(Integer.parseInt(objExecXML.getNumReg()) - 2);
                entrouTelInform = true;
                objFormIni.setRegsCarreg("0");
                objFormIni.getBtnEscolheMacro().setEnabled(false);
                objFormIni.getBtnExecMacro().setEnabled(false);
                objFormIni.setMacroExec(COdNorma.getText());
                this.dispose();

            } else if (listaMacros.getSelectedIndex() != 0) {
                JOptionPane.showMessageDialog(this, "O arquivo da macro selecionada não pode ser encontrado...", "Arquivo não localizado", JOptionPane.ERROR_MESSAGE);
                erroEncontrarMacro();
            }
        } catch (NumberFormatException | HeadlessException e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormIni.setLogSistema("----InfMacros - SelectExecução ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormIni.setLogSistema(erro1.toString()+'\n');
            }
        }
    }//GEN-LAST:event_btnSelcExecActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(() -> {
            InformacoesMacros dialog = new InformacoesMacros(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea COdNorma;
    private javax.swing.JTextField NumReg;
    private javax.swing.JButton btVoltar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnSelcExec;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTextArea jTextArea3;
    private javax.swing.JList<String> jlUtilMacros;
    private javax.swing.JTextField jtRegRet;
    private javax.swing.JTextField jtTipoRet;
    private javax.swing.JComboBox<String> listaMacros;
    // End of variables declaration//GEN-END:variables

}
