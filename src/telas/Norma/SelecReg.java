package telas.Norma;

import classes.Norma.ClasseNorma;
import classes.Norma.ExecXML;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class SelecReg extends javax.swing.JDialog {

    private ArrayList<String> Regs = new ArrayList<String>();
    private ClasseNorma objClasNorma;
    private CriaMacro objCriaMacro;
    private int Panel;
    private boolean clickVoltar;
    private ExecXML objExecXMLretMacro;

    public SelecReg(ClasseNorma O, CriaMacro P, int Panel) {
        this.Panel = Panel;
        this.objCriaMacro = P;
        this.objClasNorma = O;
        this.setModal(true);
        this.carregaLista();
        initComponents();
        clickVoltar = false;
        jlistRegs.setSelectedIndex(0);

    }

    public SelecReg(CriaMacro obj, int panel, ExecXML execXML) {
        objCriaMacro = obj;
        objExecXMLretMacro = execXML;
        this.setModal(true);
        this.carregaLista();
        initComponents();
        clickVoltar = false;
        lbSelecResultMap.setText("Selecione o(s) Registrador(es) que ira(m) receber o(s) retorno(s) da macro");
        btnVoltar.setVisible(false);
        Panel = panel;
        jlistRegs.setSelectedIndex(0);
    }

    public SelecReg() {
        this.carregaLista();
        this.setModal(true);
        initComponents();
        clickVoltar = false;
        jlistRegs.setSelectedIndex(0);
    }

    public void carregaLista() {
        Regs = (ArrayList<String>) objCriaMacro.getArRenameRegs().clone();
    }

    public ArrayList<String> getRegs() {
        return Regs;
    }

    public void setRegs(ArrayList<String> Regs) {
        this.Regs = Regs;
    }

    public ClasseNorma getObjClasNorma() {
        return objClasNorma;
    }

    public void setObjClasNorma(ClasseNorma objClasNorma) {
        this.objClasNorma = objClasNorma;
    }

    public CriaMacro getObjCriaMacro() {
        return objCriaMacro;
    }

    public void setObjCriaMacro(CriaMacro objCriaMacro) {
        this.objCriaMacro = objCriaMacro;
    }

    public int getPanel() {
        return Panel;
    }

    public boolean isClickVoltar() {
        return clickVoltar;
    }

    public void setPanel(int Panel) {
        this.Panel = Panel;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jlistRegs = new javax.swing.JList();
        jLabel1 = new javax.swing.JLabel();
        btnOk = new javax.swing.JButton();
        btnVoltar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        lbSelecResultMap = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Selecione ");
        setResizable(false);

        jlistRegs.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jlistRegs.setModel(new javax.swing.AbstractListModel() {
            public int getSize() {
                return Integer.valueOf(Regs.size());
            }
            public Object getElementAt(int i) {
                Object elemento = Regs.get(i);
                return elemento;
            }
        });
        jScrollPane2.setViewportView(jlistRegs);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Selecione o(s) registrador(es):");

        btnOk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/ok.png"))); // NOI18N
        btnOk.setText("OK");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        btnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/voltar.png"))); // NOI18N
        btnVoltar.setText("Voltar");
        btnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/jNorma.png"))); // NOI18N
        jLabel3.setText("Máquina Universal Norma");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(107, 107, 107)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVoltar)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(20, 20, 20))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lbSelecResultMap, javax.swing.GroupLayout.PREFERRED_SIZE, 386, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(96, 96, 96))))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnOk, btnVoltar});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbSelecResultMap, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38))
                    .addComponent(btnOk, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnVoltar)
                .addGap(13, 13, 13))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnOk, btnVoltar});

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        try {
            List<String> nome = jlistRegs.getSelectedValuesList();
            getObjCriaMacro().setValorSelect(nome.get(0));
            //verifica se foi selecionado o numero de registradores necessario
            if (objExecXMLretMacro != null && objExecXMLretMacro.getVarsRetorno().size() > 1 && (nome.size() < objExecXMLretMacro.getVarsRetorno().size())) {
                JOptionPane.showMessageDialog(this, "Macro com retorno multiplo. Por favor selecione mais " + (objExecXMLretMacro.getVarsRetorno().size() - nome.size()) + " registrador(es) !!", "Macro com retorno multiplo", JOptionPane.WARNING_MESSAGE);
            } else if (objExecXMLretMacro != null && objExecXMLretMacro.getVarsRetorno().size() > 1 && (nome.size() > objExecXMLretMacro.getVarsRetorno().size())) {
                JOptionPane.showMessageDialog(this, "Macro com retorno multiplo de " + (objExecXMLretMacro.getVarsRetorno().size()) + " registrador(es), Por favor não selecione mais do que o necessario !!", "Macro com retorno multiplo", JOptionPane.WARNING_MESSAGE);
            } else {
                switch (this.getPanel()) {
                    case 0:
                        getObjCriaMacro().getObjOpCriaMacro().regMaisUm();
                        break;
                    case 1:
                        getObjCriaMacro().getObjOpCriaMacro().regMenosUm();
                        break;
                    case 4:
                        getObjCriaMacro().getObjOpCriaMacro().insertReturn(nome.get(0), 0);
                        break;
                    case 5://vai selecionar o registrador que vai receber o retorno da macro
                        objCriaMacro.setRegRetorno(nome);
                        break;
                    case 6://caso esteja inserido uma macro, o seu retorno sera armazenado aqui
                        getObjCriaMacro().addRegs(nome);
                    default:
                        break;
                }

                this.setPanel(-1);
                this.dispose();
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objCriaMacro.getObjFormPrincipal().setLogSistema("----SelectReg - btnOK ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objCriaMacro.getObjFormPrincipal().setLogSistema(erro1.toString()+'\n');
            }
        }
    }//GEN-LAST:event_btnOkActionPerformed

    private void btnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed
        clickVoltar = true;
        this.dispose();
    }//GEN-LAST:event_btnVoltarActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SelecReg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SelecReg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SelecReg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SelecReg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new SelecReg().setVisible(true);
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnVoltar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JList jlistRegs;
    private javax.swing.JLabel lbSelecResultMap;
    // End of variables declaration//GEN-END:variables

}
