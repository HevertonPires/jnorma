package telas.Norma;

import classes.Norma.OperacoesJNorma;
import classes.Norma.ProcessarThread;
import classes.Norma.ClasseNorma;
import classes.Norma.ExecXML;
import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Formatter;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;

public class FormIni extends javax.swing.JFrame {

    private ClasseNorma objNorma;
    private OperacoesJNorma objOpNorma;
    private CriaMacro objCriaMacro;
    private String[] args;
    private ExecXML execXML;
    private InformacoesMacros objInfMac;
    private Configurar objConfig;
    private boolean clickLimpRegs;
    private boolean clickCarregReg;
    private ProcessarThread objThread;
    private boolean finalThread;
    private SelecMacro objSelecMacro;
    private boolean clickReexec;
    private String logSistema;
    private ArrayList<Integer> arPosRetornoTab; //posição dos registradores que receberam o retorno

    public FormIni(ClasseNorma classeNorma) throws FileNotFoundException {
        //Muda o estilo da interface swing, para o estilo usado pelo sistema operacional
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.updateComponentTreeUI(this);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {

        }

        initComponents();

        clickCarregReg = false;
        finalThread = false;
        clickLimpRegs = false;
        clickReexec = false;
        btnEscolheMacro.setEnabled(false);
        this.objNorma = classeNorma;
        btnCancelExec.setVisible(false);
        btnReexecutar.setVisible(false);
        logSistema = "";
        objConfig = new Configurar(this, true);
        objOpNorma = new OperacoesJNorma(objConfig, this);
        arPosRetornoTab = new ArrayList<>();

        if (!objOpNorma.verificaArqCaminhoCompilerExist()) {//se arquivo de caminho do compilador não existir
            objOpNorma.salvarArqCaminhoCompiler("");//salva o arquivo vazio
            objConfig.setVisible(true);//chama a tela de configuração
        } else {//caso o arquivo de caminho exista
            String Caminho = objOpNorma.lerArqCaminhoCompiler();//le o arquivo
            objConfig.setJtCaminhoComp(Caminho);
            if (!objConfig.detectaCompilador()) {//verifica se o compilador esta instalado no caminho
                if (!objConfig.getJtCaminhoComp().isEmpty()) {
                    JOptionPane.showMessageDialog(this, "Nenhum compilador encontrado no diretorio (" + objConfig.getJtCaminhoComp() + ")", "Compilador", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, "Nenhum compilador encontrado !!!", "Compilador", JOptionPane.WARNING_MESSAGE);
                }
                objConfig.setVisible(true);
            }

        }

    }

    public FormIni() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.updateComponentTreeUI(this);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {

        }
        objOpNorma = new OperacoesJNorma();
        initComponents();
        btnEscolheMacro.setEnabled(false);
        clickCarregReg = false;
        clickLimpRegs = false;
        finalThread = false;
        logSistema = "";
    }

    public void addRetorno_arPosRetornoTab(int pos) {
        arPosRetornoTab.add(pos);
    }

    public ArrayList<Integer> getArPosRetornoTab() {
        return arPosRetornoTab;
    }

    public ExecXML getExecXML() {
        return execXML;
    }

    public String getLogSistema() {
        return logSistema;
    }

    public void setLogSistema(String logSistema) {
        this.logSistema = getLogSistema() + logSistema;
    }

    public void setExecXML(ExecXML execXML) {
        this.execXML = execXML;
    }

    public void setMacroExec(String M) {
        this.MacroExec.setText(M);
    }

    public String getMacroExec() {
        return this.MacroExec.getText();
    }

    public void setFinalThread(boolean finalThread) {
        this.finalThread = finalThread;
    }

    public boolean isFinalThread() {
        return finalThread;
    }

    public JButton getBtnCancelExec() {
        return btnCancelExec;
    }

    public CriaMacro getObjCriaMacro() {
        return objCriaMacro;
    }

    public void setObjCriaMacro(CriaMacro objCriaMacro) {
        this.objCriaMacro = objCriaMacro;
    }

    public InformacoesMacros getObjInfMac() {
        return objInfMac;
    }

    public void setObjInfMac(InformacoesMacros objInfMac) {
        this.objInfMac = objInfMac;
    }

    public SelecMacro getObjSelecMacro() {
        return objSelecMacro;
    }

    public void setObjSelecMacro(SelecMacro objSelecMacro) {
        this.objSelecMacro = objSelecMacro;
    }

    public void setRegsCarreg(String regsCarreg) {
        this.lbregsCarreg.setText(regsCarreg);
    }

    public JProgressBar getJpBarra() {
        return jpBarra;
    }

    public JButton getBtnEscolheMacro() {
        return btnEscolheMacro;
    }

    public JButton getBtnReexecutar() {
        return btnReexecutar;
    }

    public JButton getBtnExecMacro() {
        return btnExecMacro;
    }

    public String[] getArgs() {
        return args;
    }

    public Configurar getObjConfig() {
        return objConfig;
    }

    public void setLbExecutando(String lbExecutando) {
        this.lbExecutando.setText(lbExecutando);
        this.lbExecutando.repaint();
    }

    public JTable getjTabelaRegs() {
        return jTabelaRegs;
    }

    public JButton getBtnExcluirMacro() {
        return btnExcluirMacro;
    }

    public JButton getBtnLimpaEstado() {
        return btnLimpaEstado;
    }

    public ClasseNorma getObjNorma() {
        return objNorma;
    }

    public void setObjNorma(ClasseNorma objNorma) {
        this.objNorma = objNorma;
    }

    public OperacoesJNorma getObjOpNorma() {
        return objOpNorma;
    }

    public void setObjOpNorma(OperacoesJNorma objOpNorma) {
        this.objOpNorma = objOpNorma;
    }

    public void setJpBarra(int jpBarra) {
        this.jpBarra.setValue(jpBarra);
    }

    public ProcessarThread getObjThread() {
        return objThread;
    }

    public void setNomMacro(String M) {
        this.NomMacro.setText(M);
    }

    public String getNomMacro() {
        return NomMacro.getText();
    }

    public void executandoMacroFim() {
        this.lbExecutando.setText("Macro Executada com Sucesso!!!");
        lbExecutando.repaint();

    }

    public void executandoMacroErro() {
        this.lbExecutando.setText("Erro na Execução!!!");
        lbExecutando.repaint();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu3 = new javax.swing.JMenu();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        btnEscolheMacro = new javax.swing.JButton();
        btnLimparRegs = new javax.swing.JButton();
        lbExecutando = new javax.swing.JLabel();
        btnCarregaReg = new javax.swing.JButton();
        btnExcluirMacro = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        btnLimpaEstado = new javax.swing.JButton();
        btnReexecutar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        MacroExec = new javax.swing.JTextPane();
        jLabel5 = new javax.swing.JLabel();
        jpBarra = new javax.swing.JProgressBar();
        jLabel2 = new javax.swing.JLabel();
        btnRemover = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTabelaRegs = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        btnSair = new javax.swing.JButton();
        btnAddReg = new javax.swing.JButton();
        NomMacro = new javax.swing.JTextField();
        btnCriaMacro = new javax.swing.JButton();
        btnInformacoes = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        btnCancelExec = new javax.swing.JButton();
        btnExecMacro = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txaRegsRetornos = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        lbregsCarreg = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jmArquivo = new javax.swing.JMenu();
        jmnConfigurar = new javax.swing.JMenuItem();
        jmAjuda = new javax.swing.JMenu();
        jmnSobre = new javax.swing.JMenuItem();
        jmnInfoUso = new javax.swing.JMenuItem();

        jMenu3.setText("jMenu3");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Simulador jNORMA");
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setIconImages(null);

        jScrollPane2.setBorder(null);

        btnEscolheMacro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/selecionar.png"))); // NOI18N
        btnEscolheMacro.setText("    Escolher Macro");
        btnEscolheMacro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEscolheMacroActionPerformed(evt);
            }
        });

        btnLimparRegs.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/limpar.png"))); // NOI18N
        btnLimparRegs.setText("Limpar Registradores");
        btnLimparRegs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparRegsActionPerformed(evt);
            }
        });

        btnCarregaReg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/carregar.png"))); // NOI18N
        btnCarregaReg.setText("Carregar Registrador");
        btnCarregaReg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCarregaRegActionPerformed(evt);
            }
        });

        btnExcluirMacro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/remover.png"))); // NOI18N
        btnExcluirMacro.setText("      Excluir Macro");
        btnExcluirMacro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirMacroActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel7.setText("Executando: ");

        btnLimpaEstado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/restaurar.png"))); // NOI18N
        btnLimpaEstado.setText("Restaurar Estado");
        btnLimpaEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpaEstadoActionPerformed(evt);
            }
        });

        btnReexecutar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/executarPeq.png"))); // NOI18N
        btnReexecutar.setText("Reexecutar");
        btnReexecutar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReexecutarActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        MacroExec.setEditable(false);
        MacroExec.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        MacroExec.setMaximumSize(new java.awt.Dimension(6, 20));
        jScrollPane1.setViewportView(MacroExec);
        MacroExec.getAccessibleContext().setAccessibleDescription("");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Macro em execução:");

        jpBarra.setBackground(new java.awt.Color(255, 255, 255));
        jpBarra.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jpBarra.setForeground(new java.awt.Color(0, 0, 0));
        jpBarra.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jpBarra.setStringPainted(true);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("    Modelo de Execução de Macros NORMA");

        btnRemover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/remover.png"))); // NOI18N
        btnRemover.setText("Remover Registrador");
        btnRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Registradores:");

        jTabelaRegs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"X", 0},
                {"Y", 0}
            },
            new String [] {
                "Nome", "Valor"
            }
        ));
        jTabelaRegs.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTabelaRegs.setGridColor(new java.awt.Color(0, 0, 0));
        jTabelaRegs.setSelectionBackground(new java.awt.Color(51, 102, 255));
        jTabelaRegs.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(jTabelaRegs);

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/executar.png"))); // NOI18N

        btnSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/sair.png"))); // NOI18N
        btnSair.setText("           Finalizar");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnAddReg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/add2.png"))); // NOI18N
        btnAddReg.setText("Adicionar Registrador");
        btnAddReg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddRegActionPerformed(evt);
            }
        });

        NomMacro.setEditable(false);
        NomMacro.setBackground(new java.awt.Color(255, 255, 255));
        NomMacro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        NomMacro.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnCriaMacro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/criarMacro.png"))); // NOI18N
        btnCriaMacro.setText("         Criar Macro");
        btnCriaMacro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCriaMacroActionPerformed(evt);
            }
        });

        btnInformacoes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/informa+º+Áes.png"))); // NOI18N
        btnInformacoes.setText("Informações Macros");
        btnInformacoes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInformacoesActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/jNorma.png"))); // NOI18N
        jLabel14.setText("Simulador");

        btnCancelExec.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/sair.png"))); // NOI18N
        btnCancelExec.setText("Cancelar");
        btnCancelExec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelExecActionPerformed(evt);
            }
        });

        btnExecMacro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/executar2.png"))); // NOI18N
        btnExecMacro.setText("Executar Macro");
        btnExecMacro.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExecMacro.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnExecMacro.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnExecMacro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExecMacroActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Registrador(es) de Retorno"));

        txaRegsRetornos.setEditable(false);
        txaRegsRetornos.setColumns(20);
        txaRegsRetornos.setFont(new java.awt.Font("Monospaced", 1, 13)); // NOI18N
        txaRegsRetornos.setRows(5);
        jScrollPane4.setViewportView(txaRegsRetornos);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Carregados:");

        lbregsCarreg.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lbregsCarreg.setForeground(new java.awt.Color(51, 51, 255));
        lbregsCarreg.setText("0");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbregsCarreg)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(lbregsCarreg))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(507, 507, 507)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 591, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnLimpaEstado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnEscolheMacro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnCriaMacro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnExcluirMacro, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnExecMacro, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSair, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(628, 628, 628)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel14))
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 367, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(NomMacro, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(341, 341, 341)
                        .addComponent(jLabel3))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(128, 128, 128)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnInformacoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnRemover)
                                    .addComponent(btnLimparRegs)
                                    .addComponent(btnAddReg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnCarregaReg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(30, 30, 30)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(58, 58, 58)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jpBarra, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCancelExec)
                            .addComponent(btnReexecutar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbExecutando, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAddReg, btnCarregaReg, btnInformacoes, btnLimparRegs, btnRemover});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnCriaMacro, btnEscolheMacro, btnExcluirMacro, btnExecMacro, btnLimpaEstado, btnSair});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel14))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(NomMacro, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btnEscolheMacro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnLimpaEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCriaMacro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnExcluirMacro)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnExecMacro)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel7)
                            .addComponent(lbExecutando, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jpBarra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(21, 21, 21)
                        .addComponent(btnCancelExec)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnReexecutar))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btnAddReg, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCarregaReg, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRemover)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnLimparRegs)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnInformacoes, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                .addContainerGap(58, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAddReg, btnCarregaReg, btnInformacoes, btnLimparRegs, btnRemover});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnCriaMacro, btnEscolheMacro, btnExcluirMacro, btnLimpaEstado, btnSair});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel14, jLabel8});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jpBarra, lbExecutando});

        jScrollPane2.setViewportView(jPanel2);

        jMenuBar1.setBackground(new java.awt.Color(204, 255, 255));
        jMenuBar1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jmArquivo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/opcao.png"))); // NOI18N
        jmArquivo.setText("Opções");

        jmnConfigurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/configuração.png"))); // NOI18N
        jmnConfigurar.setText("Configuração");
        jmnConfigurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmnConfigurarActionPerformed(evt);
            }
        });
        jmArquivo.add(jmnConfigurar);

        jMenuBar1.add(jmArquivo);

        jmAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/ajuda.png"))); // NOI18N
        jmAjuda.setText("Ajuda");

        jmnSobre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/informa+º+Áes.png"))); // NOI18N
        jmnSobre.setText("Sobre");
        jmnSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmnSobreActionPerformed(evt);
            }
        });
        jmAjuda.add(jmnSobre);

        jmnInfoUso.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/infSobre.png"))); // NOI18N
        jmnInfoUso.setText("Informações de Uso");
        jmnInfoUso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmnInfoUsoActionPerformed(evt);
            }
        });
        jmAjuda.add(jmnInfoUso);

        jMenuBar1.add(jmAjuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    public void restauraRegistradores() {
        try {
            DefaultTableModel model = (DefaultTableModel) jTabelaRegs.getModel();
            if (model.getRowCount() < 2) {
                model.addRow(new Object[]{null});
            }
            model.setValueAt("X", 0, 0);
            model.setValueAt("Y", 1, 0);

            for (int i = 0; i < jTabelaRegs.getRowCount(); i++) {
                model.setValueAt(0, i, 1);
            }
            int i = (jTabelaRegs.getRowCount()) - 1;

            while (i > 1) {//Remove linhas da tabela, deixando somente duas linhas

                model.removeRow(i);
                i--;
            }
            getObjNorma().limpaArrayNomeReg();//limpa o arrayList
            getObjNorma().limpaArrayValorReg();//limpa o arrayList

            lbregsCarreg.setText(String.valueOf(getObjNorma().getNomeReg().size()));//

            if (execXML != null) {
                execXML.setCodJava(null);
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            logSistema += "----FormIni - restauraRegistradores ---- \n\n";
            for (int i = 0; i < erro.length; i++) {
                logSistema += erro[i].toString() + '\n';
            }
        }
    }


    private void btnCriaMacroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCriaMacroActionPerformed

        objCriaMacro = new CriaMacro(getObjNorma(), this, getObjOpNorma());
        objCriaMacro.setExtendedState(JFrame.MAXIMIZED_BOTH);
        objCriaMacro.setVisible(true);
    }//GEN-LAST:event_btnCriaMacroActionPerformed

    @SuppressWarnings("empty-statement")
    private void btnExecMacroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExecMacroActionPerformed
        if (!lbregsCarreg.getText().equals("0")) {
            clickCarregReg = false;
            if (jTabelaRegs.isEditing()) {//Para que o valor que esta com focu nao seja perdido
                jTabelaRegs.getCellEditor().stopCellEditing();
            }
            if (MacroExec.getText().trim().length() != 0) {
                if (objConfig != null && objConfig.detectaCompilador()) {
                    lbExecutando.setText("Incializando...");
                    lbExecutando.repaint();
                    jpBarra.setStringPainted(true);
                    jpBarra.setValue(0);

                    btnAddReg.setEnabled(false);
                    btnEscolheMacro.setEnabled(false);
                    btnCriaMacro.setEnabled(false);
                    btnCarregaReg.setEnabled(false);
                    btnExecMacro.setEnabled(false);
                    btnRemover.setEnabled(false);
                    btnLimparRegs.setEnabled(false);
                    btnInformacoes.setEnabled(false);
                    btnLimpaEstado.setEnabled(false);
                    btnExcluirMacro.setEnabled(false);
                    btnCancelExec.setVisible(true);

                    objThread = new ProcessarThread(this);
                    objThread.start();
                } else {
                    JOptionPane.showMessageDialog(this, "Compilador Java não encontrado !!!", "Compilador", JOptionPane.ERROR_MESSAGE);
                    objConfig.setVisible(true);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Nenhuma Macro encontrada para Execução. Por favor selecione uma Macro e execute novamente !!!", "Macro não selecionada", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Não foi possivel executar, nenhum registrador carregado!!!", "Execução", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnExecMacroActionPerformed

    private void btnLimpaEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpaEstadoActionPerformed
        if (jpBarra.getValue() != 100) {
            File Arq = new File(objNorma.caminhoJava + getNomMacro() + ".class");
            if (Arq.exists()) {
                Arq.delete();
            }
        }
        operacoesFinalThread();
        btnReexecutar.setVisible(false);
    }//GEN-LAST:event_btnLimpaEstadoActionPerformed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed

        int Resp;
        Resp = JOptionPane.showConfirmDialog(this, " Tem certeza que deseja sair da aplicação ? ", "Sair", JOptionPane.YES_NO_OPTION);
        if (Resp == JOptionPane.YES_OPTION) {
            if (objOpNorma != null) {
                objOpNorma.excluiArqsTemps(objOpNorma.getArqExcluir());
            }
            if (!getLogSistema().isEmpty()) {
                File arq = new File("logSistema.txt");
                if (arq.exists()) {
                    String erros = lerArqdeLog();
                    erros += "\n\n" + getLogSistema();
                    salvarArqLog(erros);
                } else {
                    salvarArqLog(getLogSistema());
                }
            }
            System.exit(0);
        }
    }//GEN-LAST:event_btnSairActionPerformed

    private void btnAddRegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddRegActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTabelaRegs.getModel();

        Object[] linha = new Object[2];//alguma linha  
        linha[1] = 0;
        model.addRow(linha);


    }//GEN-LAST:event_btnAddRegActionPerformed

    public void addNlinhas(int num) {
        DefaultTableModel model = (DefaultTableModel) jTabelaRegs.getModel();
        for (int i = 0; i < num; i++) {//Adiciona n linhas na tabela de registradores 
            model.addRow(new Object[]{"A" + i, 0});
        }
    }

    private void btnEscolheMacroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEscolheMacroActionPerformed
        objSelecMacro = new SelecMacro(getObjNorma(), this, 1, getObjOpNorma());
        objSelecMacro.setVisible(true);
        if (!arPosRetornoTab.isEmpty()) {
            txaRegsRetornos.setText("");
            for (int i = 0; i < arPosRetornoTab.size(); i++) {
                if (i > 0 && i < arPosRetornoTab.size()) {
                    txaRegsRetornos.setText(txaRegsRetornos.getText() + ", ");
                }
                txaRegsRetornos.setText(txaRegsRetornos.getText() + jTabelaRegs.getValueAt(arPosRetornoTab.get(i), 0).toString());
            }
        }
    }//GEN-LAST:event_btnEscolheMacroActionPerformed

    private void btnCarregaRegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCarregaRegActionPerformed
        try {

            if (lbregsCarreg.getText().equals("0") || NomMacro.getText().isEmpty()) {
                boolean erro = false;
                if (jTabelaRegs.isEditing()) {
                    jTabelaRegs.getCellEditor().stopCellEditing();
                }
                jTabelaRegs.clearSelection();
                clickCarregReg = true;

                getObjNorma().limpaArrayNomeReg();
                getObjNorma().limpaArrayValorReg();

                DefaultTableModel model = (DefaultTableModel) jTabelaRegs.getModel();

                int linhasTabRegs = jTabelaRegs.getRowCount();
                // For para varrer as linhas
                for (int i = 0; i < linhasTabRegs; i++) {

                    if (model.getValueAt(i, 1) != null) {

                        String valor = model.getValueAt(i, 1).toString();
                        int valorConvert;
                        try {
                            valorConvert = Integer.valueOf(valor);//tenta converter para int o que esta jTable
                            if (model.getValueAt(i, 0) != null) {
                                String nome = model.getValueAt(i, 0).toString();

                                // Adiciona nos array's
                                // i linha e 0 e 1 coluna
                                if (!objNorma.getNomeReg().contains(nome) && !nome.isEmpty()) {//verificaçao de registradores com nomes iguais
                                    getObjNorma().getNomeReg().add(nome);
                                } else {
                                    JOptionPane.showMessageDialog(this, "Existem registradores com nomes iguais...", "Validação de nomes", JOptionPane.ERROR_MESSAGE);
                                    jTabelaRegs.setValueAt(null, i, 0);
                                    break;
                                }
                                getObjNorma().getValorReg().add(valorConvert);
                            } else {
                                jTabelaRegs.setRowSelectionInterval(i, i);
                                JOptionPane.showMessageDialog(this, "Nome do registrador não inserido !!!", "Inserir nome", JOptionPane.WARNING_MESSAGE);
                            }
                        } catch (NumberFormatException | HeadlessException e) {
                            //Verificaçoes e mensagem de valores invalidos inseridos 
                            if (jTabelaRegs.getValueAt(i, 1).toString().isEmpty()) {
                                erro = true;
                                JOptionPane.showMessageDialog(this, "Há registrador(es) adicionado(s), mas sem valor(es) inserido(s) !!!", "Falha de carregamento", JOptionPane.ERROR_MESSAGE);
                            } else {
                                JOptionPane.showMessageDialog(this, "Registrador: " + jTabelaRegs.getValueAt(i, 0).toString() + "\nValor: " + valor + "\nNão é um valor válido !!!", "Valor Invalido", JOptionPane.ERROR_MESSAGE);
                                jTabelaRegs.setValueAt("", i, 1);
                                erro = true;
                            }
                        }
                        //nenhum valor inserido em uma determinada linha
                    } else if (jTabelaRegs.getValueAt(i, 1) == null || jTabelaRegs.getValueAt(i, 0) == null) {
                        if (model.getRowCount() > 2 && jTabelaRegs.getValueAt(i, 1) == null) {
                            model.setValueAt(0, i, 1);
                        } else if (model.getRowCount() > 2) {
                            model.removeRow(i);
                        }
                    }

                }

                // Apenas para exibição
                lbregsCarreg.setText(String.valueOf(getObjNorma().getNomeReg().size()));

                if (!objNorma.getNomeReg().isEmpty() && !objNorma.getValorReg().isEmpty()) {//se os arrays de nome e valores não estiverem vazios
                    if (!clickReexec) {//se nao tiver clicado em reexecutar 
                        btnEscolheMacro.setEnabled(true);
                    } else {
                        btnExecMacro.setEnabled(true);
                    }
                    if (objSelecMacro != null) {
                        objSelecMacro.tiposSelecMacros(execXML, this.NomMacro.getText());//Funçao que vai direcionar o mapeamento 
                    }
                }
                //Quando seleciona a macro para execução na tela de informação, para abrir o mapeamento quando carregar
                if (objInfMac != null && objInfMac.isEntrouTelInform() && !lbregsCarreg.getText().equals("0")) {
                    objInfMac.getObjSelecMac().tiposSelecMacros(execXML, this.NomMacro.getText());
                    if (objInfMac != null && !objInfMac.getObjSelecMac().getObjMapeReg().isErroMap()) {
                        btnExecMacro.setEnabled(true);
                    }

                } else if (lbregsCarreg.getText().equals("0") && !erro) {
                    JOptionPane.showMessageDialog(this, "Nenhum valor inserido. Por favor insira valores inteiros !!!", "Validação da entrada", JOptionPane.WARNING_MESSAGE);
                }
                clickReexec = false;
            } else {
                JOptionPane.showMessageDialog(this, "Há valor(es) já carregado(s) no(s) registrador(es), para carregar novo(s) valor(es) clique em Limpar Registradores !!!", "Carregamento de Valores", JOptionPane.ERROR_MESSAGE);
            }
            if (lbregsCarreg.getText().equals("0")) {
                btnExecMacro.setEnabled(false);
            }
            if (!arPosRetornoTab.isEmpty()) {
                txaRegsRetornos.setText("");
                for (int i = 0; i < arPosRetornoTab.size(); i++) {
                    if (i > 0 && i < arPosRetornoTab.size()) {
                        txaRegsRetornos.setText(txaRegsRetornos.getText() + ", ");
                    }
                    txaRegsRetornos.setText(txaRegsRetornos.getText() + jTabelaRegs.getValueAt(arPosRetornoTab.get(i), 0).toString());
                }
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            logSistema += "----FormIni - btnCarregaRegistradores ---- \n\n";
            for (int i = 0; i < erro.length; i++) {
                logSistema += erro[i].toString() + '\n';
            }
        }
    }//GEN-LAST:event_btnCarregaRegActionPerformed

    private void jmnConfigurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmnConfigurarActionPerformed

        String caminho = getObjOpNorma().lerArqCaminhoCompiler();
        objConfig.setJtCaminhoComp(caminho);
        objConfig.setVisible(true);
    }//GEN-LAST:event_jmnConfigurarActionPerformed

    private void jmnSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmnSobreActionPerformed
        FormSobre P7 = new FormSobre();
        P7.setVisible(true);

    }//GEN-LAST:event_jmnSobreActionPerformed

    private void btnInformacoesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInformacoesActionPerformed

        objInfMac = new InformacoesMacros(getObjNorma(), new SelecMacro(getObjNorma(), this, 0, getObjOpNorma()), this);
        objInfMac.getBtnExcluir().setVisible(false);
        if (clickCarregReg && !getNomMacro().isEmpty()) {
            objInfMac.getBtnSelcExec().setVisible(false);
        }
        objInfMac.setVisible(true);
        clickLimpRegs = false;
    }//GEN-LAST:event_btnInformacoesActionPerformed

    private void btnRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTabelaRegs.getModel();
        if (this.jTabelaRegs.getRowCount() != 2 && JOptionPane.showConfirmDialog(this, "Deseja realmente remover o registrador ?", "Remover", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE) == JOptionPane.YES_OPTION) {
            //Caso nenhuma linha ou coluna esteja selecionada, tenta remover a ultima linha
            if (this.jTabelaRegs.getRowCount() != 2 && (this.jTabelaRegs.getSelectedColumn() != -1 || this.jTabelaRegs.getSelectedRow() != -1)) {
                model.removeRow(jTabelaRegs.getSelectedRow());
            } else if (this.jTabelaRegs.getRowCount() != 2) {//remove a linha selecionada
                model.removeRow(jTabelaRegs.getRowCount() - 1);
            } else {
                JOptionPane.showMessageDialog(this, "Não foi possivel remover registrador !!!", "Erro de remoção", JOptionPane.ERROR_MESSAGE);
            }
        } else if (jTabelaRegs.getRowCount() == 2) {
            JOptionPane.showMessageDialog(this, "Registradores [ X ] [ Y ] são padrões, não podem ser removidos !!!", "Não foi possivel remover", JOptionPane.WARNING_MESSAGE);

        }
    }//GEN-LAST:event_btnRemoverActionPerformed

    private void btnLimparRegsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparRegsActionPerformed
        if (jTabelaRegs.isEditing()) {
            jTabelaRegs.getCellEditor().stopCellEditing();
        }
        jTabelaRegs.clearSelection();
        clickLimpRegs = true;
        objOpNorma.limpaValoresTableRegs(execXML);
        arPosRetornoTab.clear();
        txaRegsRetornos.setText("");
        objNorma.getIndexMapePrin().clear();
        //if (objInfMac != null) {
          //  objInfMac.setEntrouTelInform(false);
        //}
    }//GEN-LAST:event_btnLimparRegsActionPerformed

    private void btnExcluirMacroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirMacroActionPerformed
        InformacoesMacros objInformMacr = new InformacoesMacros(getObjNorma(), new SelecMacro(getObjNorma(), this, 0, getObjOpNorma()), this);
        objInformMacr.getBtnSelcExec().setVisible(false);
        objInformMacr.setVisible(true);
    }//GEN-LAST:event_btnExcluirMacroActionPerformed

    private void jmnInfoUsoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmnInfoUsoActionPerformed
        new Ajuda(this, false).setVisible(true);
    }//GEN-LAST:event_jmnInfoUsoActionPerformed

    private void btnCancelExecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelExecActionPerformed

        if (jpBarra.getValue() != 100) {
            File Arq = new File(getNomMacro() + ".class");
            if (Arq.exists()) {
                Arq.delete();
            }
        }
        objThread.interrupt();

        finalThread = true;

    }//GEN-LAST:event_btnCancelExecActionPerformed

    private void btnReexecutarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReexecutarActionPerformed
        if (JOptionPane.showConfirmDialog(this, "Deseja reexecutar a macro usando os mesmos valores de entrada ?", "Valores de entrada", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
            for (int i = 0; i < objNorma.getValorReg().size(); i++) {
                jTabelaRegs.setValueAt(objNorma.getValorReg().get(i), i, 1);
            }
            btnExecMacroActionPerformed(evt);
        } else {
            jpBarra.setValue(0);
            btnLimparRegsActionPerformed(evt);
            btnCarregaReg.setEnabled(true);
            objNorma.getIndexMapePrin().clear();
        }
        btnReexecutar.setVisible(false);
        //arPosRetornoTab.clear();
        clickReexec = true;
    }//GEN-LAST:event_btnReexecutarActionPerformed

    public void limpaValoreRegs() {//Realiza limpeza de valores dos registradores, sem remover linhas
        DefaultTableModel model = (DefaultTableModel) jTabelaRegs.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            jTabelaRegs.setValueAt(0, i, 1);
            if (jTabelaRegs.getValueAt(i, 0) == null) {
                model.removeRow(i);
                i--;
            }
        }
    }

    public void liberaButtosTelaPrincipal() {
        btnAddReg.setEnabled(true);
        btnCriaMacro.setEnabled(true);
        btnCarregaReg.setEnabled(true);
        btnExecMacro.setEnabled(true);
        btnLimpaEstado.setEnabled(true);
        btnExcluirMacro.setEnabled(true);
        btnLimparRegs.setEnabled(true);
        btnRemover.setEnabled(true);
        btnEscolheMacro.setEnabled(false);
        btnInformacoes.setEnabled(true);
        btnCancelExec.setVisible(false);
    }

    public void operacoesFinalThread() {
        liberaButtosTelaPrincipal();
        restauraRegistradores();
        objOpNorma.setVoltMap(false);
        objOpNorma.getValorRegs().clear();
        jpBarra.setValue(0);
        this.jpBarra.setValue(0);
        this.lbExecutando.setText("");
        NomMacro.setText(null);
        MacroExec.setText(null);
        lbExecutando.repaint();
        objInfMac = null;
        clickCarregReg = false;
        clickLimpRegs = false;
        clickReexec = false;
        jTabelaRegs.clearSelection();
        objSelecMacro = null;
        objNorma.getIndexMapePrin().clear();
        getObjOpNorma().excluiArqsTemps(getObjOpNorma().getArqExcluir());
        setFinalThread(false);
        getObjOpNorma().getArqExcluir().clear();
        arPosRetornoTab.clear();
        txaRegsRetornos.setText("");
        execXML = null;

    }

    public String lerArqdeLog() {
        String nomeArq = "logSistema.txt";
        String linha = "";
        try (FileReader reader = new FileReader(nomeArq); BufferedReader leitor = new BufferedReader(reader)) {

            linha = leitor.readLine() + '\n';
            while (leitor.ready()) {
                linha += leitor.readLine() + '\n';
            }

        } catch (Exception e) {

        }
        return linha;
    }

    public void salvarArqLog(String erros) {
        String nomeArq = "logSistema.txt";
        String texto = "";

        //tentando criar arquivo
        try (Formatter saida = new Formatter(nomeArq)) {
            texto = erros;
            saida.format(texto);
            saida.close();

        } catch (Exception e) {

        }
    }

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormIni.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        }

        java.awt.EventQueue.invokeLater(() -> {
            new FormIni().setVisible(true);
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextPane MacroExec;
    private javax.swing.JTextField NomMacro;
    private javax.swing.JButton btnAddReg;
    private javax.swing.JButton btnCancelExec;
    private javax.swing.JButton btnCarregaReg;
    private javax.swing.JButton btnCriaMacro;
    private javax.swing.JButton btnEscolheMacro;
    private javax.swing.JButton btnExcluirMacro;
    private javax.swing.JButton btnExecMacro;
    private javax.swing.JButton btnInformacoes;
    private javax.swing.JButton btnLimpaEstado;
    private javax.swing.JButton btnLimparRegs;
    private javax.swing.JButton btnReexecutar;
    private javax.swing.JButton btnRemover;
    private javax.swing.JButton btnSair;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTabelaRegs;
    private javax.swing.JMenu jmAjuda;
    private javax.swing.JMenu jmArquivo;
    private javax.swing.JMenuItem jmnConfigurar;
    private javax.swing.JMenuItem jmnInfoUso;
    private javax.swing.JMenuItem jmnSobre;
    private javax.swing.JProgressBar jpBarra;
    private javax.swing.JLabel lbExecutando;
    private javax.swing.JLabel lbregsCarreg;
    private javax.swing.JTextArea txaRegsRetornos;
    // End of variables declaration//GEN-END:variables

}
