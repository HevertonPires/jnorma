package telas.Norma;

import classes.Norma.OperacoesJNorma;
import classes.Norma.ClasseNorma;
import classes.Norma.ExecXML;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;
import macronumparam.MacroNumParam;
import org.xml.sax.SAXException;

public class SelecMacro extends javax.swing.JDialog {

    private InsertCondi objInserirCond;
    private ClasseNorma objsClasNorma;
    private FormIni objFormPrincipal;
    private CriaMacro objCriaMacro;
    private int formTipo;
    private int returnReg;
    private boolean MacroCond;
    private String nomeArq;
    private OperacoesJNorma objOpNorma;
    private boolean clickVoltarSelMacro;
    private MapeReg objMapeReg;
    private ExecXML execXML;

    public SelecMacro(ClasseNorma O, FormIni P, int T, OperacoesJNorma obj) {
        this.objFormPrincipal = P;
        this.objsClasNorma = O;
        this.formTipo = T;
        this.objOpNorma = obj;
        this.setModal(true);
        clickVoltarSelMacro = false;
        execXML = null;
        initComponents();
    }

    public SelecMacro(ClasseNorma O, CriaMacro P, int T, FormIni Principal, OperacoesJNorma obj) {
        this.objFormPrincipal = Principal;
        this.objCriaMacro = P;
        this.objsClasNorma = O;
        this.formTipo = T;
        this.objOpNorma = obj;
        this.setModal(true);
        clickVoltarSelMacro = false;
        execXML = null;
        initComponents();
    }

    public SelecMacro(ClasseNorma O, CriaMacro P, int T, FormIni Principal, InsertCondi objInsCond, OperacoesJNorma obj) {
        this.objInserirCond = objInsCond;
        this.objFormPrincipal = Principal;
        this.objCriaMacro = P;
        this.objsClasNorma = O;
        this.formTipo = T;
        this.objOpNorma = obj;
        this.setModal(true);
        clickVoltarSelMacro = false;
        execXML = null;
        initComponents();
    }

    public SelecMacro() {
        this.setModal(true);
        clickVoltarSelMacro = false;
        execXML = null;
        initComponents();
    }

    public JList getListMacros() {
        return ListMacros;
    }

    public MapeReg getObjMapeReg() {
        return objMapeReg;
    }

    public ExecXML getExecXML() {
        return execXML;
    }

    public boolean isClickVoltarSelMacro() {
        return clickVoltarSelMacro;
    }

    public void setClickVoltarSelMacro(boolean clickVoltarSelMacro) {
        this.clickVoltarSelMacro = clickVoltarSelMacro;
    }

    public String getNomeArq() {
        return nomeArq;
    }

    public boolean excluirMacro(String Nome) {
        File Arq = new File(objsClasNorma.caminhoXmls + Nome + ".xml");//excluir
        if (Arq.exists()) {
            if (Arq.delete()) {
                JOptionPane.showMessageDialog(this, "Macro excluida com sucesso !!!", "Excluir", JOptionPane.INFORMATION_MESSAGE);
                if (new File(objsClasNorma.caminhoJava + Nome + "-2.xml").delete());
                if (new File(objsClasNorma.caminhoJava + Nome + ".java").delete());
                if (new File(objsClasNorma.caminhoClass + Nome + ".class").delete());
                return true;
            }
        }
        return false;
    }

    public InsertCondi getObjInserirCond() {
        return objInserirCond;
    }

    public void setObjInserirCond(InsertCondi objInserirCond) {
        this.objInserirCond = objInserirCond;
    }

    public ClasseNorma getObjsClasNorma() {
        return objsClasNorma;
    }

    public void setObjsClasNorma(ClasseNorma objsClasNorma) {
        this.objsClasNorma = objsClasNorma;
    }

    public FormIni getObjFormPrincipal() {
        return objFormPrincipal;
    }

    public void setObjFormPrincipal(FormIni objFormPrincipal) {
        this.objFormPrincipal = objFormPrincipal;
    }

    public CriaMacro getObjCriaMacro() {
        return objCriaMacro;
    }

    public void setObjCriaMacro(CriaMacro objCriaMacro) {
        this.objCriaMacro = objCriaMacro;
    }

    public int getFormTipo() {
        return formTipo;
    }

    public void setFormTipo(int formTipo) {
        this.formTipo = formTipo;
    }

    public int getReturnReg() {
        return returnReg;
    }

    public void setReturnReg(int returnReg) {
        this.returnReg = returnReg;
    }

    public boolean isMacroCond() {
        return MacroCond;
    }

    public void setMacroCond(boolean MacroCond) {
        this.MacroCond = MacroCond;
    }

    public String getLbStatusExecSelectMac() {
        return lbStatusExecSelectMac.getText();
    }

    public void setLbStatusExecSelectMac(String lbStatusExecSelectMac) {
        this.lbStatusExecSelectMac.setText(lbStatusExecSelectMac);
        this.lbStatusExecSelectMac.repaint();
    }

    public OperacoesJNorma getObjOpNorma() {
        return objOpNorma;
    }

    public void setObjOpNorma(OperacoesJNorma objOpNorma) {
        this.objOpNorma = objOpNorma;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ListMacros = new javax.swing.JList();
        btnOk = new javax.swing.JButton();
        btnVoltar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel13 = new javax.swing.JLabel();
        lbStatusExecSelectMac = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Selecione ");
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Selecione a macro:");

        ListMacros.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ListMacros.setModel(new javax.swing.AbstractListModel() {
            public int getSize(){
                return Integer.valueOf(objsClasNorma.getNomeMacro().size());
            }
            public Object getElementAt(int i){
                Object elemento = objsClasNorma.getNomeMacro().get(i);
                return elemento;
            }

        });
        jScrollPane1.setViewportView(ListMacros);

        btnOk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/ok.png"))); // NOI18N
        btnOk.setText("OK");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        btnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/voltar.png"))); // NOI18N
        btnVoltar.setText("Voltar");
        btnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/Norma/jNorma.png"))); // NOI18N
        jLabel13.setText("Máquina Universal NORMA");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel13))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(lbStatusExecSelectMac, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnOk, btnVoltar});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13)
                .addGap(13, 13, 13)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addComponent(lbStatusExecSelectMac, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnVoltar)
                    .addComponent(btnOk))
                .addGap(25, 25, 25))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnOk, btnVoltar});

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        try {
            setLbStatusExecSelectMac("Espere por favor...");
            lbStatusExecSelectMac.repaint();
            if (!objsClasNorma.getValorReg().isEmpty() || getObjCriaMacro() != null) {//verifica se tem registradores carregados
                if (!ListMacros.isSelectionEmpty()) {//verifica se foi selecionado uma macro
                    if (ListMacros.getSelectedValue().toString().trim().length() != 0) {
                        if (getObjsClasNorma().verificaMacroExiste(ListMacros.getSelectedValue().toString())) {
                            procThread th = new procThread();
                            btnOk.setEnabled(false);
                            btnVoltar.setEnabled(false);
                            th.start();
                        } else {
                            getObjsClasNorma().getNomeMacro().remove(ListMacros.getSelectedValue().toString());
                            JOptionPane.showMessageDialog(this, "Não foi possivel encontrar a macro selecionada", "Arquivo não existe", JOptionPane.ERROR_MESSAGE);
                        }

                    } else {
                        JOptionPane.showMessageDialog(this, "Macro não Selecionada. Por favor selecione uma Macro", "Macro não selecionada", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Macro não selecionada !!! Por favor selecione uma macro para continuar...", "Erro de leitura", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Impossivel realizar operação !!! Nenhum Registrador Carregado", "Carregamento de Registrador", JOptionPane.INFORMATION_MESSAGE);

            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormPrincipal.setLogSistema("----SelectMacro - btnOK ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormPrincipal.setLogSistema(erro1.toString() + '\n');
            }
        }

    }//GEN-LAST:event_btnOkActionPerformed

    class procThread extends Thread {

        @Override
        public void run() {

            nomeArq = "" + ListMacros.getSelectedValue().toString() + ".xml";

            execXML = new ExecXML();
            try {
                execXML.executarXML(nomeArq);
                if (formTipo == 2 && execXML.getTipoMetodo().equals("boolean")) {
                    execXML = null;
                    clickVoltarSelMacro = true;
                    JOptionPane.showMessageDialog(null, "Macro com retorno boleano, só pode ser usada em condições !!!", "Inserir macro", JOptionPane.ERROR_MESSAGE);
                    nomeArq = "";
                }
                if (execXML != null && objFormPrincipal.getObjConfig().detectaCompilador()) {
                    int NumRegs = Integer.parseInt(execXML.getNumReg());
                    ArrayList<String> Macros = new ArrayList<>();
                    Macros.add(execXML.getNomMacro());//adiciona a propria macro para realizar a compilção 
                    if ((objsClasNorma.getValorReg() != null && objsClasNorma.getValorReg().size() >= NumRegs) || (objCriaMacro != null && objCriaMacro.getArRenameRegs().size() >= NumRegs)) {
                        //gera todos os arquivos java necessarios para a execução dessa macro
                        if (objOpNorma.geraArqsJavaNecessario(Macros, objsClasNorma)) {
                            //compila todas as macros necessarias para execução desse macro
                            if (objOpNorma.reecompilaMacros(objOpNorma.getArqExcluir(), objFormPrincipal.getObjConfig().getJtCaminhoComp(), objsClasNorma.caminhoJava)) {
                                ClasseNorma.moveArquivo();//move o arquivo .class da pasta Java para a pasta Class
                                getObjFormPrincipal().setExecXML(execXML);
                                getObjOpNorma().setNomeArq(ListMacros.getSelectedValue().toString());
                                setReturnReg(Integer.parseInt(execXML.getNumReg()));
                                tiposSelecMacros(execXML, nomeArq);//chama a função de direcionamento de mapeamento
                            } else {
                                btnOk.setEnabled(true);
                                btnVoltar.setEnabled(true);
                                JOptionPane.showMessageDialog(null, "Não foi possivel compilar a macro [ " + ListMacros.getSelectedValue().toString() + " ], "
                                        + "por favor verifique se o compilador Java esta instalado e se está configurado corretamente !!!", "Inserir Macro", JOptionPane.ERROR_MESSAGE);
                                setLbStatusExecSelectMac("");

                            }
                        } else {
                            btnOk.setEnabled(true);
                            btnVoltar.setEnabled(true);
                            JOptionPane.showMessageDialog(null, "Não foi possivel inserir a macro [ " + ListMacros.getSelectedValue().toString() + " ], "
                                    + "por favor verifique se o compilador Java esta instalado e se está configurado corretamente !!!", "Inserir Macro", JOptionPane.ERROR_MESSAGE);
                            setLbStatusExecSelectMac("");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Quantidade de registradores incompativeis, são necessario " + execXML.getNumReg() + " registrador(es) criado(s) ou carregado(s)", "Registrador Faltando", JOptionPane.ERROR_MESSAGE);
                        setLbStatusExecSelectMac("");
                    }
                } else if (execXML != null) {

                    JOptionPane.showMessageDialog(null, "Não foi possivel compilar a macro [ " + ListMacros.getSelectedValue().toString() + " ], "
                            + "por favor verifique se o compilador Java esta instalado e se está configurado corretamente !!!", "Compilar Macro", JOptionPane.ERROR_MESSAGE);
                    setLbStatusExecSelectMac("");
                    objFormPrincipal.getObjConfig().setVisible(true);
                }
            } catch (ParserConfigurationException | SAXException | IOException ex) {
                JOptionPane.showMessageDialog(null, "Não foi possivel ler o arquivo !!!", "Erro de leitura XML", JOptionPane.ERROR_MESSAGE);
                btnOk.setEnabled(true);
                btnVoltar.setEnabled(true);

                StackTraceElement erro[] = ex.getStackTrace();
                objFormPrincipal.setLogSistema("----SelectMacro - thread ---- \n\n");
                for (StackTraceElement erro1 : erro) {
                    objFormPrincipal.setLogSistema(erro1.toString() + '\n');

                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Não foi possivel encontrar a macro selecionada", "Arquivo não existe", JOptionPane.ERROR_MESSAGE);
                btnOk.setEnabled(true);
                btnVoltar.setEnabled(true);

                StackTraceElement erro[] = ex.getStackTrace();
                objFormPrincipal.setLogSistema("----SelectMacro - thread ---- \n\n");
                for (StackTraceElement erro1 : erro) {
                    objFormPrincipal.setLogSistema(erro1.toString() + '\n');

                }
            }
            btnOk.setEnabled(true);
            btnVoltar.setEnabled(true);
        }

    }

    public void tiposSelecMacros(ExecXML execXML, String nomeArq) {
        int guardPos = 0;
        switch (this.getFormTipo()) {
            case 1:
                //mapeamento na tela principal
                selecaoPrincipal(execXML);
                break;
            case 3:
                //macro como condicioal
                selecaoMacrosCondicional(execXML);
                break;
            default:
                //inserir macro
                selecaoinsertMacro(execXML, guardPos);
                break;
        }

    }

    public void selecaoPrincipal(ExecXML execXML) {
        //verifica se o codigo de geração do arquivo de retorno ja foi criado no codigo
        try {
            if (!execXML.getCodJavaMetodo().contains("salvaArqRetorno")) {
                execXML.setCodJavaMetodo(execXML.getCodJavaMetodo() + getObjOpNorma().salvaArqRetorno(execXML.getNomMacro(), Integer.parseInt(execXML.getNumReg()), execXML));
            }

            objMapeReg = new MapeReg(this, getObjFormPrincipal(), getObjOpNorma(), execXML);
            if (!objMapeReg.isErroMap()) {         //nao ouve erro
                this.getObjFormPrincipal().setNomMacro(execXML.getNomMacro());
                this.getObjFormPrincipal().setMacroExec(execXML.getCodMacro().replaceAll("#", ""));
                objMapeReg.setVisible(true);
                objFormPrincipal.getBtnExecMacro().setEnabled(true);
                this.dispose();
            } else {

                objFormPrincipal.getBtnExecMacro().setEnabled(false);
                objOpNorma.limpaValoresTableRegs(execXML);
                objFormPrincipal.setObjSelecMacro(null);
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormPrincipal.setLogSistema("----SelectMacro - seleçãoPrincipal ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormPrincipal.setLogSistema(erro1.toString() + '\n');
            }
        }
    }

    public void selecaoinsertMacro(ExecXML execXML, int guardPos) {
        try {
            String id = "";
            //toda macro tem um ou mais retorno, inicialmente coloca @ como identificador desses registradores de retorno
            for (int i = 0; i < execXML.getVarsRetorno().size(); i++) {
                if (i > 0) {
                    id += ",";
                }
                id += "@";
            }
            getObjCriaMacro().setSintaxNormaExib(id + " = !" + execXML.getNomMacro() + " \n");
            getObjCriaMacro().setSintaxNorma(objCriaMacro.getSintaxNorma() + id + " = !" + execXML.getNomMacro() + " \n");
            getObjCriaMacro().setSintaxJava(objCriaMacro.getSintaxJava() + "!" + execXML.getNomMacro() + "(); \n");
            if (getObjCriaMacro() != null) {
                if (!getObjCriaMacro().getEstadoCodJava().isEmpty()) {
                    guardPos = getObjCriaMacro().getEstadoCodJava().size() - 1;
                }
                getObjCriaMacro().getEstadoCodJava().add(getObjCriaMacro().getSintaxJava());
                getObjCriaMacro().getEstadoCodNorma().add(getObjCriaMacro().getSintaxNorma());
            }
            objMapeReg = new MapeReg(this, getObjCriaMacro(), getObjOpNorma(), execXML);
            if (!objMapeReg.isErroMap()) {
                objMapeReg.setVisible(true);
                clickVoltarSelMacro = false;
                this.dispose();

            } else {
                getObjCriaMacro().desfazOperacao();
                clickVoltarSelMacro = true;
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormPrincipal.setLogSistema("----SelectMacro - SeleçãoInsertMacro ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormPrincipal.setLogSistema(erro1.toString() + '\n');
            }
        }
    }

    public void selecaoMacrosCondicional(ExecXML execXML) {
        try {
            String troca;

            objMapeReg = new MapeReg(this, getObjCriaMacro(), getObjOpNorma(), execXML);
            if (!objMapeReg.isErroMap()) {

                String nome = nomeArq.replaceAll(".xml", "");
                nomeArq = nome;

                getObjInserirCond().getRegsCarregList().clear();
                int numRegs = MacroNumParam.NumParamMacros(nomeArq);//pega o numero de parametros que uma macro tem
                //cria a string que sera a condição           
                String textoParam = nomeArq + " (";
                for (int i = 0; i < numRegs; i++) {
                    if (i > 0) {
                        textoParam += " ,";
                    }
                    textoParam += "var" + i;
                }
                textoParam += " )";
                getObjInserirCond().getRegsCarregList().add(textoParam);//adiciona dentro do array que é carregado nas condiçoes

                objMapeReg.setVisible(true);
                this.dispose();

            } else {
                if (!MacroCond) {
                    getObjCriaMacro().desfazOperacao();
                }
                //MacroCond = false;
                // clickVoltar = true;
                dispose();
            }
        } catch (Exception e) {
            StackTraceElement erro[] = e.getStackTrace();
            objFormPrincipal.setLogSistema("----SelectMacro - SeleçãoMacroCondicional ---- \n\n");
            for (StackTraceElement erro1 : erro) {
                objFormPrincipal.setLogSistema(erro1.toString() + '\n');
            }
        }
    }
    private void btnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed
        clickVoltarSelMacro = true;

        this.dispose();
    }//GEN-LAST:event_btnVoltarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SelecMacro.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        }
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new SelecMacro().setVisible(true);
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList ListMacros;
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnVoltar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lbStatusExecSelectMac;
    // End of variables declaration//GEN-END:variables

}
